<?php
/*
Quest Version: 1.0.0
*/

require_once("../../app/config/db-settings.php"); //Require DB connection

date_default_timezone_set('Africa/Harare');

$stmt = $mysqli->prepare("SELECT id, name, value
	FROM ".$db_table_prefix."configuration");	
$stmt->execute();
$stmt->bind_result($id, $name, $value);

while ($stmt->fetch()){
	$set[$name] = array('id' => $id, 'name' => $name, 'value' => $value);
}
$stmt->close();

//Set Settings
$emailActivation = $set['activation']['value'];
$mail_templates_dir = $set['mail-templates-dir']['value'];
$websiteName = $set['website_name']['value'];
$websiteUrl = $set['website_url']['value'];
$emailAddress = $set['email']['value'];
$resend_activation_threshold = $set['resend_activation_threshold']['value'];
$emailDate = date('dmy');
$template = $set['template']['value'];

$master_account = -1;

$default_hooks = array("#WEBSITENAME#","#WEBSITEURL#","#DATE#");
$default_replace = array($websiteName,$websiteUrl,$emailDate);





//Pages to require
require_once("../../app/config/class.mail.php");
require_once("../../app/config/class.user.php");
require_once("../../app/config/class.users.php");
require_once("../../app/config/class.tasks.php");
require_once("../../app/config/class.newuser.php");
require_once("../../app/config/class.memberStats.php");
require_once("../../app/config/class.nation.php");
require_once("../../app/config/class.member.php");
require_once("../../app/config/class.newMember.php");
require_once("../../app/config/class.report.php");
require_once("../../app/config/class.locality.php");
require_once("../../app/config/class.province.php");
require_once("../../app/config/class.alert.php");
require_once("../../app/config/class.event.php");
require_once("../../app/config/class.region.php");
require_once("../../app/config/class.houseChurch.php");
require_once("../../app/api/funcs.php");

//session_start();


//Global User Object Var
//loggedInUser can be used globally if constructed
if(isset($_SESSION["wcdbUser"]) && is_object($_SESSION["wcdbUser"]))
{
	$loggedInUser = $_SESSION["wcdbUser"];
		

	
}



?>
