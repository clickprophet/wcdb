<?php


require("../config-small.php"); //loads the system configuration settings

$settings = getSettings();
?>



        <div class="">
          <!--div class="page-title">
            <div class="title_left">
			
              <h3>
                    <a href="." type="button"><i class="fa fa-arrow-circle-left"></i></a>  Site Settings
                    <small>
                        configuration panel
                    </small>
                </h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div>
            </div>
          </div-->
          <div class="clearfix"></div>

          <div class="row">


                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Application Settings <small>Configuration Panel</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                      
                        </p>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Setting</th>
                              <th>Value</th>
							  <th>Actions</th>
                            </tr>
                          </thead>
                          <tbody>
						  <?php 
						  foreach ($settings as $row) {
						  ?>
							
                            <tr><form  id="form<?php echo $row["id"];?>">
                              <td id="name<?php echo $row["id"];?>"><?php echo $row["name"];?></td>
                              <td id="value<?php echo $row["id"];?>"><?php echo $row["value"];?></td>
							  <td>
							  </form>
							  <a id="editset<?php echo $row['id']; ?>" onclick="editSet(<?php echo $row['id']; ?>,'<?php echo $row['name']; ?>','<?php echo $row['value']; ?>')" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil"></i></a>
							  <a data-toggle="modal" href="#deleteSetting<?php echo $row['id']; ?>" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></a>
							  
							  <div class="modal fade" id="deleteSetting<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="deleteSetting<?php echo $row['id']; ?>" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<form action="" method="POST">
												<div class="modal-body">
													<p class="lead mb-0">
														<?php echo "Are you sure you want to delete the Setting "; ?> <strong>"<?php echo $row['name']; ?>"</strong>?</p>
														<p class="mt-0"><small><strong class="text-danger"><?php echo "this will permantly delete the setting from the database," ?></strong></small></p>
													</p>
												</div>
											
												<div class="modal-footer">
													<input name="settingID" type="hidden" value="<?php echo $row['id']; ?>" />
													<input name="value" type="hidden" value="<?php echo $row['name']; ?>" />
													<button type="input" name="submit" value="deleteSetting" class="btn btn-success btn-icon"><i class="glyphicon glyphicon-ok-sign"></i> Delete</button>
													<button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i> Cancel</button>
												</div>
											</form>
										</div>
									</div>
								</div>
								 <div class="modal fade" id="editSetting<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editSetting<?php echo $row['id']; ?>" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<form action="" method="POST">
												<div class="modal-body">
													<div class="form-group">
													  <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
													  <div class="col-md-9 col-sm-9 col-xs-12">
														<input type="text" class="form-control" name="name" placeholder="" value="<?php echo $row['name']; ?>" required="required">
													  </div>
													</div>
													<div class="form-group">
													  <label class="control-label col-md-3 col-sm-3 col-xs-12">Value</label>
													  <div class="col-md-9 col-sm-9 col-xs-12">
														<input type="text" class="form-control" name="value" placeholder="" value="<?php echo $row['value']; ?>" required="required">
													  </div>
													</div>
												</div>
											
												<div class="modal-footer">
													<input name="settingID" type="hidden" value="<?php echo $row['id']; ?>" />
													<button type="input" name="submit" value="editSetting" class="btn btn-success btn-icon"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
													<button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i> Cancel</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							  </td>
                            </tr>
							
						
						  <?php }?>
                           
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /page content -->
 <script>
          var handleDataTableButtons = function() {
              "use strict";
              0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                }, {
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }],
                responsive: !0
              })
            },
            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons()
                }
              }
            }();
        </script>
        <script type="text/javascript">
          $(document).ready(function() {
            $("#loaderImg").hide(); //hide loader image
            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
          });
          TableManageButtons.init();
        </script>
