
  <div class="body">
          <div class="page-title">
            <div class="title_left">
              <h3>Create Nation</h3>
            </div>

            
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Add Nation <small>wizard to create a new nation in database</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">


                  <!-- Smart Wizard -->
                  <div id="nationWizard" class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps">
                      <li>
                        <a href="#step-1">
                          <span class="step_no">1</span>
                          <span class="step_descr">
                                            Step 1<br />
                                            <small>Select nation</small>
                                        </span>
                        </a>
                      </li>
                      <li>
                        <a href="#step-2">
                          <span class="step_no">2</span>
                          <span class="step_descr">
                                            Step 2<br />
                                            <small>Enter Nation Details</small>
                                        </span>
                        </a>
                      </li>
                      <!--li>
                        <a href="#step-3">
                          <span class="step_no">3</span>
                          <span class="step_descr">
                                            Step 3<br />
                                            <small>Create Nation Leader</small>
                                        </span>
                        </a>
                      </li-->
                      <li>
                        <a href="#step-3">
                          <span class="step_no">3</span>
                          <span class="step_descr">
                                            Step 3<br />
                                            <small>Finish</small>
                                        </span>
                        </a>
                      </li>
                    </ul>
					
                    <div id="step-1">
                      <form name="regForm" id="step1" enctype="multipart/form-data" method="post" action="" novalidate>
                    	<div class="item col-md-3 col-sm-3 col-xs-12 well" style="">
                    		<p>
            							the map shows countries already registered in the database, kindly select the <strong>new</strong> nation you wish to add from  below and click next:
            					 </p>
                       <label class=""> Choose Country</label><br>
                          <div class="item col-md-12 col-sm-6 col-xs-12">
                             <select type="select" id="country" name="country" class="form-control" required="required" value="">

                              </select>
                        </div> 
                    	</div>
						 <div class="item col-md-9 col-sm-9 col-xs-12">
						 <h2 class="StepTitle">Select Nation on Map</h2>
						
						    <!--<div class="well">
						  <ul class="list">
							<li>The name of the nation</li>
							<li>the date that our ministry started in that nation</li>
							<li>The <strong>region</strong> that the nation belongs to</li>
							<li>The <strong>Leader Number one</strong> of that nation</li>
							<li>The details pertaining to the leader, contacts etc</li>
						  </ul>
						</div-->
							<div id="world-map-gdp1" class="item col-md-9 col-sm-12 col-xs-12"
               style="height:300px;"></div>
						 </div>
            </form>
					</div>
               <div id="step-2">
						<label  class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInputFile"></label>
						 <div class="item col-md-9 col-sm-9 col-xs-12">
						 <h2 class="StepTitle"><span id="ncountryCode"></span> : Nation Information</h2>
						  <input type="hidden"  id="ncountryCode" name="ncountryCode" value="" required="required"/>
						  </div>
						 <form id="nationAddForm" action="" enctype="multipart/form-data" method="post" role="form" class="form-horizontal form-label-left" novalidate>
						 <div class="item form-group">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12">Region Name  <span 
                class="required">*</span></label>
							  <div class="col-md-6 col-sm-9 col-xs-12">
								<select name="nregion" id="nregion" class="form-control nregions" required="required">
								  <option>Choose region</option>
								  
								</select>
							  </div>
                <script>
                  $('#nregion').change(function() {
                    var value=$("#nregion option:selected").val();
                    var name=$("#nregion option:selected").text();
                    $("#finishRegion").html(name);
                    $("#finishRegionChk").html("<i class='icon-check green'></i>");

                  });
                </script>
							<span><a href="#"  data-toggle="modal" data-target=".newRegion" id="newRegionBtn" class="btn btn-success btn-sm"><i class="icon-note"></i> New Region</a></span>
						</div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Ministry Start Date<span class="required">*</span>
							</label>
							<div class="item col-md-6 col-sm-9 col-xs-12">
							<input  id="nbirthday" name="nbirthday" class="date-picker form-control col-md-7 col-xs-12"  type="text" required="required" >
							</div>
						</div>
						  <script>
							$( function() {
								$( "#nbirthday" ).datepicker({ 
                  changeMonth: true,
								  changeYear: true,
                  dateFormat:"yy-mm-dd",
                  showAnim:"clip",
                  onSelect: function (date) {
                    $("#finishDate").html(date);
                    $("#finishDateChk").html("<i class='icon-check green'></i>");
                  },
              });
							} );
							</script>
						
						<div class="item form-group">
							 <label class=" control-label col-md-3 col-sm-3 col-xs-12">Leader Name  <span class="required">*</span></label>
							  <div class="item col-md-6 col-sm-9 col-xs-12">
								 <select name="nationLeader" id="nationLeader" class="form-control leaders nationLeader"
                  required="required">
                 <!--  <option>Select Leader..</option> --> 
                </select>
                </div>
                <script>
                  $('#nationLeader').change(function() {
                    var value=$("#nationLeader option:selected").val();
                    var name=$("#nationLeader option:selected").text();
                    $("#finishLeader").html(name);
                    $("#finishLeaderChk").html("<i class='icon-check green'></i>");

                  });
                </script>
							<!--span><p class="small">If the leader's name is not on the list proceed to create the leader's record on the next page</p></span-->
              <span><a href="#"  data-toggle="modal" data-target=".newMember" id="newRegionBtn" class="btn btn-success btn-sm"><i class="icon-note"></i> Create Leader</a></span>
						</div>

            <div class="item form-group">
            <label class=" control-label col-md-3 col-sm-3 col-xs-12">Number Of Desciples<span class="required">*</span></label>
              <div class="item col-md-6 col-sm-9 col-xs-12">
               <input type=""name="numberOfDesciples" id="numberOfDesciples"
                class="form-control leaders" required="required" placeholder="Number Of Desciples">
                </div>
                <script>
                  $('#numberOfDesciples').change(function() {
                    var value=$("#numberOfDesciples").val();
                    $("#finishnumberOfDesciples").html(value);
                    $("#finishnumberOfDesciplesChk").html("<i class='icon-check green'></i>");

                  });
                </script>
            </div>
					</form>
          </div>
					
                   
            <div id="step-3">
						<div class="col-md-3 col-sm-12 col-xs-12"></div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
						  <h2 class="StepTitle">Finish Nation creation</h2>
						  <p>Check and confirm the following details for the nation. If satisfied, click Finish to proceed to create the nation </p>
						  <table class="table table-stripped projects">
						  	<thead>
						  		<th style="width: 50%">field</th>
						  		<th>Value</th>
						  		<th>Check</th>
						  	</thead>
						  	<tbody>
						  		<tr>
						  			<td>Nation</td>
						  			<td><div id="finishNation"></div></td>
						  			<td><div id="finishNationChk"><i class="icon-close red"></i></div></td>
						  		</tr>
						  		<tr>
						  			<td>Region</td>
						  			<td><div id="finishRegion"></div></td>
						  			<td><div id="finishRegionChk"><i class="icon-close red"></i></div></td>
						  		</tr>
						  		<tr>
						  			<td>Date Started</td>
						  			<td><div id="finishDate"></div></td>
						  			<td><div id="finishDateChk"><i class="fa fa-warning" style="color:orange;" ></i></div></td>
						  		</tr>
						  		<tr>
						  			<td>Leader</td>
						  			<td><div id="finishLeader"></div></td>
						  			<td><div id="finishLeaderChk"><i class="icon-close red"></i></div></td>
						  		</tr>
                  <tr>
                    <td>Number Of Desciples</td>
                    <td><div id="finishnumberOfDesciples"></div></td>
                    <td><div id="finishnumberOfDesciplesChk"><i class="icon-close red"></i></div></td>
                  </tr>
						  	</tbody>
						  </table>


						  <span id="iloading" style="display:none;" ><img src="images/loading.gif" alt="loading..." width="30px" /></span>
						 </div> 
                    </div>

                  </div>
                  <!-- End SmartWizard Content -->

                 
                </div>
              </div>
          </div>
        </div>
      </div>
  </div>
  
      <!-- /page content -->

<!-- Modal content -->


<!--/ Modal Content -->

<script type="text/javascript">

  $(function(){
     nationLeaderCombo();
   });
	
          function loadPreview(user) {
          	var url = window.location.href; 
            var table=$('#data_preview').DataTable({
				"ajax": {
				"url": url+"/config/funcs.php?fn=fetchTempmembers&user="+user,
				"dataSrc": ""
				},
				"columns": [
					{ "data": "firstName" },
					{ "data": "lastName" },
					{ "data": "address" },
					{ "data": "surburb" },
					{ "data": "locality" },
					{ "data": "city" },
					{ "data": "country" },
					{ "data": "sex" },
					{ "data": "phone" },
					{ "data": "email" },
					{ "data": "tags" },
					{ "data": "id" }
				],
				"columnDefs": [
            {
                "targets": [ 11 ],
                "visible": false,
                "searchable": false
            },
			 {
                "targets": [ 10 ],
                "visible": false
            }]
                
			});
			
		

		}
</script>
  <!-- worldmap -->
      
 
<script type="text/javascript">
  
    $(function() {
      $('#country').html('<option>Choose Nation</option>'); 
          var opt = $("#country");
          $.getJSON("../app/api/funcs.php?fn=fetchCountries", function(response) {
           $.each(response, function() {
           opt.append($("<option />").val(this.code).text(this.name));
           });
        });
    });
    

    $(document).on('change', 'select#country', function(){

      var value = $('select#country option:selected').val();
      var sel=$('select#country');
      var name=this.options[this.selectedIndex].text;
      console.log(name);

         $("#ncountryCode").html(name);
          document.getElementById("ncountryCode").value=value;
          $("#finishNation").html(name);
          $("#finishNationChk").html("<i class='icon-check green'></i>");
          document.getElementById("countryx").value=value;
           
           var data=JSON.stringify(value);
           console.log(data);
           var mapObject = $('#world-map-gdp1').vectorMap('get', 'mapObject');
           mapObject.clearSelectedRegions();
           mapObject.setSelectedRegions(JSON.parse(data)); //to set
           //var name = mapObject.getRegionName(value);

        
         
         
         // refreshMemberCombo(value);

         });

    
</script>	 


<script>

          $(function() {
            $('#world-map-gdp1').vectorMap({
              map: 'world_mill_en',
              backgroundColor: '#6C7A89',
              zoomOnScroll: false,
              zoomAnimate:true,
              regionsSelectable:true,
              regionsSelectableOne: true,
              series: {
                regions: [{
                  values: gdpData,
                  //scale: ['#E6F2F0', '#149B7E'],
                  scale: ['#E6F2F0', '#149B7E'],
                  normalizeFunction: 'polynomial'
                }]
              },

              regionStyle: {
              //fill:'black',
              selected: {
                  fill: 'red' 
              }
            },
                onRegionTipShow: function(e, el, code) {
                el.html(el.html() );
              },

              onRegionClick: function (event, code) {
              var map = $('#world-map-gdp1').vectorMap('get', 'mapObject');
              var name = map.getRegionName(code);
              
                $("#ncountryCode").html(name);
                document.getElementById("ncountryCode").value=name;
                $("#finishNation").html(name);
                $("#finishNationChk").html("<i class='icon-check green'></i>");
                document.getElementById("countryx").value=name;
                //openPage(15,code); 

                //populate leader combo
                //refreshMemberCombo(value);    
            }



            });
          });
</script>

  <script type="text/javascript">
    function getid(){
            var str=window.location.href;
            var idArr= str.split("/");
            var l=idArr.length;

             if(idArr[l-2]!="new"){
              document.getElementById('country').style.display = 'visible'; 
            }else{
              document.getElementById('country').style.display = 'none'; 
            }

            return idArr[l-1];

           
          }

    $(document).ready(function() {
     
    
      // Smart Wizard
      $('#nationWizard').smartWizard({
        transitionEffect: 'slide',
        ajaxType: "POST", 
        onLeaveStep:onLeaveStepFunction,
        buttonOrder: ['prev', 'next', 'finish'] ,
        onFinish: function(req,res){
          saveNation();
          routie('/nations');
        }
        
    });

        function onLeaveStepFunction(obj, context){
        console.log("Do you want to leave the step "+context.fromStep+"?");
        return validateSteps(context.fromStep);
      
      }

      function validateSteps(step){
        var x=0;
        switch(step){
          // case 1:
          //  //validate step 1
          //     //  select region on map
          //       var data=JSON.stringify(value);
          //       var mapObject = $('#world-map-gdp1').vectorMap('get', 'mapObject');
          //        mapObject.setSelectedRegions(JSON.parse(data)); //to set
          //            if(mapObject='null'){
          //         errorNote('Select Nation','You did not select a nation.')
                  
          //           }else{
                      
                     
          //          }
          //          return false;
          //   break;
          case 2:
            //validate step 2
            var regForm=$('#nationAddForm');
            if (!validator.checkAll(regForm)) {
              return false;
            }
            return true;
            break;
          default:
            return true;
            break;
        }
      }
   
      initialiseRegionsCombo();

    $("#loaderImg").hide();
    });

    
  </script>
 
 

