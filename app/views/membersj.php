<?php
require_once("../config-small.php"); //loads the system configuration settings
session_start();
$members=fetchMembers();
$nation=$_SESSION["wcdbUser"]->nation;
?>

        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                   <a href="." type="button"><i class="fa fa-arrow-circle-left"></i></a> Manage Members
                    <small>         
                    </small>
                </h3>
				
            </div>
            <div class="title_right">
              <!--div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
				
				
              </div-->
			  <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right">
			  <div class="btn-group">
					<a onclick="openPage(5)" class="btn btn-default btn-sm" ><i class="glyphicon glyphicon-refresh"></i></a>
                    <button class="btn btn-default btn-sm" data-toggle="modal" data-target=".newMember"><i class="glyphicon glyphicon-plus"></i> Member</button>
					<button class="btn btn-default btn-sm" data-toggle="modal" data-target=".newLocality"><i class="glyphicon glyphicon-plus"></i> Locality</button>
				</div>
			  </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
		  <div class="col-md-12">
			<div class="x_panel">
			  <div class="x_content">
			   <div id="memberTabs" class="" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				  <li id="homeTab" role="presentation" class="active tab"><a href="#tab_content0" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Home</a>
				  </li>
				  <!--li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Profile</a>
				  </li>
				  <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Profile</a>
				  </li-->
				</ul>
				<div id="myTabContent" class="tab-content">
					<div role="tabpanel" class="tab-pane fade active in" id="tab_content0" aria-labelledby="home-tab" tabindex='1'>
					  <div class="col-md-12 col-sm-12 col-xs-12">
						
							<table id="datatable-responsive" class="table  table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
							  <thead>
								<tr>
								  <th>FirstName</th>
								  <th>LastName</th>
								  <th>Adress</th>
								  <th>Surburb</th>
								  <th>Locality</th>
								  <th>City</th>
								  <th>Country</th>
								  <th>Sex</th>
								  <th>Phone</th>
								  <th>E-mail</th>
								  <th>Tags</th>
								  <th>id</th>
								</tr>
							  </thead>
							</table>
						  </div>
						  </div>
						  <?php foreach($members as $row){?>
						  <div role="tabpanel" class="tab-pane fade" id="tab_content<?php echo $row["id"];?>" aria-labelledby="profile-tab">
							
						  </div>
						  <?php }?>
						
					  </div>

					

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /page content -->

<script type="text/javascript">
	
          $(document).ready(function() {
            var nation = '<?php echo $nation; ?>'; 
			if(nation.length>3){nation='';}
            var table=$('#datatable-responsive').DataTable({
				"ajax": {
				"url": "api/funcs.php?fn=fetchmembers&n="+nation,
				"dataSrc": ""
				},
				"columns": [
					{ "data": "firstName" },
					{ "data": "lastName" },
					{ "data": "address" },
					{ "data": "surburb" },
					{ "data": "locality" },
					{ "data": "city" },
					{ "data": "country" },
					{ "data": "sex" },
					{ "data": "phone" },
					{ "data": "email" },
					{ "data": "tags" },
					{ "data": "id" }
				],
				"columnDefs": [
            {
                "targets": [ 11 ],
                "visible": false,
                "searchable": true
            },
			 {
                "targets": [ 10 ],
                "visible": false
            }],
			    dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                }, {
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }
				]
			});
			
			 $('#datatable-responsive tbody').on( 'dblclick', 'tr', function () {
				var data = table.row( this ).data();
				//console.log(table.row(this).data());				
				if ( $(this).hasClass('selected') ) {
					$(this).removeClass('selected');
				}
				else {
					table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
					viewMemberTab(data["id"],data["firstName"]);  //open the tab for the respective member
				}
			} );
		
			$('#button').click( function () {
				table.row('.selected').remove().draw( false );
			} );
		});
        </script>

