
 <script id="topNations" type="text/template">
    <table class="countries_list">
      {{#nations}}
        <tr>
          <td><a style="cursor:pointer;" onclick="routie('/{{code}}')">{{name}}</a></td>
          <td class="fs15 fw700 text-right">{{m}}</td>
        </tr>
        {{/nations}}
       
      </tbody>
    </table>
  </script>
	
 <!-- top tiles -->
        <div class="row tile_count">
          <div class="col-sm-3 col-6">
          <div class="animated flipInY tile_stats_count dashTab card" onclick="">
            <div class="text-center">
              <div class="count gblDisc" ></div>
              <span class="count_top">Total Disciples</span>
              <!--span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i><?php echo ($stats->memberChange);?>% </i> this year</span-->
            </div>
          </div>
        </div>
        
        <div class="col-sm-3 col-6">
          <div class="animated flipInY  tile_stats_count dashTab card" onclick="">
            <div class="text-center">
              <div class="count green gblNations" ></div>
              <span class="count_top">Nations</span>
              <!--span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last year</span-->
            </div>
          </div>
        </div>
        <!--div class="col-sm-3 col-6">
          <div class="animated flipInY tile_stats_count dashTab card" onclick="">
            <div class="text-center">
              <span class="count_top"><i class="icon-calendar"></i> Events</span>
              <div class="count" id="gblEvents"></div>
              <!--span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last year</span>
            </div>
          </div>
        </div-->
        <div class="col-sm-3 col-6">
          <div class="animated flipInY tile_stats_count dashTab card" onclick="">
            <div class="text-center">
              <div class="count" id="gblLocalities"></div>
              <span class="count_top">Localities</span>
              <!--span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last year</span-->
            </div>
          </div>
        </div>
        <div class="col-sm-3 col-6">
          <div class="animated flipInY  tile_stats_count dashTab card">
            <div class="text-center">
              <div class="count" id="gblHC"></div>
              <span class="count_top">House Churches</span>
              <!--span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last year</span-->
            </div>
          </div>
        </div>
        <!--div class="col-sm-3 col-6">
          <div class="animated flipInY  tile_stats_count dashTab card">
            <div class="text-center">
              <span class="count_top"><i class="icon-user"></i> Leaders</span>
              <div class="count" id="gblLeaders"></div-->
              <!--span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last year</span>
            </div>
          </div>
        </div-->

        </div>
        <!-- /top tiles -->

                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                  
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Geo-Location of Disciples <small>geo-presentation</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">Settings 1</a>
                                  </li>
                                  <li><a href="#">Settings 2</a>
                                  </li>
                                </ul>
                              </li>
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                            <div class="dashboard-widget-content">
                              <div class="col-md-3 hidden-small">
                                <h2 class="line_30"><span class="gblDisc"></span> disciples in
                                  <span class="gblHC"></span> house churches in <span class="gblNations"></span> nations</h2>
                                <div id="topNationsDiv"></div>
                               
                              </div>
                              <div id="world-map-gdp" class="col-md-9 col-sm-12 col-xs-12" style="height:300px;"></div>
                            </div>
                          </div>
                        </div>
                </div>
                <br />
        		</div>
        		
            <div class="row">
             <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Recent Activities <small>in db</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div id="recentActions" class="dashboard-widget-content">
                        </div>
                      </div>
                    </div>
                  </div>

                <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="row">

                  <!-- Display top user profiles -->
                  <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="x_panel tile fixed_height_320 overflow_hidden">
                              <div class="x_title">
                                <h2>Recent Members Added</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                  </li>
                                  <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                      <li><a href="#">view all profiles</a>
                                      </li>
                                      <li><a href="#"></a>
                                      </li>
                                    </ul>
                                  </li>
                                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                                  </li>
                                </ul>
                                <div class="clearfix"></div>
                              </div>
        					           <div class="x_content" id="topMembers">
                            <ul class="list-unstyled top_profiles scroll-view">                        
                            </ul>
        					       </div>
                     </div>
                  </div>

                   <!-- Display online user profiles -->
                  <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="x_panel tile fixed_height_320 overflow_hidden">
                              <div class="x_title">
                                <h2>Online Users<small>active sessions</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                  </li>
                                  <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                      <li><a href="#">view all profiles</a>
                                      </li>
                                      <li><a href="#"></a>
                                      </li>
                                    </ul>
                                  </li>
                                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                                  </li>
                                </ul>
                                <div class="clearfix"></div>
                              </div>
                             <div class="x_content" id="onlineUsers" style="overflow: scroll;">
                              <ul class="list-unstyled top_profiles scroll-view">                        
                              </ul>
                        </div>
                    </div>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="x_panel tile fixed_height_320 overflow_hidden">
                      <div class="x_title">
                        <h2>Top Dates</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">View All</a>
                              </li>
                              <li><a href="#">New Event</a>
                              </li>
        					            <li><a href="#">Refresh Event</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content" id="topEvents">
        				
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="x_panel tile fixed_height_320">
                      <div class="x_title">
                        <h2>Quick Actions</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div class="dashboard-widget-content">
                          <ul class="quick-list">
                            <li><i class="icon-user"></i><a href="#">My Profile</a>
                            </li>
                            <li><i class="icon-arrow-down"></i><a href="#">import excel</a>
                            </li>
                            <li><i class="icon-envelope-letter"></i><a href="#">My Inbox</a> </li>
                            
                          </ul>

                          <div class="sidebar-widget">
                            <h4>Database Growth</h4>
                            <canvas width="150" height="80" id="foo" class="" style="width: 160px; height: 100px;"></canvas>
                            <div class="goal-wrapper">
                              
                              <span id="gauge-text" class="gauge-value pull-left"> </span><span class="gauge-value pull-left">KB</span>
                              <span id="goal-text" class="goal-value pull-right">5MB</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

        
                  <div class="col-md-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Monthly Summary <small>Over the past 30 days </small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">

                        <div class="row" style="border-bottom: 1px solid #E0E0E0; padding-bottom: 5px; margin-bottom: 5px;">
                          <div class="col-md-12" style="overflow:hidden;">
                            <span class="sparkline_one" style="height: 160px; padding: 10px 25px;">
                                          <canvas width="300" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                      </span>
                            <h5 style="margin:18px">disciple records creation activity</h5>
                          </div>

                          
                        </div>
                      </div>
                    </div>
                  </div>
                

          </div>
        </div>
  </div>
   <!-- gauge js -->
  <script type="text/javascript" src="js/gauge/gauge.min.js"></script>
  <script type="text/javascript" src="js/gauge/gauge_demo.js"></script>
  
  <!-- sparkline -->
  <script src="js/sparkline/jquery.sparkline.min.js"></script>

  <!-- display top latest events -->
  <script type="text/javascript">
      $(function() {
        var nation = '';
        sessionStorage.setItem("nation", nation);

        $.getJSON('api/funcs.php?fn=fetchTopNationMembers', function(data) {
          var template = $('#topNations').html();
          var html = Mustache.to_html(template, data);
          $('#topNationsDiv').html(html);
        }); //getEvents

      	$.getJSON('api/funcs.php?fn=fetchEvents', function(data) {
      		var template = $('#eventstpl').html();
      		var html = Mustache.to_html(template, data);
      		$('#topEvents').html(html);
      	}); //getEvents
      	
      	$.getJSON('api/funcs.php?fn=fetchOnlineUsers', function(data) {
      		var template = $('#usersOnlinestpl').html();
      		var html = Mustache.to_html(template, data);
      		$('#onlineUsers').html(html);
      	}); //getTopUsers

        $.getJSON('api/funcs.php?fn=fetchRecentMembers', function(data) {
          var template = $('#membersstpl').html();
          var html = Mustache.to_html(template, data);
          $('#topMembers').html(html);
        }); //getTopMembers
        
      	$.getJSON('api/funcs.php?fn=fetchLogs', function(data) {
          var template = $('#recentActionsstpl').html();
          var html = Mustache.to_html(template, data);
          $('#recentActions').html(html);
        }); //getTopMembers
      	//$('#footer').load('includes/footer.php').html();
        $.getJSON("api/funcs.php?fn=fetchGlobalTotals", function(res) {
              $('#gblLeaders').html(res.leaders.toLocaleString());
              $('#gblHC').html(res.housechurches.toLocaleString());
              $('#gblLocalities').html(res.localities.toLocaleString());
              $('.gblDisc').html(res.disciples.toLocaleString());
              $('#gblEvents').html(res.events.toLocaleString());
              $('.gblNations').html(res.nations.toLocaleString());
               $('.gblHC').html(res.housechurches.toLocaleString());
              $('#gauge-text').html(res.dbsize.toLocaleString());
              //alert(response.name);
         
          });
      }); //function
  </script>

  <!-- worldmap -->
  <script type="text/javascript" src="js/maps/jquery-jvectormap-2.0.3.min.js"></script>
  <script type="text/javascript" src="js/maps/gdp-data1.js"></script>
  <script type="text/javascript" src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
  <script type="text/javascript" src="js/maps/jquery-jvectormap-us-aea-en.js"></script>
   <script type="text/javascript">
    $(function() {
      'use strict';
      var countriesArray = $.map(countries, function(value, key) {
        return {
          value: value,
          data: key
        };
      });
      // Initialize autocomplete with custom appendTo:
      $('#autocomplete-custom-append').autocomplete({
        lookup: countriesArray,
        appendTo: '#autocomplete-container'
      });
    });
  </script>
   
  <script>
    $(function() {
      $('#world-map-gdp').vectorMap({
        map: 'world_mill_en',
        backgroundColor: '#6C7A89',
        zoomOnScroll: false,
        zoomAnimate:true,
        series: {
          regions: [{
            values: gdpData,
            scale: ['#ffffff', '#149B7E'],
            //scale: ['#f7f4f9', '#6D366F'],
            normalizeFunction: 'polynomial'
          }]
        },
        onRegionTipShow: function(e, el, code) {
          el.html(el.html() + ' (' + gdpData[code] + ' Members)');
        },

        onRegionClick: function (event, code) {
            routie('/'+code); 
        }

      });
    });
  </script>


<!--script>
    //@code_start
    $(function() {
      $.getJSON('js/maps/us-unemployment.json', function(data) {
        var val = 2009;
        statesValues = jvm.values.apply({}, jvm.values(data.states)),
          metroPopValues = Array.prototype.concat.apply([], jvm.values(data.metro.population)),
          metroUnemplValues = Array.prototype.concat.apply([], jvm.values(data.metro.unemployment));
        $('#usa_map').vectorMap({
          map: 'us_aea_en',
          markers: data.metro.coords,
          backgroundColor: 'transparent',
          zoomOnScroll: false,
          series: {
            markers: [{
              attribute: 'fill',
              scale: ['#FEE5D9', '#A50F15'],
              values: data.metro.unemployment[val],
              min: jvm.min(metroUnemplValues),
              max: jvm.max(metroUnemplValues)
            }, {
              attribute: 'r',
              scale: [5, 20],
              values: data.metro.population[val],
              min: jvm.min(metroPopValues),
              max: jvm.max(metroPopValues)
            }],
            regions: [{
              scale: ['#E6F2F0', '#149B7E'],
              attribute: 'fill',
              values: data.states[val],
              min: jvm.min(statesValues),
              max: jvm.max(statesValues)
            }]
          },
          onMarkerTipShow: function(event, label, index) {
            label.html(
              '<b>' + data.metro.names[index] + '</b><br/>' +
              '<b>Population: </b>' + data.metro.population[val][index] + '</br>' +
              '<b>Unemployment rate: </b>' + data.metro.unemployment[val][index] + '%'
            );
          },
          onRegionTipShow: function(event, label, code) {
            label.html(
              '<b>' + label.html() + '</b></br>' +
              '<b>Unemployment rate: </b>' + data.states[val][code] + '%'
            );
          }
        });
      });
    });
    //@code_end
  </script-->


  <script>
    $('document').ready(function() {
      $.ajax({
        url: "api/funcs.php?fn=fetchRecordsSparkline",
       method: "GET",
      success: function(data) {
        var date = [];
      var records = [];
      var jsonArr = jQuery.parseJSON(data);

      for(var i in jsonArr) {
        date.push(jsonArr[i].date);
        records.push(jsonArr[i].records);
      }

      $(".sparkline_one").sparkline(records, {
        type: 'bar',
        height: '125',
        barWidth: 13,
        colorMap: {
          '7': '#a1a1a1'
        },
        barSpacing: 2,
        barColor: '#26B99A'
      });
    }
    });
  });

  $("#loaderImg").hide(); //hide loader image
</script>

