
 <div class="body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                          <!-- start recent activity -->
                          <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                            <!-- detail row -->
                            <div class="x_panel">
                              <div class="x_title">
                                <h2>Summary <small>nation details</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                  </li>
                                  <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                      <li><a href="#">Settings 1</a>
                                      </li>
                                      <li><a href="#">Settings 2</a>
                                      </li>
                                    </ul>
                                  </li>
                                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                                  </li>
                                </ul>
                                <div class="clearfix"></div>
                              </div>
                              <div class="x_content">
                            <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:50%"><i class="icon-globe"></i> Country Name:</th>
                                  <td id="nName"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-speech"></i> Code</th>
                                  <td id="nCode"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-pointer"></i> Region:</th>
                                  <td id="nRegion"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-screen-smartphone"></i> Telephone:</th>
                                  <td id="nPhone"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-envelope-letter"></i> Email:</th>
                                  <td id="nEmail"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-calendar"></i> Ministry start Date:</th>
                                  <td id="nDate"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-user"></i> Leader No.1</th>
                                  <td id="nLeader"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-users"></i> Total Disciples</th>
                                  <td class="ctrlDiscTotal"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-users"></i> Disciples Registered in DB</th>
                                  <td class="discTotal"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-map"></i> Spiritual Provinces</th>
                                  <td class="provTotal"></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                        </div> <!--close detail row-->

                      <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Profile Progress <small>disciples in DB as % of total</small></h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                          <div id="echart_guage" style="height:240px;"></div>
                      
                        </div>
                      </div>
                    </div>
                  </div>
                  </div><!--col md 4 -->
                       <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Disciples <small>by date</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">print Report</a>
                                  </li>
                                  <li><a href="#">email Report</a>
                                  </li>
                                </ul>
                              </li>
                              <!--li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li-->
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                             <div id="echart_bar_horizontal" style="height:370px;"></div>
                            <!--canvas id="chartCanvas"></canvas-->
                          </div>
                        </div>
                      </div>
                     <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="x_panel tile fixed_height_320 overflow_hidden">
                          <div class="x_title">
                            <h2>Top Dates</h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">View All</a>
                                  </li>
                                  <li><a href="#">New Event</a>
                                  </li>
                                  <li><a href="#">Refresh Event</a>
                                  </li>
                                </ul>
                              </li>
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content" id="topNationalEvents">
                          </div>
                        </div>
                      </div>
                          <!-- Display online user profiles -->
                      <div class="col-md-6 col-sm-6 col-xs-12">
                              <div class="x_panel tile fixed_height_320 overflow_hidden">
                                  <div class="x_title">
                                    <h2>Online Users<small>active sessions</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                      <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                          <li><a href="#">view all profiles</a>
                                          </li>
                                          <li><a href="#"></a>
                                          </li>
                                        </ul>
                                      </li>
                                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                  </div>
                                 <div class="x_content" id="onlineNationalUsers" style="overflow: scroll;">
                                  <ul class="list-unstyled top_profiles scroll-view">                        
                                  </ul>
                            </div>
                        </div>
                      </div>
                    </div>

                  </div><!--col md 8 -->
                </div>
                    <!-- end row -->
              
            </div>

          </div>
        </div>
      </div>
	 

  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

 <!-- echart -->
  <script src="js/echart/echarts-all.js"></script>
  <script src="js/echart/green.js"></script>

  <!-- Nation Profile scripts -->
 <script type="text/javascript" src="js/includes/nationProfile.js"></script>
 <script type="text/javascript" src="js/includes/events.js"></script>
  
