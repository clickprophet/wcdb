<?php
require_once("../config-small.php"); //loads the system configuration settings
session_start();
$nation=$_SESSION["wcdbUser"]->nation;
$reports=getReports($nation);			
?>
        <div class="">
          <div class="page-title">
            <div class="title_left">
			
              <h3>
                    <a href="." type="button"><i class="fa fa-arrow-circle-left"></i></a>  Manage Reports
                    <small>
                        details of all nations registered
                    </small>
                </h3>
            </div>

            <!--div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div>
            </div-->
          </div>
          <div class="clearfix"></div>

          <div class="row">


                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Reports <small>Biannual Mission Field Accounts</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                          "Therefore go and make disciples of all nations..." Matt 28:19
                        </p>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Nation</th>
                              <th>Date</th>
                              <th>Members</th>
                              <th>Localities</th>
                              <th>HouseChurches</th>
                              <th>Leaders</th>
                              <th>Notes</th>
							  <th>Actions</th> 
                            </tr>
                          </thead>
                          <tbody>
						  <?php 
						  if($reports!=0){
						  foreach($reports as $row){
						  ?>
                            <tr>
                              <td><?php echo $row["name"];?></td>
                              <td><?php echo $row["date"];?></td>
                              <td><?php echo $row["members"];?></td>
                              <td><?php echo $row["localities"];?></td>
                              <td><?php echo $row["housechurches"];?></td>
                              <td><?php echo $row["leaders"];?></td>
                              <td><?php echo $row["notes"];?></td>
							  <td><a href="" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil"></i></a>
							  <a href="" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></a>
							  </td>
                            </tr>
						  <?php }
						  }?>
                           
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /page content -->
 <script>
          var handleDataTableButtons = function() {
              "use strict";
              0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                }, {
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }],
                responsive: !0
              })
            },
            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons()
                }
              }
            }();
        </script>
        <script type="text/javascript">
          $(document).ready(function() {
            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
          });
          TableManageButtons.init();
        </script>
