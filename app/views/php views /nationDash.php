<?php
require_once("../config-small.php"); //loads the system configuration settings
$nation=$_REQUEST['nation'];
$nationName=$_REQUEST['nationName'];
$stats=new memberStats($nation);
$alertBox="";
$members=getTotalMembers($nation) ;
$localities=getTotalLocalities($nation);
?>
<div style="">
<div><?php echo $alertBox; ?></div>
        <!-- top tiles -->
       <div class="row tile_count">
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab" onclick="openPage(5)">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="icon-users"></i> Total Members</span>
              <div class="count"><?php echo $members ;?></div>
              <span class="count_bottom"><i class="green">4% </i> From last year</span>
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab" onclick="loadLocalities('<?php echo $nation;?>')">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="icon-map"></i> Localities</span>
              <div class="count green"><?php echo $localities ;?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last year</span>
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="icon-users"></i> Reports</span>
              <div class="count"><?php echo $stats->adults ;?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last year</span>
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="icon-globe"></i> Provinces</span>
              <div class="count"><?php echo getTotalProvinces($nation) ;?></div>
              <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last year</span>
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="icon-home"></i> House Churches</span>
              <div class="count"><?php echo $stats->houseChurches ;?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last year</span>
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="icon-user"></i> Leaders</span>
              <div class="count"><?php echo $stats->total ;?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last year</span>
            </div>
          </div>

        </div>
        <!-- /top tiles -->

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
          
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $nationName;?> <small> nation progress summary</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="dashboard-widget-content">
                      <div class="col-md-3 hidden-small">
                        <h2 class="line_30"><?php echo $members; ?> Members in <?php echo $localities; ?> Localities</h2>

                        <table class="countries_list">
                          <tbody>
                             <?php $str="select count(m.id) as members, l.localityName, m.country from cmfi_members m 
										left join cmfi_localities l on m.locality=l.id
										where m.country='".$nation."'
										group by localityName 
										order by members desc LIMIT 5" ;
									$result = mysqli_query($mysqli, $str);
									while( $rs = $result->fetch_array(MYSQLI_ASSOC)) {
							  ?>
                            <tr>
                              <td><a onclick="localityProfile()"><?php echo $rs["localityName"];?></a></td>
                              <td class="fs15 fw700 text-right"><?php echo $rs["members"];?></td>
                            </tr>
								<?php }?>
                          </tbody>
                        </table>
                      </div>
					<div class="col-md-9 hidden-small">
					  <div id="grid" class="grid" style="height:350px;">
             
            </div>
					  </div>
                    </div>
                  </div>
                </div>
        </div>
        <br />

         <div class="row">


          <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="x_panel tile fixed_height_320 overflow_hidden">
                      <div class="x_title">
                        <h2>Misionaries</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Add New</a>
                              </li>
                              
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
					  <div class="x_content">
                      <ul class="list-unstyled top_profiles scroll-view">
                        <li class="media event">
                          <a class="pull-left border-aero profile_thumb">
                            <i class="fa fa-user aero"></i>
                          </a>
                          <div class="media-body">
                            <a class="title" href="#">Ms. Mary Jane</a>
                            <p><strong>$2300. </strong> Agent Avarage Sales </p>
                            <p> <small>12 Sales Today</small>
                            </p>
                          </div>
                        </li>
                        <li class="media event">
                          <a class="pull-left border-green profile_thumb">
                            <i class="fa fa-user green"></i>
                          </a>
                          <div class="media-body">
                            <a class="title" href="#">Ms. Mary Jane</a>
                            <p><strong>$2300. </strong> Agent Avarage Sales </p>
                            <p> <small>12 Sales Today</small>
                            </p>
                          </div>
                        </li>
                        <li class="media event">
                          <a class="pull-left border-blue profile_thumb">
                            <i class="fa fa-user blue"></i>
                          </a>
                          <div class="media-body">
                            <a class="title" href="#">Ms. Mary Jane</a>
                            <p><strong>$2300. </strong> Agent Avarage Sales </p>
                            <p> <small>12 Sales Today</small>
                            </p>
                          </div>
                        </li>
                        
                       
                      </ul>
					  </div>
                    </div>
          </div>

          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel tile fixed_height_320 overflow_hidden">
              <div class="x_title">
                <h2>Top Dates</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
             <article class="media event">
                    <a class="pull-left date">
                      <p class="month">April</p>
                      <p class="day">23</p>
                    </a>
                    <div class="media-body">
                      <a class="title" href="#">Item One Title</a>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </article>
                  <article class="media event">
                    <a class="pull-left date">
                      <p class="month">April</p>
                      <p class="day">23</p>
                    </a>
                    <div class="media-body">
                      <a class="title" href="#">Item Two Title</a>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </article>
                  <article class="media event">
                    <a class="pull-left date">
                      <p class="month">April</p>
                      <p class="day">23</p>
                    </a>
                    <div class="media-body">
                      <a class="title" href="#">Item Two Title</a>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </article>
                 
              </div>
            </div>
          </div>


          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel tile fixed_height_320">
              <div class="x_title">
                <h2>Quick Actions</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="dashboard-widget-content">
                  <ul class="quick-list">
                    <li><i class="fa fa-calendar-o"></i><a href="#">Open Calendar</a>
                    </li>
                    <li><i class="fa fa-envelope-o"></i><a href="#">Send Report</a>
                    </li>
                    <li><i class="fa fa-home"></i><a href="#">Add Member</a> </li>
                    <li><i class="fa fa-globe"></i><a href="#">Add Nation</a>
                    </li>
                    <li><i class="fa fa-users"></i><a href="#">View members</a> </li>
                    <li><i class="fa fa-user"></i><a href="#">My Profile</a>
                    </li>
                    <li><i class="fa fa-area-chart"></i><a href="#">Logout</a>
                    </li>
                  </ul>

                 
                </div>
              </div>
            </div>
          </div>

        </div>



		
		
            </div>
          </div>
        </div>
	</div> <!-- Container -->	
		

  

 <script>
 $('.grid').masonry({
  // options
  itemSelector: '.grid-item',
  columnWidth: 200
});
 </script>

