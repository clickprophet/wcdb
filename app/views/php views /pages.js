  routie({
      '/register/:id': function(id) {
          $('#contentDiv').load('views/register.html').show();
          $('#btnRegister').hide();
      },
      '/:code/provinces': function() {//show landing page
          $('#contentDiv').load('views/provinces.html').show();
      },
      '/:code/disciples': function() {//show landing page
          $('#contentDiv').load('views/disciples.html').show();
      },
      '/disciple/:id': function(id) {//show landing page
          $('#contentDiv').load('views/memberProfile.php?id='+id).show();
      },
      '/disciple/new': function() {//show landing page
          $('#contentDiv').load('views/memberAdd.php').show();
      },
      '/admin/users': function() {//show landing page
          $('#contentDiv').load('views/users.php').show();
      },
      '/admin/settings': function() {//show landing page
          $('#contentDiv').load('views/settings.php').show();
      },
      '/admin/backup': function() {//show landing page
          $('#contentDiv').load('views/backupDB.php').show();
      },
      '/admin/events': function() {//show landing page
          $('#contentDiv').load('views/events.php').show();
      },
      '/:code/localities': function() {//show landing page
          $('#contentDiv').load('views/localities.html').show();
      },
      '/:code/assemblies': function() {//show landing page
          $('#contentDiv').load('views/assemblies.html').show();
      },
      '/:code/settings': function(code) {//show landing page
          $('#contentDiv').load('views/nationSettings.html?code='+code).show();
      },
      '/:code/events': function() {//show landing page
          $('#contentDiv').load('views/events.html').show();
      },
      '/tools/excel_import': function() {//show landing page
          $('#contentDiv').load('views/importFromExcel.php').show();
      },
      '/users': function() {//show landing page
          $('#contentDiv').load('views/users.php').show();
      },
      '/user/:userId': function(userId) {//show landing page
          $('#contentDiv').load('views/userProfile.php?id='+userId).show();
      },
      '/nations': function() {//show landing page
          $('#contentDiv').load('views/nations.php').show();
      },
      '/nations/new': function() {//show landing page
          $('#contentDiv').load('views/nationAdd.php').show();
          $('#nationHome').html(`<a href="." ><i class="icon-home"></i></a>`);
      },
      '/': function() {//show landing page
          $('#contentDiv').load('views/globalDash.html').show();
      },
       '/:code': function(code) {//show landing page
          $('#contentDiv').load('views/nationProfile.php').show();
      },
      '/admin': function() {//show landing page
          $('#contentDiv').load('views/events_admin.html').show();
      },
      '/admin/event/:eventId': function(eventId) {//show landing page
          $('#contentDiv').load('views/events_admin_profile.html').show();
      }
  });


function openPage(p,q){
		
		if(p==1){
		var page="backhome";
		}
		else if(p==2){ var page="calendar";	}
		else if(p==3){ var page="localities";	}
		else if(p==4){ var page="nations";	}
		else if(p==5){ var page="membersj";	}
		else if(p==6){ var page="reportsx";	}
		else if(p==7){ var page="profile";	}
		else if(p==8){ var page="settings";	}
		else if(p==9){ var page="users";	}
		else if(p==10){ var page="Analytics";	}
		else if(p==11){ var page="importFromExcel";	}
		else if(p==12){ var page="nations_tiled";	}
		else if(p==13){ var page="backupDB";	}
		else if(p==14){ var page="userProfile";	}
		else if(p==15){ var page="nationProfile"; var nation=q;sessionStorage.setItem("nation", nation);}
		else if(p==16){ var page="inbox";	}
		else if(p==17){ var page="leaderProfile";	}
		else if(p==18){ var page="events";	}
		else if(p==19){ var page="memberAdd";	}
		else if(p==20){ var page="eventProfile";	}
		else if(p==21){ var page="globalDash";	}
		else if(p==22){ var page="nationAdd";	}
		else if(p==23){ var page="memberProfile";	}
		else if(p==24){ var page="provinceProfile";	}
		else if(p==25){ var page="pages";	}
		else if(p==26){ var page="userProfileView";	}

		if(q==0){
		var getPage="views/"+page+".php";
		}else if(p==40){ var getPage="views/glEvents.html";	
		}else{
			var getPage="views/"+page+".php?id="+q;
		}
		$("#loaderImg").show();
		$("#contentDiv").load(getPage).show();

	}