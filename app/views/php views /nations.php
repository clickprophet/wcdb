 <style type="text/css">
  #datatable-nations tr .links {
    display:none;
}

#datatable-nations tr:hover .links {
    display:block;   
}
</style>
        <div class="">
          <div class="page-title">
            <div class="title_left">
			
              <h3>
                    <a href="." type="button"><i class="fa fa-arrow-circle-left"></i></a>  Manage Nations
                    <small>
                        details of all nations registered
                    </small>
                </h3>
            </div>

            <div class="title_right">
            
			  <div class="col-md-7 col-sm-7 col-xs-12 form-group pull-right">
			  <div class="btn-group">
					<a onclick="routie('/nations')" class="btn btn-default btn-sm" ><i class="glyphicon glyphicon-refresh"></i></a>
					<a onclick="openPage(12)" class="btn btn-default btn-sm" ><i class="glyphicon glyphicon-equalizer"></i></a>
                    <button class="btn btn-success btn-sm" onclick="routie('/nations/new')"><i class="glyphicon glyphicon-plus"></i> New Nation</button>
                     <!-- <button class="btn btn-success btn-sm" data-toggle="modal"  data-target=".newNation"><i class="glyphicon glyphicon-plus"></i>Modal Test</button> -->
					<!--button class="btn btn-default btn-sm" data-toggle="modal" data-target=".newLocality"><i class="glyphicon glyphicon-plus"></i> Locality</button>
					<button class="btn btn-default btn-sm" data-toggle="modal" data-target=".newMember"><i class="glyphicon glyphicon-plus"></i> Member</button-->	
				</div>
			  </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">


                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <!--div class="x_title">
                        <h2>Nations <small>Details</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">refresh</a>
                              </li>
                              <li><a href="#">add new</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div-->
                      <div class="x_content">

                        <p class="text-muted font-13 m-b-30">
                          "Therefore go and make disciples of all nations..." Matt 28:19
                        </p>
                        <table id="datatable-nations" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Nation</th>
                              <th>Region</th>
                              <th>Members</th>
                              <th>Localities</th>
                              <th>House Churches</th>
                              <th>Leader</th>
                              <th></th>
                              <th>Phone</th>
                              <th>E-mail</th>
                              <th>Actions</th>
                            </tr>
                          </thead>
                         
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /page content -->

  <!--modal forms-->
    <!-- stats -->     
        
<div id="editStats" class="modal fade editStats" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" ><div class="lblHeading">Edit nation stats</div></h4>
              </div>
              <div class="modal-body">
                <div class="x_panel">
            
                  <div class="x_content">
                    <br />
                     <div class="col-xs-4">
                          <form id="nationStatsForm" class="form-horizontal form-label-left input_mask" action="" method="post">
                           <div class="form-group">
                              <label class="control-label col-md-4 col-sm-4 col-xs-12">Disciples<span class="required">*</span></label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="number" class="form-control" name="statsTotal" id="statsTotal" placeholder="0" required="required">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-4 col-sm-64col-xs-12">Leaders<span class="required">*</span></label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="number" class="form-control" name="statsLeaders" id="statsLeaders" placeholder="0" required="required">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-4 col-sm-4 col-xs-12">Localities<span class="required">*</span></label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="number" class="form-control" name="statsLoc" id="statsLoc" placeholder="0" required="required">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-4 col-sm-4 col-xs-12">House Churches<span class="required">*</span></label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="number" class="form-control" name="statsHC" id="statsHC" placeholder="0" required="required">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-4 col-sm-4 col-xs-12">As at Date<span class="required">*</span></label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="datepicker" class="form-control" name="statsDate" id="statsDate" placeholder="0" required="required">
                              </div>
                            </div>
                            <script>
                            $( function() {
                              $( "#statsDate" ).datepicker({ 
                                changeMonth: true,
                                changeYear: true,
                                dateFormat:"yy-mm-dd",
                                showAnim:"clip",
                                showButtonPanel: true,
                              });
                            } );
                            </script>
                            <div class="form-group">
                              <label class="control-label col-md-4 col-sm-4 col-xs-12"></label>
                              <div class="col-md-4 col-sm-4 col-xs-12" id="provReportBtn"></div>
                              <div class="col-md-4 col-sm-4 col-xs-12" id="provReportUpdateBtn"></div>
                            </div>

                          </form>
                          </div>
                          <div class="col-xs-8 natdates">
                             <table id="natDates" class="table  table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                              <thead>
                              <tr>
                                <!--th>id</th-->
                                <th>Date</th>
                                <th>Disciples</th>
                                <th>House Churches</th>
                                <th>Localities</th>
                                <th>Leaders</th>
                                <th>Actions</th>
                              </tr>
                              </thead>
                            </table>
                          </div>
                        </div>
                  </div>
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <button onclick="saveReport()" id="submit" class="btn btn-primary" value="AddNation" data-dismiss="modal">Submit</button>
                </div>
        
              </div>
            </div>
          </div>
        

<!-- Add new nation modal -->
<div id="newNation" class="modal fade newNation" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" ><div class="lblHeading">Create New Nation</div></h4>
              </div>
              <div class="modal-body">
                <div class="x_panel">
            
                  <div class="x_content">

                        <!-- Smart Wizard -->
                        <div id="nationWizard" class="form_wizard wizard_horizontal">
                          <ul class="wizard_steps">
                            <li>
                              <a href="#step-1">
                                <span class="step_no">1</span>
                                <span class="step_descr">
                                                  Step 1<br />
                                                  <small>Select nation</small>
                                              </span>
                              </a>
                            </li>
                            <li>
                              <a href="#step-2">
                                <span class="step_no">2</span>
                                <span class="step_descr">
                                                  Step 2<br />
                                                  <small>Enter Nation Details</small>
                                              </span>
                              </a>
                            </li>
                            <!--li>
                              <a href="#step-3">
                                <span class="step_no">3</span>
                                <span class="step_descr">
                                                  Step 3<br />
                                                  <small>Create Nation Leader</small>
                                              </span>
                              </a>
                            </li-->
                            <li>
                              <a href="#step-3">
                                <span class="step_no">3</span>
                                <span class="step_descr">
                                                  Step 3<br />
                                                  <small>Finish</small>
                                              </span>
                              </a>
                            </li>
                          </ul>
                
                          <div id="step-1">
                            <form name="regForm" id="step1" enctype="multipart/form-data" method="post" action="" novalidate>
                            <div class="item col-md-3 col-sm-3 col-xs-12 well" style="">
                              <p>
                                the map shows countries already registered in the database, kindly select the <strong>new</strong> nation you wish to add from  below and click next:
                             </p>
                             <label class=""> Choose Country</label><br>
                                <div class="item col-md-12 col-sm-6 col-xs-12">
                                   <select type="select" id="country" name="country" class="form-control" required="required" value="">

                                    </select>
                              </div> 
                            </div>
                   <div class="item col-md-9 col-sm-9 col-xs-12">
                   <h2 class="StepTitle">Select Nation on Map</h2>
                  
                      
                    <div id="world-map-gdp1" style="height:300px;">
                      
                    </div>
                   </div>
                  </form>
                </div>
              
              <div id="step-2">
                  <label  class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInputFile"></label>
                   <div class="item col-md-9 col-sm-9 col-xs-12">
                   <h2 class="StepTitle"><span id="ncountryCode"></span> : Nation Information</h2>
                    <input type="hidden"  id="ncountryCode" name="ncountryCode" value="" required="required"/>
                    </div>
                   <form id="nationAddForm" action="" enctype="multipart/form-data" method="post" role="form" class="form-horizontal form-label-left" novalidate>
                   <div class="item form-group">
                     <label class="control-label col-md-3 col-sm-3 col-xs-12">Region Name  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                      <select name="nregion" id="nregion" class="form-control nregions" required="required">
                        <option>Choose region</option>
                        
                      </select>
                      </div>
                      <script>
                        $('#nregion').change(function() {
                          var value=$("#nregion option:selected").val();
                          var name=$("#nregion option:selected").text();
                          $("#finishRegion").html(name);
                          $("#finishRegionChk").html("<i class='icon-check green'></i>");

                        });
                      </script>
                    <span><a href="#"  data-toggle="modal" data-target=".newRegion" id="newRegionBtn" class="btn btn-success btn-sm"><i class="icon-note"></i> New Region</a></span>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Ministry Start Date<span class="required">*</span>
                    </label>
                    <div class="item col-md-6 col-sm-9 col-xs-12">
                    <input  id="nbirthday" name="nbirthday" class="date-picker form-control col-md-7 col-xs-12"  type="text" required="required" >
                    </div>
                  </div>
                    <script>
                    $( function() {
                      $( "#nbirthday" ).datepicker({ 
                        changeMonth: true,
                        changeYear: true,
                        dateFormat:"yy-mm-dd",
                        showAnim:"clip",
                        onSelect: function (date) {
                          $("#finishDate").html(date);
                          $("#finishDateChk").html("<i class='icon-check green'></i>");
                        },
                    });
                    } );
                    </script>
                  
                  <div class="item form-group">
                     <label class=" control-label col-md-3 col-sm-3 col-xs-12">Leader Name  <span class="required">*</span></label>
                      <div class="item col-md-6 col-sm-9 col-xs-12">
                       <select name="nationLeader" id="nationLeader" class="form-control leaders"
                        required="required">
                       <!--  <option>Select Leader..</option> --> 
                      </select>
                      </div>

                        <script type="text/javascript">
                        $('#nationLeader').html('<option>Choose Nation Leader</option>'); 
                        var o = $("#nationLeader");
                            $.getJSON("../app/api/funcs.php?fn=fetchmembers", function(response) {
                                 $.each(response, function() {
                                 o.append($("<option />").val(this.id).text(this.firstName+' '+this.lastName));
                             });
                          });
                      </script>
                      <script>
                        $('#nationLeader').change(function() {
                          var value=$("#nationLeader option:selected").val();
                          var name=$("#nationLeader option:selected").text();
                          $("#finishLeader").html(name);
                          $("#finishLeaderChk").html("<i class='icon-check green'></i>");

                        });
                      </script>
                    <!--span><p class="small">If the leader's name is not on the list proceed to create the leader's record on the next page</p></span-->
                    <span><a href="#"  data-toggle="modal" data-target=".newMember" id="newRegionBtn" class="btn btn-success btn-sm"><i class="icon-note"></i> Create Leader</a></span>
                  </div>

                  <div class="item form-group">
                  <label class=" control-label col-md-3 col-sm-3 col-xs-12">Number Of Desciples<span class="required">*</span></label>
                    <div class="item col-md-6 col-sm-9 col-xs-12">
                     <input type="text"name="numberOfDesciples" id="numberOfDesciples"
                      class="form-control leaders" required="required" placeholder="Number Of Desciples">
                      </div>
                      <script>
                        $('#numberOfDesciples').change(function() {
                          var value=$("#numberOfDesciples").val();
                          var name =$("#numberOfDesciples").val();
                          $("#finishnumberOfDesciples").html(name);
                          $("#finishnumberOfDesciplesChk").html("<i class='icon-check green'></i>");

                        });
                      </script>
                  </div>
                </form>
                </div>
                
                         
                  <div id="step-3">
                  <div class="col-md-3 col-sm-12 col-xs-12"></div>
                  
                  <div class="col-md-6 col-sm-12 col-xs-12">
                    <h2 class="StepTitle">Finish Nation creation</h2>
                    <p>Check and confirm the following details for the nation. If satisfied, click Finish to proceed to create the nation </p>
                    <table class="table table-stripped projects">
                      <thead>
                        <th style="width: 50%">field</th>
                        <th>Value</th>
                        <th>Check</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Nation</td>
                          <td><div id="finishNation"></div></td>
                          <td><div id="finishNationChk"><i class="icon-close red"></i></div></td>
                        </tr>
                        <tr>
                          <td>Region</td>
                          <td><div id="finishRegion"></div></td>
                          <td><div id="finishRegionChk"><i class="icon-close red"></i></div></td>
                        </tr>
                        <tr>
                          <td>Date Started</td>
                          <td><div id="finishDate"></div></td>
                          <td><div id="finishDateChk"><i class="fa fa-warning" style="color:orange;" ></i></div></td>
                        </tr>
                        <tr>
                          <td>Leader</td>
                          <td><div id="finishLeader"></div></td>
                          <td><div id="finishLeaderChk"><i class="icon-close red"></i></div></td>
                        </tr>
                        <tr>
                          <td>Number Of Desciples</td>
                          <td><div id="finishnumberOfDesciples"></div></td>
                          <td><div id="finishnumberOfDesciplesChk"><i class="icon-close red"></i></div></td>
                        </tr>
                      </tbody>
                    </table>

                    <span id="iloading" style="display:none;" ><img src="images/loading.gif" alt="loading..." width="30px" /></span>
                   </div> 
                          </div>

                        </div>
                        <!-- End SmartWizard Content -->



                   </div>
                 </div>
                </div>
               
              </div>
            </div>
          </div>

<!--/ Add new nation mdal-->

   <!--/ Modal Forms -->


        <script>
           $(document).ready(function() {
              //localities table
              $('#datatable-nations').DataTable({
                "ajax": {
              "url": "api/funcs.php?fn=fetchNations",
              "dataSrc": ""
              },
              buttons: [
                  {
                      text: 'New Nation',
                      action: function ( e, dt, node, config ) {
                          openPage(22);
                      }
                  }
              ],
              "columns": [
                { "data": "name" },
                { "data": "region" },
                { "data": "members" },
                { "data": "localities" },
                { "data": "housechurches" },
                { "data": "leaderLinked" },
                { "data": "image" },
                { "data": "phone" },
                { "data": "email" },
                { "data": "code" }
              ],
              columnDefs: [{
                   targets: 9,
                   render: function(data, type, full, meta){
                      if(type === 'display'){
                         data = `<div class="button-group">
                         <a onclick="setNation('${data}')" data-toggle="modal" data-target=".editStats" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> </a> 
                             <a onclick="routie('/${data}')" class="btn btn-xs btn-primary"><i class="fa fa-flag"></i> </a> 
                             <a onclick="deleteNation('${data}')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a>
                             </div>`;                     
                      }
                       
                      return data;
                   }
                }
                ],
                dom: "Bfrtip",
                      buttons: [{
                        extend: "copy",
                        className: "btn-sm"
                      }, {
                        extend: "csv",
                        className: "btn-sm"
                      }, {
                        extend: "excel",
                        className: "btn-sm"
                      }, {
                        extend: "pdf",
                        className: "btn-sm"
                      }, {
                        extend: "print",
                        className: "btn-sm"
                      }
              ]
              });
            });

           function setNation(nation){
              //initialise new modal
              $("#nationStatsForm")[0].reset();
              $('.lblHeading').html('Edit nation stats : <strong>'+nation+'</strong>');
              $('.natdates').html(`<table id="natDates" class="table  table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                              <thead>
                              <tr>
                                <!--th>id</th-->
                                <th>Date</th>
                                <th>Disciples</th>
                                <th>House Churches</th>
                                <th>Localities</th>
                                <th>Leaders</th>
                                <th>Actions</th>
                              </tr>
                              </thead>
                            </table>`);
              
              //get top report
              $.getJSON('api/api.php/records/cmfi_nation_stats/?filter=code,eq,'+nation,
                function(data){
                  console.log(data);
                  var n=data.records[0];
                  if(data.records.length!=0)
                  {
                      nationStatsForm.statsTotal.value=n.disciples;
                      nationStatsForm.statsDate.value=n.date;
                      nationStatsForm.statsLeaders.value=n.leaders;
                      nationStatsForm.statsHC.value=n.housechurches;
                      nationStatsForm.statsLoc.value=n.localities;
                      $('.modal-footer').html(`
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <button onclick="addNewReport('${nation}')" id="submit" class="btn btn-success" value="submit"><i class="fa fa-plus-circle"></i> Save as New</button>
                      <button onclick="saveReport(${n.id})" id="submit" class="btn btn-primary" value="submit" data-dismiss="modal"><i class="fa fa-check-circle"></i> Save</button>
                    `);

                       //---------        NATION REPORTS    ---------------//
                     
                      $('#natDates').DataTable({
                        "ajax": {
                          "url": "api/api.php/records/cmfi_nation_stats/?filter=code,eq,"+nation,
                          "dataSrc": "records"
                          },
                        "bFilter": false,
                        "columns": [
                          { "data": "date","width":120 },
                          { "data": "disciples" },
                          { "data": "housechurches","width":50 },
                          { "data": "localities" },
                          { "data": "leaders" },
                          { "data": "id"}
                        ],
                        columnDefs: [{
                       targets: 5,
                       render: function(data, type, full, meta){
                          if(type === 'display'){
                             data = `<div class="button-group">
                                 <a onclick="editNationReport(${data})" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> </a> 
                                 <a onclick="deleteNationReport(${data})" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a>
                                 </div>`;                     
                          }
                           
                          return data;
                       }
                    }
                    ],
                        "aLengthMenu":[[5,10,25,50,-1],[5,10,25,50,"All"]],
                        "pageLength":5
                              
                      });

                }
                else{
                  $('.modal-footer').html(`
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <button onclick="addNewReport('${nation}')" id="submit" class="btn btn-success" value="submit" data-dismiss="modal"><i class="fa fa-plus-circle"></i> Add New Stats</button>
                `);
                  
                }
                });



           }

           function editNationReport(id){
            console.log("Editing report id: "+id);
            $.getJSON("api/api.php/records/cmfi_nation_stats/"+id, function(res) {
              $('#statsTotal').val(res.disciples);
              $('#statsLeaders').val(res.leaders);
              $('#statsLoc').val(res.localities);
              $('#statsHC').val(res.housechurches);
              $('#statsDate').val(res.date);
              $('.modal-footer').html(`
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <button onclick="saveReport(${id})" id="submit" class="btn btn-success" value="submit" data-dismiss="modal"><i class="fa fa-check-circle"></i> Save</button>
                `);
                      
            });
          }

          function saveReport(id){
            $.ajax({
              url:'api/api.php/records/cmfi_nation_stats/'+id,
              type:'PUT',
              data:{
                  disciples:nationStatsForm.statsTotal.value,
                  date: nationStatsForm.statsDate.value,
                  leaders: nationStatsForm.statsLeaders.value,
                  housechurches: nationStatsForm.statsHC.value,
                  localities:nationStatsForm.statsLoc.value
              },
              success: function(data){
                console.log(data);
                successNote(sresponse,'Report saved successfully !');
                $('#natDates').DataTable().ajax.reload(); //refresh dates table
                $('#datatable-nations').DataTable().ajax.reload(); //refresh nations table
              },
              error: function(data){
                console.log(data);
                errorNote(fresponse,'Oops something went wrong! '+ data);
              }
            });
          }


          function addNewReport(nation){
            $.ajax({
              url:'api/api.php/records/cmfi_nation_stats/',
              type:'POST',
              data:{
                  code:nation,
                  disciples:nationStatsForm.statsTotal.value,
                  date: nationStatsForm.statsDate.value,
                  leaders: nationStatsForm.statsLeaders.value,
                  housechurches: nationStatsForm.statsHC.value,
                  localities:nationStatsForm.statsLoc.value
              },
              success: function(data){
                console.log(data);
                successNote(sresponse,'Report added successfully !');
                
                $('#datatable-nations').DataTable().ajax.reload(); //refresh nations table
                //$('#natDates').DataTable().ajax.reload(); //refresh dates table
              },
              error: function(data){
                console.log(data);
                errorNote(fresponse,'Oops something went wrong! '+ data);
              }
            });
          }

      </script>
        <script type="text/javascript">
          
          $("#loaderImg").hide(); //hide loader image
        </script>
       
