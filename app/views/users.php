
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <script src="js/custom-sm.js"></script>
   <!-- switchery -->
  <script src="js/switchery/switchery.min.js"></script> 

        <div class="">
          <div class="clearfix"></div>
            <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>User <small>Details</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content ">
                        <p class="text-muted font-13 m-b-30">
                        </p>
                        <table id="datatable-users" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
							                <th></th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Admin</th>
                              <th>Active</th>
							                <th>LastSeen</th>
                              <!--th>Records</th-->
                              <th>Visits</th>
							                <th>Actions</th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                </div>
              </div>
          </div>
            
            <!-- /page content -->
			
	<script>
     $(document).ready(function() {
      var nation = sessionStorage.getItem("nation");
     
        //users table
        $('#datatable-users').DataTable({
          "ajax": {
        "url": "api/funcs.php?fn=fetchAllUsers&n="+nation,
        "dataSrc": ""
        },
        "columns": [
          { "data": "image" },
          { "data": "name" },
          { "data": "email"},
          { "data": "phone"},
          { "data": "admin"},
          { "data": "active_"},
          { "data": "lastSeen"},
          { "data": "visits"},
          { "data": "actions" }
        ],
          dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                }, {
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }
        ]
        });

      });

</script>

<script type="text/javascript">
  $(document).ready(function() {
    $("#loaderImg").hide(); //hide loader image
  });
</script>
