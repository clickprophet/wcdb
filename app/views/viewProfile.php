<?php


require_once("../config-small.php"); //loads the system configuration settings
//require_once("../resources/common/funcs.php");

$userId=$_REQUEST['userId'];
$userdetails = fetchUserDetails($_REQUEST['userId']);
$recentTasks=getRecentTasks($userId);
if(!$recentTasks){
echo '';
}
$recentEvents=getRecentEvents($userId);

$count=0;
$message[]="";
$i=0;
//compute profile completeness

//if(!empty($userdetails['user_name'])){$count++;}else{$i++; $message[$i]="username missing";} 
//if(!empty($userdetails['display_name'])){$count++;}else{$i++; $message[$i]="displayname missing";} 
if(!empty($userdetails['email'])){$count++;} else{$i++; $message[$i]="email missing";}
if(!empty($userdetails['title'])){$count++;} else{$i++; $message[$i]="job title missing";}
if(!empty($userdetails['image'])){$count++;} else{$i++; $message[$i]="set personal image";}
if(!empty($userdetails['facebook_profile'])){$count++;} else{$i++; $message[$i]="set facebook profile link";}
if(!empty($userdetails['twitter_profile'])){$count++;} else{$i++; $message[$i]="set twitter profile link";}
if(!empty($userdetails['linkedin_profile'])){$count++;} else{$i++; $message[$i]="set linkedin profile link";}
if(!empty($userdetails['pinterest_profile'])){$count++;} else{$i++; $message[$i]="set google profile link";}
if(!empty($userdetails['userFirst'])){$count++;} else{$i++; $message[$i]="first name missing";}
if(!empty($userdetails['userLast'])){$count++;} else{$i++; $message[$i]="surname missing";}
if(!empty($userdetails['weatherLoc'])){$count++;} else{$i++; $message[$i]="set your current location";}
if(!empty($userdetails['qualification'])){$count++;} else{$i++; $message[$i]="set highest qualification";}
if(!empty($userdetails['gender'])){$count++;} else{$i++; $message[$i]="gender missing";}
if(!empty($userdetails['userNotes'])){$count++;} else{$i++; $message[$i]="set profile summary";} 
if(!empty($userdetails['phone'])){$count++;} else{$i++; $message[$i]="phone missing";}
if(!empty($userdetails['DOB'])){$count++;} else{$i++; $message[$i]="date of birth missing";}

$percComplete=substr((($count/17)*100),0,4);

// Update User Details
	/*<?php include_once('update_profile.php'); ?>*/
	
	if (isset($_POST['submit2']) && $_POST['submit2'] == 'updateUserDetails') {
		$linkedin=$mysqli->real_escape_string($_POST['linkedin']);
		$facebook=$mysqli->real_escape_string($_POST['facebook']);
		$google=$mysqli->real_escape_string($_POST['google']);
		$twitter=$mysqli->real_escape_string($_POST['twitter']);
		$weatherLoc=$mysqli->real_escape_string($_POST['city']);
		$email=$mysqli->real_escape_string($_POST['email']);
		$phone=$mysqli->real_escape_string($_POST['phone']);
		
		updateEmail($userId,$email);
		if(updateUserContacts($userId,$linkedin,$twitter,$facebook,$google,$email,$phone,$weatherLoc)>0){
		$msgBox = alertBox($taskMarkedCompMsg, "<i class='fa fa-check-square'></i>", "success");
			
		} else {
			$msgBox = alertBox($taskCompErrorMsg, "<i class='fa fa-times-circle'></i>", "danger");
		}
	}

//get company details
$compQry="SELECT * FROM gantt_companies WHERE id=".$loggedInUser->company;
$res=mysqli_query($mysqli,$compQry);
$myCompany=mysqli_fetch_assoc($res);
	
?>
<script type="text/javascript">

$(document).ready(function() { 
	var options = { 
			target: '#output',   // target element(s) to be updated with server response 
			beforeSubmit: beforeSubmit,  // pre-submit callback 
			success: afterSuccess,  // post-submit callback 
			resetForm: true        // reset the form after successful submit 
		}; 
		
	 $('#MyUploadForm').submit(function() { 
			$(this).ajaxSubmit(options);  			
			// always return false to prevent standard browser submit and page navigation 
			return false; 
		}); 
}); 

function afterSuccess(success)
{
	$('#submit-btn').show(); //hide submit button
	$('#loading-img').hide(); //hide submit button
	$('#current-img').hide(); //hide currentimage

}

//function to check file size before uploading.
function beforeSubmit(){
    //check whether browser fully supports all File API
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		
		if( !$('#imageInput').val()) //check empty input filed
		{
			$("#output").html("Are you kidding me?");
			return false
		}
		
		var fsize = $('#imageInput')[0].files[0].size; //get file size
		var ftype = $('#imageInput')[0].files[0].type; // get file type
		

		//allow only valid image file types 
		switch(ftype)
        {
            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                break;
            default:
                $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
				return false
        }
		
		//Allowed file size is less than 1 MB (1048576)
		if(fsize>1048576) 
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
			return false
		}
				
		$('#submit-btn').hide(); //hide submit button
		$('#loading-img').show(); //hide submit button
		$("#output").html("");  
	}
	else
	{
		//Output error to older browsers that do not support HTML5 File API
		$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
		return false;
	}
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

</script>

<style>

#upload-wrapper input[type=file] {
	border: 1px solid #DDD;
	padding: 3px;
	background: #FFF;
	border-radius: 3px;
	width:140px;
}
#upload-wrapper #submit-btn {
	border: none;
	margin-top:2px;
	padding: 5px;
	background: #61BAE4;
	border-radius: 3px;
	color: #FFF;
}
</style>

<div class="innerLR">

	<!-- Widget -->
	<div class="widget widget-tabs widget-tabs-gray widget-tabs-double-2 border-bottom-none">
	
		<!-- Widget heading -->
		<div class="widget-head">
			<ul>
				<li class="active"><a class="glyphicons display" href="#overview" data-toggle="tab"><i></i>Overview</a></li>
				<li><a class="glyphicons edit" href="#edit-account" data-toggle="tab"><i></i>Edit account</a></li>
				<li><a class="glyphicons luggage" href="#projects" data-toggle="tab"><i></i>Projects</a></li>
                <li><a class="glyphicons certificate" href="#skills" data-toggle="tab"><i></i>Observe me</a></li>
                <li><a class="glyphicons group" href="#teams" data-toggle="tab"><i></i>Teams</a></li>
			</ul>
		</div>
		<!-- // Widget heading END -->
		
		<div class="widget-body">
		
			
				<div class="tab-content">
				
					<div class="tab-pane active widget-body-regular padding-none" id="overview">
					
						<div class="row-fluid row-merge">
							<div class="span3 center innerL innerTB">
							
								<div class="innerR">
									<!-- Profile Photo -->
									 <div id="upload-wrapper">
										<div align="left">
											<form action="upload.php" method="post" enctype="multipart/form-data" id="MyUploadForm">
												
												<input  name="image_file" id="imageInput" type="file" />
												<input  type="submit"  id="submit-btn" class="glyphicons upload" value="Upload" />
											
											</form>
											<div id="output">
												<img src="images/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
												<a href="" class="thumb"><img src="images/users/<?php echo $userdetails['image']?>" alt="Profile"  width="200px" height="300px" id="current-img"/></a>
											</div>   
										  </div>             
										</div>
									
									<div class="separator bottom"></div>
									<!-- // Profile Photo END -->
									
									<!-- Social Icons -->
									<a href="<?php echo $userdetails['facebook_profile']; ?>" class="glyphicons standard primary facebook"><i></i></a>
									<a href="<?php echo $userdetails['twitter_profile']; ?>" class="glyphicons standard twitter"><i></i></a>
									<a href="<?php echo$userdetails['linkedin_profile']; ?>" class="glyphicons standard linked_in"><i></i></a>
									<div class="clearfix separator bottom"></div>
									<!-- // Social Icons END -->
									
									<!-- Twitter Section -->
									<h5 class="glyphicons single tag"><i></i> <?php echo $userdetails['title'];?></h5>
									<section class="twitter-feed">
										<div class="row-fluid">
											<div class="span12">
												<div class="tweet">Last Sign In<span class="label label-inverse"><?php echo dateFormat(date('Y-m-d h:i:s', $userdetails['last_sign_in_stamp']))." at ".timeFormat(date('Y-m-d h:i:s',  $userdetails['last_sign_in_stamp']));?></span></div>
											</div>
											<div class="clearfix"></div>
										</div>
									</section>
									<!-- Twitter Section END -->
									
								</div>
								
							</div>
							
							<form class="form-horizontal">
							<div class="span9 containerBg innerTB">
							
								<div class="innerLR">
									<div class="row-fluid innerB">
										<div class="span7">
										
											<!-- About -->
											<div class="widget widget-heading-simple widget-body-gray margin-none">
												<div class="widget-head"><h4 class="heading glyphicons user"><i></i><?php echo $userdetails['userFirst'].' '.$userdetails['userLast'];?></h4></div>
												<div class="widget-body">
													<p><?php echo $userdetails['userNotes'];?></p>
												</div>
											</div>
											<!-- // About END -->
										
										</div>
										<div class="span5">

											<!-- Bio -->
											<div class="widget widget-heading-simple widget-body-gray margin-none">
												<div class="widget-head"><h4 class="heading glyphicons calendar"><i></i>Bio <span>Personal information</span></h4></div>
												<div class="widget-body">
													<ul class="unstyled icons margin-none">
														<li class="glyphicons birthday_cake"><i></i> <span class="label"><?php echo $userdetails['DOB']; ?></span> </li>
														<li class="glyphicons tie"><i></i> Working at <a href="<?php echo $myCompany['Website'];?>"><?php echo $myCompany['name'];?></a></li>
														<li class="glyphicons certificate"><i></i><?php echo $userdetails['qualification']?> </li>
														<li class="glyphicons microphone"><i></i> English </li>
													</ul>
												</div>
											</div>
											<!-- // Bio END -->
											
										</div>
									</div>
									<div class="row-fluid">
										<div class="span7">
										
											<!-- Latest Orders/List Widget -->
											<div class="widget widget-heading-simple widget-body-gray" data-toggle="collapse-widget">
											
												<!-- Widget Heading -->
												<div class="widget-head">
													<h4 class="heading glyphicons briefcase"><i></i><?php echo "Recent Goals"; ?></h4>
													<a href="" class="details pull-right">view all</a>
												</div>
												<!-- // Widget Heading -->
												
												<div class="widget-body list products">
													<ul>
														<?php
														$q="SELECT 	catId,catName,	UNIX_TIMESTAMP(catDate) AS orderDate FROM categories
														WHERE
															userId = ".$userId." AND
															isActive = 1
														ORDER BY
															orderDate DESC LIMIT 5";
														$re=$mysqli->query($q);
														if(mysqli_num_rows($re)>1){
														While($frow3=mysqli_fetch_array($re)) { ?>
						<li><span class="title"><a href="index.php?page=viewKPA&catId=<?php echo $frow3['catId']; ?>"><?php echo clean(substr($frow3['catName'],0,30)); ?></a></span></li>
						<?php }
							}					?>
																					
													</ul>
												</div>
											</div>
											<!-- // Latest Orders/List Widget END -->
											
                                            <?php
											if($percComplete<100){
											echo'<div class="alert alert-primary">
												<a class="close" data-dismiss="alert">&times;</a>
												<span class="glyphicon icon-warning-sign"><i></i></span><p>Your profile looks incomplete. Please see the pending items and try to complete them. </p>
											</div>';
											}
											?>
										</div>
										<div class="span5">
											
											<div class="widget widget-heading-simple widget-body-gray" data-toggle="collapse-widget">
				
												<!-- Widget Heading -->
												<div class="widget-head">
													<h4 class="heading glyphicons history"><i></i><?php echo $recentActivityTitle; ?></h4>
													<a href="" class="details pull-right">view all</a>
												</div>
												<!-- // Widget Heading END -->
												
												<div class="widget-body list">
													<ul>
													<?php
						if(!empty($recentTasks)){foreach ($recentTasks as $task){
							echo '<li><span><a href="index.php?page=viewTask&taskId='.$task['taskId'].'">'.clean($task['taskTitle']).'</a></span></li>';
							}
						}
						if(!empty($recentEvents)){foreach ($recentEvents as $event) {
							echo '<li><span>'.clean($event['eventTitle']).'</span></li>';
							} 
						}
					?>
														
																												
													</ul>
												</div>
											</div>
											
											<div class="widget">
    <div class="widget-head progress progress-primary" id="widget-progress-bar">
        <div class="bar" style="width: <?php echo $percComplete.'%';?>;">Profile <strong>Completeness</strong> - <strong class="steps-percent"><?php echo $percComplete.'%';?></strong>
        </div>
    </div>
    <div class="widget-body">
        	<h4>Pending Updates</h4>
       <ul>
       <?php  
	   		$x=0;
       		while($x<17){
				$x++;
				if(isset($message[$x])){
       			echo'<li>'.$message[$x].'</li>';
				}
			}
	   ?>
	    </ul>
    </div>
</div>
											
										</div>
									</div>
								</div>
								
							</div>
							</form>
						</div>
					
					</div>
				
					<!-- Tab content -->
					<div class="tab-pane widget-body-regular" id="edit-account">
						<div class="widget widget-tabs widget-tabs-gray widget-tabs-vertical row-fluid row-merge margin-none widget-body-white">
							<!-- Widget heading -->
							<div class="widget-head span3">
								<ul>
									<li class="active"><a class="glyphicons pencil" href="#account-details" data-toggle="tab"><i></i>Account details</a></li>
									<li><a class="glyphicons settings" href="#account-settings" data-toggle="tab"><i></i>Account settings</a></li>
                                    
									<li><a class="glyphicons eye_open" href="#privacy-settings" data-toggle="tab"><i></i>Privacy settings</a></li>
								</ul>
							</div>
							<!-- // Widget heading END -->
							
							<div class="widget-body span9">
							
								<div class="tab-content">
								<div class="tab-pane active" id="account-details">
								<!--<form action="" method="post">-->
								<!-- Row -->
								<div class="row-fluid">
								
								
									<!-- Column -->
									<div class="span6">
								
										<!-- Group -->
										<div class="control-group">
											<label class="control-label">First name</label>
											<div class="controls">
												<input type="text" id="firstName" name="firstName" value="<?php echo $userdetails['userFirst']; ?>" class="span10" />
												<span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="First name is mandatory"><i></i></span>
											</div>
										</div>
										<!-- // Group END -->
										
										<!-- Group -->
										<div class="control-group">
											<label class="control-label">Last name</label>
											<div class="controls">
												<input type="text" id="lastName" name="lastName" value="<?php echo $userdetails['userLast']; ?>" class="span10" />
												<span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="Last name is mandatory"><i></i></span>
											</div>
										</div>
										<!-- // Group END -->
										
										<!-- Group -->
										<div class="control-group">
											<label class="control-label">Date of birth</label>
											<div class="controls">
												<div class="input-append" >
													<input type="text"  name="dob" id="3datetimepicker" class="span12" data-date-format="yyyy-mm-dd" value="<?php echo dateformat($userdetails['DOB']); ?>" />
													<span class="add-on glyphicons calendar"><i></i></span>
												</div>
											</div>
										</div>
										<!-- // Group END -->
										
									</div>
									<!-- // Column END -->
									
									<!-- Column -->
									<div class="span6">
									
										<!-- Group -->
										<div class="control-group">
											<label class="control-label">Gender</label>
											<div class="controls">
												<select class="span12" id="gender" name="gender">
													<option value="Male">Male</option>
													<option value="Female">Female</option>
												</select>
											</div>
										</div>
										<!-- // Group END -->
										
										<!-- Group -->
										<div class="control-group" style="display:none">
											<label class="control-label">User Id</label>
											<div class="controls">
												<input type="text" id="id" name="id" value="<?php echo $userdetails['id']; ?>" class="input-mini" />
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								
								<div class="separator line bottom"></div>
								
								<!-- Group -->
								<div class="control-group row-fluid">
									<label class="control-label">About me</label>
									<div class="controls">
                                    <ul id="mustHaveId-wysihtml5-toolbar" class="wysihtml5-toolbar"><li class="dropdown"><a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-font icon-white"></i>&nbsp;<span class="current-font">Normal text</span>&nbsp;<b class="caret"></b></a><ul class="dropdown-menu"><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="div" href="javascript:;" unselectable="on">Normal text</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h1" href="javascript:;" unselectable="on">Heading 1</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h2" href="javascript:;" unselectable="on">Heading 2</a></li></ul></li><li><div class="btn-group"><a class="btn btn-primary" data-wysihtml5-command="bold" title="CTRL+B" href="javascript:;" unselectable="on">Bold</a><a class="btn btn-primary" data-wysihtml5-command="italic" title="CTRL+I" href="javascript:;" unselectable="on">Italic</a></div></li><li><div class="btn-group"><a class="btn btn-primary" data-wysihtml5-command="insertUnorderedList" title="Unordered List" href="javascript:;" unselectable="on"><i class="icon-list icon-white"></i></a><a class="btn btn-primary" data-wysihtml5-command="insertOrderedList" title="Ordered List" href="javascript:;" unselectable="on"><i class="icon-th-list icon-white"></i></a><a class="btn btn-primary" data-wysihtml5-command="Outdent" title="Outdent" href="javascript:;" unselectable="on"><i class="icon-indent-right icon-white"></i></a><a class="btn btn-primary" data-wysihtml5-command="Indent" title="Indent" href="javascript:;" unselectable="on"><i class="icon-indent-left icon-white"></i></a></div></li><li><div class="bootstrap-wysihtml5-insert-link-modal modal hide fade" data-backdrop="false"><div class="modal-header"><a class="close" data-dismiss="modal">×</a><h3>Insert Link</h3></div><div class="modal-body"><input value="http://" class="bootstrap-wysihtml5-insert-link-url input-xlarge"></div><div class="modal-footer"><a href="#" class="btn" data-dismiss="modal">Cancel</a><a href="#" class="btn btn-primary" data-dismiss="modal">Insert link</a></div></div><a class="btn btn-primary" data-wysihtml5-command="createLink" title="Link" href="javascript:;" unselectable="on"><i class="icon-share icon-white"></i></a></li><li><div class="bootstrap-wysihtml5-insert-image-modal modal hide fade" data-backdrop="false"><div class="modal-header"><a class="close" data-dismiss="modal">×</a><h3>Insert Image</h3></div><div class="modal-body"><input value="http://" class="bootstrap-wysihtml5-insert-image-url input-xlarge"></div><div class="modal-footer"><a href="#" class="btn" data-dismiss="modal">Cancel</a><a href="#" class="btn btn-primary" data-dismiss="modal">Insert image</a></div></div><a class="btn btn-primary" data-wysihtml5-command="insertImage" title="Insert image" href="javascript:;" unselectable="on"><i class="icon-picture icon-white"></i></a></li></ul>
                                    <textarea id="mustHaveId" class="wysihtml5 span12" rows="5" name="userNotes"><?php echo $userdetails['userNotes'];?></textarea>
									</div>
								</div>
								<!-- // Group END -->
								
								<!--Form actions-->
								<div class="form-actions" style="margin:-20px;">
									<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok" id="submit" name="submit" value="updateUserDetails"><i></i>Save changes</button>
									<button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
									
								</div>
								
								 <!--<div id="feedback" class="span12" style="display:none;margin-top:20px;margin-left:0px;margin-bottom:10px;padding:10px;background:#ff0000;border-radius:5px;color:#fff"></div>-->
								 <div id="feedback" class="alert alert-primary" style="display:none;margin-top:20px;">
							     </div>
								<!-- // Form actions END -->
								
								</div>
								<!--</form>-->

								<div class="tab-pane" id="account-settings">
									<!-- Row -->
									<div class="row-fluid">
									  
										<!-- Column -->
										<div class="span3">
											<strong>Change password</strong>
											<p class="muted">Update user security details, leave password blank to if you dont wish to change it.</p>
										</div>
										<!-- // Column END -->
										
										<!-- Column -->
										<div class="span9">
											<label for="inputUsername">Username</label>
											<input type="text" id="inputUsername" class="span10" name="username" value="<?php echo $userdetails['user_name'];?>" disabled="disabled" />
											<span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="Username can't be changed"><i></i></span>
											<div class="separator"></div>
													
											<label for="oldpassword">Old password</label>
											<input type="password" id="oldpassword" name="oldpass" class="span10" value="" placeholder="Leave empty for no change" />
											<span style="margin: 0;" class="btn-action single glyphicons circle_question_mark" data-toggle="tooltip" data-placement="top" data-original-title="Leave empty if you don't wish to change the password"><i></i></span>
											<div class="separator"></div>
											
											<label for="newpassword">New password</label>
											<input type="password" id="newpassword" name="newPass" class="span12" value="" placeholder="Leave empty for no change" />
											<div class="separator"></div>
											
											<label for="repeatpassword">Repeat new password</label>
											<input type="password" id="repeatpassword" name="repeatPass" class="span12" value="" placeholder="Leave empty for no change" />
											
													<!-- Form actions -->
											<div class="form-actions" style="margin-left:-20px;">
												<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok" id="change_password" name="change_password" value="updateUserSettings"><i></i>Change Password</button>
											</div>
											
											<div class="separator"></div>
										</div>
										<!-- // Column END -->
									</div>
									<!-- // Row END -->
									
									<div class="separator line bottom"></div>
									
									<!-- Row -->
									<div class="row-fluid">
									 <div id="new_feedback" class="span12" style="display:none;margin-top:-20px;margin-left:10px;margin-bottom:10px;padding:10px;background:#ff0000;border-radius:5px;color:#fff"></div>
										<!-- Column -->
										<div class="span3">
											<strong>Contact details</strong>
											<p class="muted">Update your personal contact details including links to social networks.</p>
										</div>
										<!-- // Column END -->
										
										<!-- Column -->
										<div class="span9">
											<div class="row-fluid">
												<div class="span6">
													<label for="phone">Phone</label>
													<div class="input-prepend">
														<span class="add-on glyphicons phone"><i></i></span>
														<input type="text" id="phone" name="phone" class="input-large" value="<?php if(!empty($userdetails['phone'])){$count++;}echo $userdetails['phone'];?>" />
													</div>
													<div class="separator"></div>
														
													<label for="email">E-mail</label>
													<div class="input-prepend">
														<span class="add-on glyphicons envelope"><i></i></span>
														<input type="text" id="email" name="email" class="input-large" value="<?php if(!empty($userdetails['email'])){$count++;}echo $userdetails['email'];?>" />
													</div>
													<div class="separator"></div>
														
													<label for="city">City</label>
													<div class="input-prepend">
														<span class="add-on glyphicons link"><i></i></span>
														<input type="text" id="city" name="city" class="input-large" value="<?php if(!empty($userdetails['WeatherLoc'])){$count++;}echo $userdetails['weatherLoc'];?>" />
													</div>
													<div class="separator"></div>
												</div>
												<div class="span6">
													<label for="facebook">Facebook</label>
													<div class="input-prepend">
														<span class="add-on glyphicons facebook"><i></i></span>
														<input type="text" id="facebook" name="facebook" class="input-large" value="<?php if(!empty($userdetails['facebook_profile'])){$count++;} echo $userdetails['facebook_profile'];?>" />
													</div>
													<div class="separator"></div>
													
													<label for="twitter">Twitter</label>
													<div class="input-prepend">
														<span class="add-on glyphicons twitter"><i></i></span>
														<input type="text" id="twitter" name="twitter" class="input-large" value="<?php if(!empty($userdetails['twitter_profile'])){$count++;} echo $userdetails['twitter_profile']?>" />
													</div>
													<div class="separator"></div>
													
													<label for="linkedin">Linkedin</label>
													<div class="input-prepend">
														<span class="add-on glyphicons skype"><i></i></span>
														<input type="text" id="linkedin" name="linkedin" class="input-large" value="<?php if(!empty($userdetails['linkedin_profile'])){$count++;} echo $userdetails['linkedin_profile'];?>" />
													</div>
													<div class="separator"></div>
													
													<label for="google">Google plus</label>
													<div class="input-prepend">
														<span class="add-on glyphicons yahoo"><i></i></span>
														<input type="text" id="google" name="google" class="input-large" value="<?php if(!empty($userdetails['pinterest_profile'])){$count++;} echo $userdetails['pinterest_profile'];?>" />
													</div>
													<div class="separator"></div>
												</div>
											</div>
										</div>
										<!-- // Column END -->
										
									</div>
									<!-- // Row END -->
									
									<!-- Form actions -->
									<div class="form-actions" style="margin: 0;">
										<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok" id="submit2" name="submit2" value="updateUserSettings"><i></i>Save changes</button>
									</div>
									<!-- // Form actions END -->
								</div>

								<div class="tab-pane" id="privacy-settings">
									<div class="uniformjs">
										<label class="checkbox"><input type="checkbox" checked="checked" /> Share full profile info with everyone.</label>
										<label class="checkbox"><input type="checkbox" /> share basic information.</label>
										<label class="checkbox"><input type="checkbox" /> hide my tasks and projects.</label>
										<div class="alert alert-primary">
											<a class="close" data-dismiss="alert">&times;</a>
											<p>Choose who you want to share your information with.</p>
										</div>
									</div>
								</div>
								</div>
							
							</div>
						</div>
						
					</div>
					<!-- // Tab content END -->
					
					<!-- Tab content -->
					<div class="tab-pane widget-body-regular" id="projects">
					
						<div class="well">
							<a type="button" href="#newCategory" class="btn btn-primary btn-icon glyphicons circle_plus pull-right" data-toggle="modal"><i></i>Add project</a>
							<p class="lead margin-none"><strong>1024</strong> tasks closed this month</p>
							<div class="clearfix"></div>
						</div>
						
						<table class="table table-striped table-vertical-center table-projects table-bordered">
							<thead>
								<tr>
									<th colspan="2">Project</th>
									<th width="100" class="center"></th>
									<th width="100" class="center"></th>
									<th width="140" class="center"></th>
									<th width="120" class="center"></th>
								</tr>
							</thead>
							<tbody>
                            <?php  
							$q="SELECT * FROM categories WHERE userId=".$userId;
							$res=mysqli_query($mysqli,$q);
							
							while($row=mysqli_fetch_assoc($res)){
								$id=$row['catId'];
							?>
								<tr>
									<td width="80" class="center"><a type="button" href="#newCategory" class="btn btn-primary btn-icon glyphicons glass" style="height:15px;"><i></i></a></td>
									<td class="important"><?php echo $row['catName'];?></td>
									<td class="center stats"><span>Deadline</span><span class="count" style="font-size:16px;"><?php echo dbFullDateFormat($row['catDue']);?></span></td>
									<td class="center stats"><span>Days Left</span><span class="count"><?php echo getWorkingDays(date("Y-m-d"),date($row['catDue'])); ?></span></td>
									<td class="center stats"><span>Progress</span><span class="count"><?php echo percentGoalComplete($id);?>%</span></td>
									<td class="center"><a href="index.php?page=viewKPA&catId=<?php echo $id; ?>" type="button" class="btn btn-default">Manage</a></td>
								</tr>
								<?php }?>
							</tbody>
						</table>
						
					</div>
                    
                    <div class="tab-pane widget-body-regular" id="skills">
                     <div class="widget widget-tabs widget-tabs-gray widget-tabs-vertical row-fluid row-merge margin-none widget-body-white">
	
							<!-- Widget heading -->
							<div class="widget-head span3">
								<ul>
									<li class="active"><a class="glyphicons settings" href="#skill-details" data-toggle="tab"><i></i>My crazy skills</a></li>
									<li><a class="glyphicons certificate" href="#account-settings" data-toggle="tab"><i></i>Academic qualifications</a></li>
                                    
									<li><a class="glyphicons eye_open" href="#privacy-settings" data-toggle="tab"><i></i>Innovative Initiatives</a></li>
                                    	<li><a class="glyphicons share" href="#privacy-settings" data-toggle="tab"><i></i>Other</a></li>
								</ul>
							</div>
							<!-- // Widget heading END -->
							
							<div class="widget-body span9">
							
								<div class="tab-content">
								<div class="tab-pane active" id="skill-details">
									<!-- Row -->
								<div class="row-fluid">
                                	<div class="buttons pull-right">
		<a href="#newSkill" class="btn btn-primary btn-icon glyphicons circle_plus" data-toggle="modal"><i></i> Add Skill</a>
        <a href="#newQualification" class="btn btn-success btn-icon glyphicons circle_plus" data-toggle="modal"><i></i> Add Qualification</a>
	</div>
                                    <div class="span9 offset1">
                                    	<div class="row-fluid">
											<h5 class="strong">Skills</h5>
                                            <?php
											//popolating the user skills
											$query="SELECT * from gantt_skills where skillType=0 and userID=".$userId;
											$result1 = $mysqli->query($query);
					
											if(!empty($result1)){
											
											$totalUsers=mysqli_num_rows($result1);
											
											if (mysqli_num_rows($result1)>0){
											while($user = mysqli_fetch_array($result1)){					
												$skill=$user["skillName"];
												$rating=$user["userRating"]."%";
												echo'<div class="span6"><div class="progress progress-mini progress-primary count-outside add-outside"><div class="count">'.$rating.'</div><div class="bar" style="width: '.$rating.';"></div><div class="add">'.$skill.'</div>
									</div>
									</div>
										<div class="span3" style="margin-top:-5px;">		
										<span data-toggle="tooltip" data-placement="top" title="'.$editTooltip.'">
											<a data-toggle="modal" href="#editSkill'.$user['id'].'"><i class="glyphicon icon-pencil"></i></a>
										</span>
										<span  data-toggle="tooltip" data-placement="top" title="'.$deleteTooltip.'">
											<a data-toggle="modal" href="#deleteSkill'.$user['id'].'"><i class="glyphicon icon-remove-sign"></i></a>
										</span>
										</div>
										
									';
													}
												}
											}
											    ?>
                                           
											</div>
                                             <h5 class="strong">Academic Qualifications</h5>
                                             <?php
											//popolating the user skills
											$query="SELECT * from gantt_skills where skillType=1 and userID=".$userId;
											$result1 = $mysqli->query($query);
					
											if(!empty($result1)){
											
											$totalUsers=mysqli_num_rows($result1);
											
											if (mysqli_num_rows($result1)>0){
											while($user = mysqli_fetch_array($result1)){					
												$skill=$user["skillName"];
												$rating=$user["userRating"]."%";
												$year=$user["dateAttained"];
												echo'<div class="span6"><div ><div style="width:200;"></div><div class="add">'.$skill.'</div>
									</div>
									</div>
										<div class="span3" style="margin-top:-5px;">		
										<span data-toggle="tooltip" data-placement="top" title="'.$editTooltip.'">
											<a data-toggle="modal" href="#editSkill'.$user['id'].'"><i class="glyphicon icon-pencil"></i></a>
										</span>
										<span  data-toggle="tooltip" data-placement="top" title="'.$deleteTooltip.'">
											<a data-toggle="modal" href="#deleteSkill'.$user['id'].'"><i class="glyphicon icon-remove-sign"></i></a>
										</span>
										</div>
										
									';
													}
												}
											}
											    ?>
                                           
                                             
                                             
											</div>
								
								</div> <!--//Row-->
                                </div> <!--//Tab pane-->
                                </div> <!--//Tab Content-->
                             </div> <!--//Widget body-->
                             
                        </div> <!--//Widget Tabs-->      
                                	
                    </div>
 
                    <div class="tab-pane widget-body-regular" id="teams">
                     <div class="widget widget-tabs widget-tabs-gray widget-tabs-vertical row-fluid row-merge margin-none widget-body-white">
                             
                        </div> <!--//Widget Tabs-->      
                                	
                    </div> 
					<!-- // Tab content END -->
                   
                    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	                <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	                <script src="js/ajax.js"></script>
                    
				</div>
			
		</div>
	</div>
	<!-- // Widget END -->
	
</div>	
	
		
		</div>
		<!-- // Content END -->
		
	