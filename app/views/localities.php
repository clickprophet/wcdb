<?php
require_once("../config-small.php"); //loads the system configuration settings
$nation="";
if (!empty($_REQUEST['nationID'])){$nation=$_REQUEST['nationID'];}
	
?>
        <div class="">
          <div class="page-title">
            <div class="title_left">
			
              <h3>
                    <a href="." type="button"><i class="fa fa-arrow-circle-left"></i></a>  Manage Localities
                    <small>
                        details of all localities reached
                    </small>
                </h3>
            </div>

            <!--div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div>
            </div-->
          </div>
          <div class="clearfix"></div>

          <div class="row">


                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Localities <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                        </p>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
								<th></th>
								<th>Locality</th>
								<th>Nation</th>
								<th>Date</th>
								<th>Province</th>
								<th>Leader</th>
                            </tr>
                          </thead>
                          <tbody>
						  
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /page content -->
<script type="text/javascript">
	
          $(document).ready(function() {
            
            var table=$('#datatable-responsive').DataTable({
				"ajax": {
				"url": "config/funcs.php?fn=fetchLocalities",
				"dataSrc": ""
				},
				"columns": [
					{ "data": "id" },
					{ "data": "localityName" },
					{ "data": "nation" },
					{ "data": "datePlanted" },
					{ "data": "provinceName" },
					{ "data": "locality_leader" }
					
				],
			    dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                }, {
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }]
			});
			
			 $('#datatable-responsive tbody').on( 'click', 'tr', function () {
				 
				if ( $(this).hasClass('selected') ) {
					$(this).removeClass('selected');
				}
				else {
					table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
			} );
		
			$('#button').click( function () {
				table.row('.selected').remove().draw( false );
			} );
		});
        </script>
