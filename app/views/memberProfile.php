<?php 
//get global settings
require_once("../config-small.php"); //loads the system configuration settings
session_start();
$memberID=$_REQUEST['id'];
$member=fetchMemberDetails($memberID);
$tabID="tab_content".$memberID;
$lang_path="../".$_SESSION["wcdbUser"]->lang;
require_once("$lang_path");


?>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <script src="js/custom-sm.js"></script>
<style>
/* member avatar update form   */

  a.boxclose{
    float:right;
    margin-top:-10px;
    margin-right:-10px;
    cursor:pointer;
    color: #fff;
    border: 1px solid #AEAEAE;
    border-radius: 10px;
    background: red;
    font-size: 11px;
    font-weight: bold;
    display: inline-block;
    line-height: 0px;
    padding: 6px 3px;       
}

.avatarUploadForm{
  display:none;
  border:2px dashed #d2d2d2;
  background-color:#f7f7f7; 
  padding:2px;
}

.boxclose:before {
    content: "×";
}
/*Family tree*/
* {margin: 0; padding: 0;}

.tree ul {
  padding-top: 20px; position: relative;
  
  transition: all 0.5s;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
}

.tree li {
  float: left; text-align: center;
  list-style-type: none;
  position: relative;
  padding: 20px 5px 0 5px;
  
  transition: all 0.5s;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
}

/*We will use ::before and ::after to draw the connectors*/

.tree li::before, .tree li::after{
  content: '';
  position: absolute; top: 0; right: 50%;
  border-top: 1px solid #ccc;
  width: 50%; height: 20px;
}
.tree li::after{
  right: auto; left: 50%;
  border-left: 1px solid #ccc;
}

/*We need to remove left-right connectors from elements without 
any siblings*/
.tree li:only-child::after, .tree li:only-child::before {
  display: none;
}

/*Remove space from the top of single children*/
.tree li:only-child{ padding-top: 0;}

/*Remove left connector from first child and 
right connector from last child*/
.tree li:first-child::before, .tree li:last-child::after{
  border: 0 none;
}
/*Adding back the vertical connector to the last nodes*/
.tree li:last-child::before{
  border-right: 1px solid #ccc;
  border-radius: 0 5px 0 0;
  -webkit-border-radius: 0 5px 0 0;
  -moz-border-radius: 0 5px 0 0;
}
.tree li:first-child::after{
  border-radius: 5px 0 0 0;
  -webkit-border-radius: 5px 0 0 0;
  -moz-border-radius: 5px 0 0 0;
}

/*Time to add downward connectors from parents*/
.tree ul ul::before{
  content: '';
  position: absolute; top: 0; left: 50%;
  border-left: 1px solid #ccc;
  width: 0; height: 20px;
}

.tree li a{
  border: 1px solid #ccc;
  padding: 5px 10px;
  text-decoration: none;
  color: #666;
  font-family: arial, verdana, tahoma;
  font-size: 11px;
  display: inline-block;
  
  border-radius: 5px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  
  transition: all 0.5s;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
}

/*Time for some hover effects*/
/*We will apply the hover effect the the lineage of the element also*/
.tree li a:hover, .tree li a:hover+ul li a {
  background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
}
/*Connector styles on hover*/
.tree li a:hover+ul li::after, 
.tree li a:hover+ul li::before, 
.tree li a:hover+ul::before, 
.tree li a:hover+ul ul::before{
  border-color:  #94a0b4;
}


</style>

<!-- Family template -->
  

  <div class="col-md-12">
	<div class="x_panel">
	 
	  <div class="x_title">
		<h2>Disciple Details</h2>
		<ul class="nav navbar-right panel_toolbox">
		  <li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
			<ul class="dropdown-menu" role="menu">
			  <li><a href="#"><i class="icon-printer"></i> Print</a>
			  </li>
			  <li><a href="#"><i class="icon-shield"></i> Deactivate</a>
			  </li>
				 <li><a onclick="
				 	(new PNotify({
                            title: 'Confirmation Needed',
                            text: 'Are you sure you want make this disciple a system user?',
                            icon: 'glyphicon glyphicon-question-sign',
                            hide: false,
                            confirm: {
                              confirm: true
                            },
                            buttons: {
                              closer: false,
                              sticker: false
                            },
                            history: {
                              history: false
                            },
                            addclass: 'stack-modal',
                            stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                          })).get().on('pnotify.confirm', function(){
                            makeUser();
                          });
				 " ><i class="icon-user-follow" ></i> Make User</a>
			  </li>
			  <li><a onclick="deleteMember(<?php echo $memberID; ?>)"><i class="icon-trash"></i> Delete</a>
			  </li>
			</ul>
		  </li>
		  <li><a onclick="closeTab('<?php echo $tabID;?>')" class="close-link"><i class="fa fa-close"></i></a>
		  </li>
		</ul>
		<div class="clearfix"></div>
	  </div>
	  
	   <div class="x_content">
	   <div class="x_body">
		<div class="col-md-3 col-sm-12 col-xs-12">
		<div id="uploadForm" class="avatarUploadForm">
			<a onclick="showHide('uploadForm')" class="boxclose" type="button"></a>
			<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
				<input id="id" name="memberId" value="<?php echo $memberID; ?>" type="hidden">
				<input class="form-control" id="image_file" name="image_file" type="file">
			</form>
			<!--button class="btn btn-default" data-dismiss="modal" type="button">Close</button--><div style="margin:10px"></div>
			<button class="btn btn-primary btn-sm" onclick="saveImageFromFile('uploadimage')" type="submit" name="submit" value="Upload">Upload</button>
		</div>
		   <div class="profile_img">
			
			  <!-- end of image cropping -->
			  <div id="avatar">
				<!-- Current avatar -->
				<div id="profileAvatar"  data-toggle="tooltip" data-placement="bottom" title="Change the avatar">
				<img onclick="showHide('uploadForm')" class="img-responsive avatar-view" src="images/members/<?php echo $member["image"];?>" alt="Avatar" 
				onerror="if (this.src != 'images/members/user.svg') this.src = 'images/members/user.svg';"></div>
                      
				<!-- Loading state -->
				<div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
			  </div>
			  <!-- end of image cropping -->

			</div>
		<div class="col-md-12 col-sm-12 col-xs-12 text-center" >
			<h3><span class="fullNamex<?php echo $memberID;?>"></span></h3>
			<div id="memberStatus<?php echo $memberID;?>"></div>
			<br><br>
		</div>
		  
		</div>
		<div class="col-md-9 col-sm-12 col-xs-12">
		
			
			<div class="col-xs-9">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home-tab-r">
					  <form id="saveMemberForm_<?php echo $memberID;?>" class="form-horizontal form-label-left input_mask" action="" method="post">
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="First Name(s)">
						  <input type="text" class="form-control has-feedback-left" id="firstNamex" name="firstNamex"  placeholder="First Name" value="">
						  <span class="icon-user form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Surname">
						  <input type="text" class="form-control has-feedback-left" id="lastNamex" value="" placeholder="Last Name">
						  <span class="icon-users form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Email">
						  <input type="text" class="form-control has-feedback-left" id="emailx" value="" name="email" placeholder="Email">
						  <span class="icon-envelope form-control-feedback left" aria-hidden="true"></span>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Address">
						  <input type="text" class="form-control has-feedback-left" id="addressx" value="" placeholder="Address">
						  <span class="icon-pointer form-control-feedback left" aria-hidden="true"></span>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Surburb">
						  <input type="text" class="form-control has-feedback-left" id="surburbx" value="" placeholder="Surburb">
						  <span class="icon-home form-control-feedback left" aria-hidden="true"></span>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="City">
						  <input type="text" class="form-control has-feedback-left" id="cityx" value="" placeholder="City">
						  <span class="icon-map form-control-feedback left" aria-hidden="true"></span>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
							<select id="countryNamey" name="countryNamey" class="form-control has-feedback-left countries">
							  <option value="">Choose Country</option>
							</select>
						 <span class="icon-globe form-control-feedback left" aria-hidden="true"></span>
								
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Locality">
								
							<select id="localitiesy" name="localitiesy" class="form-control localities" >
							  <option>Choose Locality</option>
							 </select>
						
						  <span class="icon-pin form-control-feedback left" aria-hidden="true"></span>
						</div>

						

						<script>
							//set locality on change of country
							$(document).on('change', 'select#countryNamey', function(){
									var value = $('select#countryNamey option:selected').val();
									$("select#localitiesy").load("includes/fetchLocalities.php?nationID="+value);
									//$('#localitiesy').val(<?php echo $member['locality'];?>);
									//$('#localitiesy').trigger('change');
	
							})
						</script>

						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Phone Number">
						  <input type="text" class="form-control has-feedback-left" id="phonex" value=""data-validate-length-range="8,20" placeholder="Phone">
						  <span class="icon-screen-smartphone form-control-feedback left" aria-hidden="true"></span>
						</div>
						
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Date of Birth">
							<input id="birthdayx" id="DOB" class="date-picker form-control has-feedback-left birthdayx" value="" type="text" >
						   <span class="icon-calendar form-control-feedback left" aria-hidden="true" ></span>
						</div>
				
						<!-- House church Selector -->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("HOUSE-CHURCH");?>">
								
							<select id="houseChurchx" name="houseChurchx" class="form-control has-feedback-left">
								<option><?php echo lang("CHOOSE")." ".lang("HOUSE-CHURCH");?></option>
							 </select>
						
						  <span class="icon-pin form-control-feedback left" aria-hidden="true"></span>
						</div>
						<!-- discipleMaker-->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("DISCIPLE-MAKER");?>">
						 	<select id="discipleMakerx" name="discipleMakerx" class="form-control has-feedback-left members">
								<option>Select disciple Maker..</option>
							</select>
						  <span class="icon-user form-control-feedback left" aria-hidden="true"></span>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
						 <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required">*</span>Gender:</label>
							
							 <div class="col-md-9 col-sm-9 col-xs-12">
							 <p>
							  M:
							  <input type="radio" class="flat" name="gender" id="genderM" value="M" <?php if( $member["sex"]=="M"){echo 'checked=""';}?> required /> F:
							  <input type="radio" class="flat" name="gender" id="genderF" value="F"  <?php if( $member["sex"]=="F"){echo 'checked=""';}?>/>
							</p>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 control-group" data-toggle="tooltip" data-placement="top" title="Tags">
						  
							<input id="tags_<?php echo $memberID;?>" type="text" class="tags form-control" value="" />
							<div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
						
						</div>
							
					 </div>
                  <div class="tab-pane" id="profile-tab-r">
                     <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Family Name">
                      <select id="familyNamex" name="familyNamex" class="form-control has-feedback-left families">
                        <option value="0">Add new family</option>
                      </select>
                   
                      <span class="icon-users form-control-feedback left" aria-hidden="true"></span>
                       
                    </div>
                    <!-- family role Selector -->
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Family Role">  
                      <select id="familyRolex" name="familyRolex" class="form-control has-feedback-left" >
                        <option value=0>Choose role</option>
                        <option value=1>Family head</option>
                        <option value=2>Spouse</option>
                        <option value=3>Child</option>
                       </select>
                      <span class="icon-home form-control-feedback left" aria-hidden="true"></span>
                   
                    </div>
                     <div  class="col-md-12 col-sm-12 col-xs-12" style="background-color: #F2F2F2;height: 250px">
                      <div id="familyDiv" class="tree">
                      </div>


                      </div>
                       
                      </div>
                      <div class="tab-pane" id="messages-r">Coming Soon....</div>
                      <div class="tab-pane" id="settings-r">Coming Soon....</div>
                    </div>
                  </div>
				  <div class="col-xs-3">
                    <!-- required for floating -->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-right">
                      <li class="active"><a href="#home-tab-r" data-toggle="tab">General Info</a>
                      </li>
                      <li><a href="#profile-tab-r" data-toggle="tab">Family <span class="badge bg-red">new!</span></a>
                      </li>
                      <!--li><a href="#messages-r" data-toggle="tab">Teams</a>
                      </li>
                      <li><a href="#settings-r" data-toggle="tab">Testimony</a>
                      </li-->
                    </ul>
                  </div>
                  <input id="nationx" name="nationx" type="hidden" value="<?php echo $member['country']; ?>" />
				</form>
				
				 <div class="x_footer pull-right" style="margin-top:10px;margin-right:5px;">
						<input id="memberID" type="hidden" value="<?php echo $memberID; ?>" />

						

						<button class="btn btn-warning" type="button" onclick="
						(new PNotify({
                            title: 'Confirmation Needed',
                            text: 'Are you sure you want to delete disciple?',
                            icon: 'glyphicon glyphicon-question-sign',
                            hide: false,
                            confirm: {
                              confirm: true
                            },
                            buttons: {
                              closer: false,
                              sticker: false
                            },
                            history: {
                              history: false
                            },
                            addclass: 'stack-modal',
                            stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                          })).get().on('pnotify.confirm', function(){
                            deleteMember(<?php echo $memberID; ?>);
                          });
						"  name="delete" id="delete">
						<i class="icon-trash"></i> Delete</button>
                        <button type="button" onclick="closeTab('<?php echo $tabID;?>')" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" onclick="
                        (new PNotify({
                            title: 'Confirmation Needed',
                            text: 'Are you sure you want to save member details?',
                            icon: 'glyphicon glyphicon-question-sign',
                            hide: false,
                            confirm: {
                              confirm: true
                            },
                            buttons: {
                              closer: false,
                              sticker: false
                            },
                            history: {
                              history: false
                            },
                            addclass: 'stack-modal',
                            stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                          })).get().on('pnotify.confirm', function(){
                            saveMember();
                          });
                        " class="btn btn-primary" name="submit" >Save</button>
                  </div>
			
				</div>	   
			</div>
			
		 </div>
	 </div>
 </div>
 
  

  
   <!-- tags -->
  <script src="js/tags/jquery.tagsinput.min.js"></script>
   <!-- input tags -->
   
   	<script>
	$( function() {
		$( ".birthdayx" ).datepicker({
				dateFormat: "yyyy-mm-dd",
				ShowAnim:"clip",                 
				changeMonth: true,
				changeYear: true
		});
	} );
	</script>
	<script type="text/javascript">
   	$(document).ready(function(){
   		//populate leaders drop down
      	var opt1 = $(".members");
          $.getJSON("api/funcs.php?fn=fetchmembers&n=<?php echo $member['country'];?>", function(response) {
               $.each(response, function() {
               opt1.append($("<option />").val(this.id).text(this.firstName+' '+this.lastName));
           });
        });

   		var opt2 = $("#houseChurchx");
          $.getJSON("api/funcs.php?fn=fetchHouseChurches&n=<?php echo $member['country'];?>", function(response) {
               $.each(response, function() {
               opt2.append($("<option />").val(this.id).text(this.familyName));
           });
        });

        var opt3 = $(".countries");
          $.getJSON("api/funcs.php?fn=fetchCountries", function(response) {
               $.each(response, function() {
               opt3.append($("<option />").val(this.code).text(this.name));
           });
        });

          //populate localities drop down
      	var opt4 = $(".localities");
          $.getJSON("api/funcs.php?fn=fetchLocalities&n=<?php echo $member['country'];?>", function(response) {
               $.each(response, function() {
               opt4.append($("<option />").val(this.id).text(this.localityName));
           });
        });

             //populate localities drop down
      	var opt5 = $(".families");
          $.getJSON("api/funcs.php?fn=fetchFamilies&n=<?php echo $member['country'];?>", function(response) {
               $.each(response, function() {
               opt5.append($("<option />").val(this.id).text(this.familyName));
           });
        });

         $.getJSON("api/funcs.php?fn=fetchMemberDetails&id=<?php echo $memberID;?>", function(res) {
         	var form=saveMemberForm_<?php echo $memberID;?>;
            $( 'select#houseChurchx' ).val(res.assembly).change();
        	$( 'select#countryNamey' ).val( res.country).change();
			$( 'select#discipleMakerx' ).val( res.discipleMaker).change();
			$('#localitiesy').val(res.locality).change();
        	$( 'select#familyNamex' ).val( res.familyId);
			$('select#familyNamex').trigger('change');
			$( 'select#familyRolex' ).val( res.familyRole);
			$('select#familyRolex').trigger('change');
			form.cityx.value=res.city;
			form.firstNamex.value=res.firstName;
			form.lastNamex.value=res.lastName;
			form.emailx.value=res.email;
			form.addressx.value=res.address;
			form.birthdayx.value=res.DOB;
			form.phonex.value=res.phone;
			form.surburbx.value=res.surburb;
			$('#tags_'+res.id).val(res.tags);
			$('.fullNamex'+res.id).html(res.firstName+' '+res.lastName);
			$('#tags_'+res.id).tagsInput({
		        width: 'auto'
		      });
			
			if(res.userId>=1){$("#memberStatus<?php echo $memberID;?>").append("<span class='label label-success'> System User</span>&nbsp");}else{ $("#memberStatus<?php echo $memberID;?>").append("<span class='label label-default'>not a system user</span>&nbsp");
			$("#memberStatus<?php echo $memberID;?>").append(`<br/><br/><button class="btn btn-sm btn-info" type="button" onclick="
						(new PNotify({
                            title: 'Confirmation Needed',
                            text: 'Are you sure you want make this disciple a system user?',
                            icon: 'glyphicon glyphicon-question-sign',
                            hide: false,
                            confirm: {
                              confirm: true
                            },
                            buttons: {
                              closer: false,
                              sticker: false
                            },
                            history: {
                              history: false
                            },
                            addclass: 'stack-modal',
                            stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                          })).get().on('pnotify.confirm', function(){
                            makeUser(<?php echo $memberID; ?>);
                          });
						"  name="makeuser" id="makeuser"><i class="icon-user"></i> Make User</button>`);
			 if(res.isAdmin==1){$("#memberStatus<?php echo $memberID;?>").append( "<span class='label label-primary'>Admin</span>");} 
			}
			
        });

   		

     });

   </script>

  
  <script>
    function onAddTag(tag) {
      alert("Added a tag: " + tag);
    }

    function onRemoveTag(tag) {
      alert("Removed a tag: " + tag);
    }

    function onChangeTag(input, tag) {
      alert("Changed a tag: " + tag);
    }
	$("#loaderImg").hide(); //hide loader image
	   
  </script>
  
  <script type="text/javascript">
  	$(function() {
  	  $.getJSON('api/funcs.php?fn=fetchFamily&id=<?php echo $member["familyId"];?>', function(data) {
          var template = $('#familystpl').html();
          var html = Mustache.to_html(template, data);
          $('#familyDiv').html(html);
        }); //getTopMembers
  	   });
  </script>

  
  
