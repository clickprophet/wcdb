
      <!-- page content -->
        <div class="">

         
          <div class="clearfix"></div>

          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">  <a href="." type="button"><i class="fa fa-arrow-circle-left"></i></a>
                  <h2> Calendar Events <small>Sessions</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div id='wrap'>
                  <div class="row">
                    <div class="col-sm-2">
                    <div id='external-events'>
                      <div id='external-events-listing'>
                        <h4>Draggable Events</h4>
                        <div class='fc-event'>World University of Prayer</div>
                        <div class='fc-event'>SKSG</div>
                        <div class='fc-event'>International Student Camp</div>
                        <div class='fc-event'>International Bible Camp</div>
                        <div class='fc-event'>Ministry Fast</div>
                        <div class='fc-event'>Prayer Crusade</div>
                        <div class='fc-event'>Prayer Siege</div>
                        <div class='fc-event'>Prayer Day</div>
                        <div class='fc-event'>New Event..  </div>
                      </div>
                      <p>
                        <input type='checkbox' id='drop-remove' checked='checked' />
                        <label for='drop-remove'>remove after drop</label>
                      </p>
                      <br/>
                      <h5>(Drop in trash can) To Delete Events</h5>
                      <p>
                        <!--img src="trash-can.png" id="trash" alt=""-->
                        <center><div id="trash"><i class="fa fa-trash"  style="font-size:38px;"></i></div></center>
                      </p>
                    </div>
                    </div>
                    <div class="col-sm-10">
                    <div id='calendar'></div>
                    </div>
                   
                </div>
                 <div style='clear:both'></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <!-- /page content -->


  <!-- Start Calendar modal -->
  <div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="myModalLabel">New Calendar Entry</h4>
        </div>
        <div class="modal-body">
          <div id="testmodal" style="padding: 5px 20px;">
            <form id="antoform" class="form-horizontal calender" role="form">
              <div class="form-group">
                <label class="col-sm-3 control-label">Title</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="title" name="title">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                  <textarea class="form-control" style="height:55px;" id="descr" name="descr"></textarea>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary antosubmit">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="myModalLabel2">Edit Calendar Entry</h4>
        </div>
        <div class="modal-body">

          <div id="testmodal2" style="padding: 5px 20px;">
            <form id="antoform2" class="form-horizontal calender" role="form">
              <div class="form-group">
                <label class="col-sm-3 control-label">Title</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="title2" name="title2">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                  <textarea class="form-control" style="height:55px;" id="descr2" name="descr"></textarea>
                </div>
              </div>

            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary antosubmit2">Save changes</button>
        </div>
      </div>
    </div>
  </div>

  <div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
  <div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>

  <!-- End Calendar modal -->
<!-- Add New Event-->   
<div id="eventModal" class="modal fade editEvent" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-md">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Event</h4>
              </div>
              <div class="modal-body">
                <div class="x_panel">
            
            <div class="x_content">
              <br />
               <form id="addEventForm" class="form-horizontal form-label-left input_mask" action="" method="post">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <input type="text" class="form-control has-feedback-left" name="eventTitlex" id="eventTitlex" placeholder="Event Title"  value="">
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <input type="text" class="form-control has-feedback-left" id="eventDescx" name="eventDescx" placeholder="Description" value="">
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <input type="text" class="date date-picker form-control has-feedback-left" id="startDatex" name="startDatex" placeholder="Start Date" value="">
                <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <input type="text" class="date date-picker form-control" id="endDatex" name="endDatex" placeholder="End Date">
                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
              </div>
              <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Event Color</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="input-group demo2">
                          <input type="text" id="eventColorx" name="eventColorx" value="" class="form-control" />
                          <span class="input-group-addon"><i></i></span>
                        </div>
                      </div>
                    </div>
                <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Event Region</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        
                          <select type="select" id="eventRegion" name="eventRegion" class="form-control">
                            <option value="<?php echo $n;?>"><i class="icon-pin"></i> Local</option>
                            <option value="GLB"><i class="icon-globe-alt"></i> Global</option>

                        </select>
                        
                      </div>
                    </div>
              <input type="hidden" id="eventIdx" name="eventIdx" value=""/>
              
              </form>             
            </div>
            </div>
            </div>
    
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button  onclick="saveEvent()" type="submit" class="btn btn-primary" name="submit" value="AddMember" data-dismiss="modal">Submit</button>
            </div>
  
          </div>
        </div>
      </div>
                
  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>
  
   <script src="js/moment/moment.min.js"></script>  
  <script src="js/calendar/fullcalendar.min.js"></script>


<script>

  </script>


 <script>
  var nation = sessionStorage.getItem("nation");
    $(function() {
      $("#loaderImg").hide(); //hide loader image
      var date = new Date();
      var d = date.getDate();
      var m = date.getMonth();
      var y = date.getFullYear();
      var started;
      var categoryClass;
      

      /* initialize the external events
        -----------------------------------------------------------------*/

        $('#external-events .fc-event').each(function() {

            // store data so the calendar knows to render an event upon drop
            $(this).data('event', {
                title: $.trim($(this).text()), // use the element's text as the event title
                stick: true // maintain when user navigates (see docs on the renderEvent method)
            });

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

        });

  


/* initialize the calendar
        -----------------------------------------------------------------*/

      var zone = "02:00";
      var calendar = $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },


        //selectable: true,
        //selectHelper: true,
        editable: true,
        droppable:true,
        navLinks: true, // can click day/week names to navigate views
			  eventLimit: true, // allow "more" link when too many events
        dragRevertDuration: 0,
        drop: function() {
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
        

        events:  {
            url: 'api/funcs.php?fn=fetchAllEvents&n='+nation+'&id=GLB',  //data/events.php
            type: 'POST', // Send post data
            data: 'type=fetch',
            cache: true,
            editable:true,
            async: false,
            success: function(response){
              json_events = response;
            },
            error: function(e) {
                errorNote('There was an error while fetching events. '+e.responseText);
            }
        },

        //adding a new event by dropping
        eventReceive: function(event){ 
              var title = event.title;
              var start = event.start.format("YYYY-MM-DD[T]HH:MM:SS");
              var nation=sessionStorage.getItem("nation");
              $.ajax({
                url: 'api/post.php',
                data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone+'&nation='+nation,
                type: 'POST',
                dataType: 'json',
                success: function(res){
                  event.id = res.eventid;
                  //successNote("Success","new event "+res.title+" added successfully!");
                  $('#calendar').fullCalendar('refetchEvents'); //refresh calendar
                },
                error: function(e){
                  errorNote("Error",e.responseText);
                }
            });
              //$('#calendar').fullCalendar('updateEvent',event);
          },

          //Renaming an event
          /*eventClick: function(event, jsEvent, view) {
              var title = prompt('Event Title:', event.title, { buttons: { Ok: true, Cancel: false} });
              if (title){
              event.title = title;
              $.ajax({
                url: 'api/post.php',
                data: 'type=changetitle&title='+title+'&eventid='+event.eventId,
                type: 'POST',
                dataType: 'json',
                success: function(response){
                  if(response.status == 'success')
                  $('#calendar').fullCalendar('updateEvent',event);
                },
                error: function(e){
                  errorNote('Error processing your request: '+e.responseText);
                }
              });
              }
            },*/
        eventClick: function(event, jsEvent, view){
          $('#eventIdx').val(event.eventId);
          $('#eventTitlex').val(event.title);//alert(event.title);
          $('#startDatex').val(event.start.format());
          if(event.end!=null){$('#endDatex').val(event.end.format());}
          $('#eventColorx').val(event.color);
          $('#eventColorx').trigger('change');
          $('#eventDescx').val(event.eventDesc);
          $('#eventModal').modal();
          $('select#eventRegion').val(event.nation);
          $('select#eventRegion').trigger('change');
        },

        //dropping an existing event on another date
        eventDrop: function(event, delta, revertFunc) {
              var title = event.title;
              var eventId=event.eventId;
              var start = event.start.format();
              var end = (event.end == null) ? start : event.end.format();
              
              $.ajax({
                  url: 'api/post.php',
                  data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+eventId,
                  type: 'POST',
                  dataType: 'json',
                  success: function(response){
                    if(response.status != 'success')
                    revertFunc();
                  },
                  error: function(e){
                    revertFunc();
                    errorNote('Error!','Error processing your request: '+e.responseText);
                  }
                  });
            },

        //resize event
        eventResize: function(event, delta, revertFunc) {
                console.log(event);
                var title = event.title;
                var end = event.end.format();
                var start = event.start.format();
                $.ajax({
                  url: 'api/post.php',
                  data: 'type=update&title='+title+'&start='+start+'&end='+end+'&eventid='+event.eventId,
                  type: 'POST',
                  dataType: 'json',
                  success: function(response){
                  if(response.status == 'success')
                    $('#calendar').fullCalendar('refetchEvents'); //refresh calendar
                    //successNote('Success','Event updated successfully');
                  
                  },
                  error: function(e){
                  errorNote('Error!','Error processing your request: '+e.responseText);
                  }
                });
        },

          //delete events by drag and drop
         eventDragStop: function (event, jsEvent, ui, view) {
              if (isElemOverDiv()) {
                
              (new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this event - <strong>'+event.title+'</strong>?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
                      $.ajax({
                        url: 'api/post.php',
                        data: 'type=remove&eventid='+event.eventId,
                        type: 'POST',
                        dataType: 'json',
                        success: function(response){
                          if(response.status == 'success')
                            $('#calendar').fullCalendar('refetchEvents');
                        },
                        error: function(e){
                        errorNote('Error processing your request: '+e.responseText);
                        }
                    });

                   });
                }
            },
        /*eventAfterRender: function(event, element, view) 
            {
                $(element).css('height','15');
              },*/

        select: function(start, end, allDay) {
          $('#fc_create').click();

          started = start;
          ended = end

          $(".antosubmit").on("click", function() {
            var title = $("#title").val();
            if (end) {
              ended = end
            }
            categoryClass = $("#event_type").val();

            if (title) {
              calendar.fullCalendar('renderEvent', {
                  title: title,
                  start: start,
                  end: end,
                  allDay: allDay,
                  eventDesc:eventDesc,
                  color:color
                }
                //true // make the event "stick"
              );
            }
            $('#title').val('');
            calendar.fullCalendar('unselect');

            $('.antoclose').click();

            return false;
          });
        }
      
        
      });
    });

//First set the co-ordinates of the mouse to (-1,-1) during page load. When the mouse moves capture the x and y co-ordinates and save them to global variables.
      var currentMousePos = {
                x: -1,
                y: -1
            };

            jQuery(document).on("mousemove", function (event) {
              currentMousePos.x = event.pageX;
              currentMousePos.y = event.pageY;
            });
          
   function isElemOverDiv() {
   var trashEl = jQuery('#trash');
   var ofs = trashEl.offset();
   var x1 = ofs.left;
   var x2 = ofs.left + trashEl.outerWidth(true);
   var y1 = ofs.top;
   var y2 = ofs.top + trashEl.outerHeight(true);
   if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&      currentMousePos.y >= y1 && currentMousePos.y <= y2) {      return true;    }    return false; }
 
  
 
  </script>
  
    <!-- color picker -->
  <script src="js/colorpicker/bootstrap-colorpicker.min.js"></script>
  <script src="js/colorpicker/docs.js"></script>

