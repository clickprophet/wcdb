<?php
require_once("../config-small.php"); //loads the system configuration settings
session_start();
$members=fetchMembers();
$nation=$_SESSION["wcdbUser"]->nation;
$nations=getNations();
$countries=getCountries();

$lang_path="../".$_SESSION["wcdbUser"]->lang;


require_once("$lang_path");
?>
  <!-- load icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <script src="js/custom-sm.js"></script>
  <!-- ------------------------- -->
  <style>
/* member avatar update form   */

  a.boxclose{
    float:right;
    margin-top:-10px;
    margin-right:-10px;
    cursor:pointer;
    color: #fff;
    border: 1px solid #AEAEAE;
    border-radius: 10px;
    background: red;
    font-size: 11px;
    font-weight: bold;
    display: inline-block;
    line-height: 0px;
    padding: 6px 3px;       
}

.avatarUploadForm{
  display:none;
  border:2px dashed #d2d2d2;
  background-color:#f7f7f7; 
  padding:2px;
}

.boxclose:before {
    content: "×";
}

</style>

        <div class="">
          <!--div class="page-title">
            <div class="title_left">
              <h3>
                   <?php echo lang("ADD-NEW-DISCIPLE");?>
                    <small>         
                    </small>
                </h3>
				
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
				
				
              </div-->
			  <!--div class="col-md-8 col-sm-8 col-xs-12 form-group pull-right">
			  <div class="btn-group">
					<a onclick="openPage(19)" class="btn btn-default btn-sm" ><i class="glyphicon glyphicon-refresh"></i></a>
                    <button class="btn btn-default btn-sm" data-toggle="modal" data-target=".newMember"><i class="glyphicon glyphicon-plus"></i> House Church</button>
					<button class="btn btn-default btn-sm" data-toggle="modal" data-target=".newLocality"><i class="glyphicon glyphicon-plus"></i> Locality</button>
				</div>
			  </div>
            </div>
          </div-->
          <div class="clearfix"></div>
          <div class="row">
		  <div class="col-md-12">
			<div class="x_panel">
						
				<div class="x_content">
					
						<div class="x_body">
		<div class="col-md-3 col-sm-12 col-xs-12">
		<div id="uploadForm1" class="avatarUploadForm">
			<a onclick="showHide('uploadForm1')" class="boxclose" type="button"></a>
			<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
				<input class="form-control" id="image_file" name="image_file" type="file">
			</form>
			<!--button class="btn btn-default" data-dismiss="modal" type="button">Close</button--><div style="margin:10px"></div>
			<button class="btn btn-primary btn-sm" onclick="saveImage('uploadimage')" type="submit" name="submit" value="Upload"><?php echo lang("UPLOAD");?></button>
			<!--button class="btn btn-default btn-sm" data-toggle="modal" data-target=".imgUpload"><i class="glyphicon glyphicon-camera"></i> <?php echo lang("WEBCAM");?></button-->
			<!--button class="btn btn-default btn-sm" onclick="imgDisplay()"> img file</button-->
		</div>
		   <div class="profile_img">
			
			  <!-- end of image cropping -->
			  <div id="avatar">
				<!-- Current avatar -->
				<div id="profileAvatar"  data-toggle="tooltip" data-placement="bottom" title="Change the avatar">
				<img  onclick="showHide('uploadForm1')" class="img-responsive avatar-view" src="images/members/user.png>" alt="Avatar" 
				onerror="if (this.src != 'images/members/user.png') this.src = 'images/members/user.png';"></div>
                      
				<!-- Loading state -->
				<div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
			  </div>
			  <!-- end of image cropping -->
			 	 <div class="col-md-3 col-sm-12 col-xs-12 form-group has-feedback" 
				  <input id="imgFileName" value="" type="text" class="form-control has-feedback-left" >
				  <span class="icon-camera form-control-feedback left" aria-hidden="true"></span>
				</div>
			</div>
		<div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;" >
			<h3></h3>
		</div>
		  
		</div>
		<div class="col-md-9 col-sm-12 col-xs-12">
		
			
			<div class="col-xs-9">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home-r">
					  <form id="addMemberForm" class="form-horizontal form-label-left input_mask" action="" method="post">
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("FIRSTNAME");?>">
						  <input type="text" class="form-control has-feedback-left" id="firstName" name="firstName" placeholder="<?php echo lang("FIRSTNAME");?>" value="">
						  <span class="icon-user form-control-feedback left" aria-hidden="true"></span>
						</div>
						<!-- Surname-->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("SURNAME");?>">
						  <input type="text" class="form-control has-feedback-left" id="lastName" name="lastName" value="" placeholder="<?php echo lang("SURNAME");?>">
						  <span class="icon-users form-control-feedback left" aria-hidden="true"></span>
						</div>
						<!-- Email -->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("EMAIL");?>">
						  <input type="text" class="form-control has-feedback-left" id="email" value="" name="email" placeholder="<?php echo lang("EMAIL");?>">
						  <span class="icon-envelope form-control-feedback left" aria-hidden="true"></span>
						</div>
						<!-- Address -->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("ADDRESS");?>">
						  <input type="text" class="form-control has-feedback-left" id="address" name="address" value="" placeholder="<?php echo lang("ADDRESS");?>">
						  <span class="icon-pointer form-control-feedback left" aria-hidden="true"></span>
						</div>
						<!--Surburb -->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("SURBURB");?>">
						  <input type="text" class="form-control has-feedback-left" id="surburb"  name="surburb" value="" placeholder="<?php echo lang("SURBURB");?>">
						  <span class="icon-home form-control-feedback left" aria-hidden="true"></span>
						</div>
						<!-- City -->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("CITY");?>">
						  <input type="text" class="form-control has-feedback-left" id="city" name="city" value="" placeholder="<?php echo lang("CITY");?>">
						  <span class="icon-map form-control-feedback left" aria-hidden="true"></span>
						</div>
						<!-- country selector -->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
							<select id="countryNamex" name="countryNamex" class="form-control has-feedback-left">
							  <option value=""><?php echo lang("CHOOSE")." ".lang("COUNTRY");?></option>
							 <?php 
								foreach ($nations as $n) 
									{
										echo '<option  value="'.$n["code"].'">'.$n["name"].'</option>';
									}
							  ?>
							</select>
						 <span class="icon-globe form-control-feedback left" aria-hidden="true"></span>
						</div>
						

						<!-- Locality Selector -->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("LOCALITY");?>">
								
							<select id="localityx" name="localityx" class="form-control has-feedback-left">
							  <option><?php echo lang("CHOOSE")." ".lang("LOCALITY");?></option>
							 </select>
						
						  <span class="icon-pin form-control-feedback left" aria-hidden="true"></span>
						</div>

						<script>
							$(document).on('change', 'select#countryNamex', function(){
									var value = $('select#countryNamex option:selected').val();
									//alert (value);
									$("select#localityx").load("includes/fetchLocalities.php?nationID="+value);
							})
						
						</script>

						<!-- House church Selector -->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("HOUSE-CHURCH");?>">
								
							<select id="houseChurch" name="houseChurch" class="form-control has-feedback-left">
							  <option><?php echo lang("CHOOSE")." ".lang("HOUSE-CHURCH");?></option>
							 </select>
						
						  <span class="icon-pin form-control-feedback left" aria-hidden="true"></span>
						</div>
						<!-- discipleMaker-->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("DISCIPLE-MAKER");?>">
						  <input type="text" class="form-control has-feedback-left" id="discipleMaker"  name="discipleMaker" value="" placeholder="<?php echo lang("DISCIPLE-MAKER");?>" disabled>
						  <span class="icon-user form-control-feedback left" aria-hidden="true"></span>
						</div>

						<!-- phone number-->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("PHONE");?>">
						  <input type="text" class="form-control has-feedback-left" id="phone" name="phone" value="" data-validate-length-range="8,20" placeholder="<?php echo lang("PHONE");?>">
						  <span class="icon-screen-smartphone form-control-feedback left" aria-hidden="true"></span>
						</div>
						<!-- DOB -->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="<?php echo lang("DOB");?>">
							<input id="birthday" name="birthday" class="date-picker form-control has-feedback-left birthday" value="" type="text" placeholder="1990-01-01" >
						   <span class="icon-calendar form-control-feedback left" aria-hidden="true" ></span>
						</div>
						<script>
							$( function() {
								$( ".birthday" ).datepicker({
										dateFormat: "yy-mm-dd",
										ShowAnim:"clip", 
										yearRange: "-80:+0", //last 80 years               
										changeMonth: true,
										changeYear: true
								});
							} );
						</script>
						

						<!-- Gender -->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
						 <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required">*</span><?php echo lang("GENDER");?>:</label>
							
							 <div class="col-md-9 col-sm-9 col-xs-12">
							 <p>
							  M:
							  <input type="radio" class="flat" name="gender" id="genderM" value="M"  required /> F:
							  <input type="radio" class="flat" name="gender" id="genderF" value="F"/>
							</p>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 control-group" data-toggle="tooltip" data-placement="top" title="<?php echo lang("TAGS");?>">
							<input id="tags" type="text" class="tags form-control" value="" />
							<div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
						</div>
						</form>	
					 </div>

					 <!---------------------- Family Tab ---------------------->
                     <div class="tab-pane" id="profile-r">
                     	<!-- Family Name-->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Family Name">
						  <input type="text" class="form-control has-feedback-left" id="familyName" value="" placeholder="Family Name" disabled>
						  <span class="icon-users form-control-feedback left" aria-hidden="true"></span>
						</div>
						<!-- family role Selector -->
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Locality">	
							<select id="familyRole" name="role" class="form-control has-feedback-left" disabled>
							  <option>Choose role</option>
							  <option>Family head</option>
							  <option>Spouse</option>
							  <option>Child</option>
							 </select>
						  <span class="icon-home form-control-feedback left" aria-hidden="true"></span>
						</div>
						 <div class="col-md-12 col-sm-12 col-xs-12" style="background-color: #E2E2E2;height: 250px">
						  </div>



                      </div>
                      <!-- /Family Tab-->

                      <div class="tab-pane" id="messages-r">Coming Soon....</div>
                      <div class="tab-pane" id="settings-r">Coming Soon....</div>
                    </div>
                  </div>
				  <div class="col-xs-3">
                    <!-- required for floating -->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-right">
                      <li class="active"><a href="#home-r" data-toggle="tab"><?php echo lang("GENERAL-INFO");?></a>
                      </li>
                      <li><a href="#profile-r" data-toggle="tab"><?php echo lang("FAMILY");?></a>
                      </li>
                      <li><a href="#messages-r" data-toggle="tab">Teams</a>
                      </li>
                      <li><a href="#settings-r" data-toggle="tab">Testimony</a>
                      </li>
                    </ul>
                  </div>				
				 <!--div class="x_footer pull-right" style="margin-top:10px;margin-right:5px;">
						<!--input id="memberID" type="hidden" value="" />
                        <button type="button" onclick="('#addMemberForm')[0].reset();" class="btn btn-default" data-dismiss="modal"><?php echo lang("CANCEL");?></button>
                        <button type="submit" onclick="addMember()" class="btn btn-primary" name="submit" ><?php echo lang("SAVE");?></button>
                  </div-->
                </div>
              </div>
          </div>
      </div>
    </div>
</div>
</div>
<!-- /page content -->


<!-- Add Image Modal -->
     <div class="modal fade imgUpload" tabindex="-1" role="dialog" aria-hidden="true" id="webcamImageModal">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo lang("CAPTURE-IMAGE");?></h4>
                      </div>
                      <div class="modal-body">
                        <div class="x_panel">
						
						<div class="x_content">
						  <br />
						   <form class="form-horizontal form-label-left input_mask" action="" method="POST">

							<div class="form-group">
							
							  <div id="camera" class="col-md-9 col-sm-9 col-xs-12">
								
							  </div>
							  <div id="upload_results" style="background-color:#eee;"></div>
							</div>
						
						</div>
					  </div>
                      </div>
                      <div class="modal-footer">
                      	<button class="btn btn-default" onclick="javascript:void(webcam.snap()) <?php //session_start();?>" type="button"><?php echo lang("TAKE-SNAPSHOT");?></button>
						<button class="btn btn-default" onclick="javascript:void(webcam.configure('camera'))" type="button"><?php echo lang("CONFIGURE");?></button> 
                        <button type="button" onclick="routiE('/:code/disciples')" class="btn btn-default" data-dismiss="modal"><?php echo lang("CANCEL");?></button>
                        <button type="submit" onclick="imgDisplay()" class="btn btn-primary" name="submit" value="AddImage"><?php echo lang("SAVE");?></button>
                      </div>
					</form>
                    </div>
                  </div>
                </div>
<!-- /Add Image Modal -->


 
<script type="text/javascript" src="js/webcam/webcam.js"></script>
<script type="text/javascript">
  $(function(){
    webcam.set_api_url('data/uploadImg.php');
    webcam.set_swf_url('js/webcam/webcam.swf');
    webcam.set_quality(90);
    webcam.set_shutter_sound(true,'js/webcam/shutter.mp3');
    $('#camera').html(webcam.get_html(320,320));
});
 </script>
 <script language="JavaScript">
		webcam.set_hook( 'onComplete', 'my_completion_handler' );
		
		function take_snapshot() {
			// take snapshot and upload to server
			document.getElementById('upload_results').innerHTML = '<h1>Uploading...</h1>';
			webcam.snap();
		}
		
		function my_completion_handler(msg) {
			// extract URL out of PHP output
			/*if (msg.match(/(http\:\/\/\S+)/)) {
				var image_url = RegExp.$1;
				// show JPEG image in page
				document.getElementById('upload_results').innerHTML = 
					'<h1>Upload Successful!</h1>' + 
					'<h3>JPEG URL: ' + image_url + '</h3>' + 
					'<img src="' + image_url + '">';
				
				// reset camera for another shot
				webcam.reset();
			}
			else alert("PHP Error: " + msg);*/
			$("#profileAvatar").html('<img id="avatarImg" class="img-responsive avatar-view" src="images/members/'+msg+'" alt="Avatar" title="Change the avatar" ><input type="hidden" id="imagefile" name="imagefile" value="'+msg+'">');
			$("#imgFileName").html(msg);
			$('#webcamImageModal').modal('hide');
	    	return false;
		}
	</script>

<script>
	function imgDisplay(){
		var img=$("#imagefile").val();
		alert(img);
	}


</script>


 <!-- tags functionality -->
  <script src="js/tags/jquery.tagsinput.min.js"></script>
 
 <!-- input tags -->
  
  <script>
    function onAddTag(tag) {
      alert("Added a tag: " + tag);
    }

    function onRemoveTag(tag) {
      alert("Removed a tag: " + tag);
    }

    function onChangeTag(input, tag) {
      alert("Changed a tag: " + tag);
    }

	   
  </script>
  
  <script> $(function() {
      $('#tags').tagsInput({
        width: 'auto'
      });
    });
</script>

