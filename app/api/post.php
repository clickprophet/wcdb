<?php
include_once"../config-small.php";
session_start();	//start session 
$userId=$_SESSION["wcdbUser"]->user_id; //get the current user's id
$memberId=$_SESSION["wcdbUser"]->memberId; //get the current member's id


$type="";
if (isset($_POST["type"]))
	{
		$type=$_POST["type"];
	}
Global $mysqli;	$con=$mysqli;

//save settings
if(isset($_POST["settingId"])){
$id=$_POST["settingId"];
$name=$_POST["name"];
$value=$_POST["value"];

if(updateSetting($id,$name,$value)==1){
	echo 1;
}else{
	echo 0;
}
}

//save member details
if(isset($_POST["formName"]) && $_POST["formName"]=="saveMemberDetails"){
$id=$_POST["settingId"];
$name=$_POST["name"];
$value=$_POST["value"];

if(updateSetting($id,$name,$value)==1){
	echo 1;
}else{
	echo 0;
}
}

//save nation
if(isset($_POST["formName"]) && $_POST["formName"]=="addNation"){
$ministryDate=$_POST["ministryDate"];
$leader = $mysqli->real_escape_string($_POST['leader']);
$region = $mysqli->real_escape_string($_POST['region']);
$numberOfDesciples=$_POST["numberOfDesciples"];
$name=$_POST["name"];

$n=new nation($ministryDate,$name,$region,$leader,$numberOfDesciples);
	if($n->exists()=="false"){
		$res=$n->Add();
		addLog($userId,"New Nation",$res["msg"]);
	}else{
		$res=array('status'=>0,'msg'=>'Nation '.$name.' already exists in database');
	}
 	echo jsonResponse($res);	
}

//update nation
if(isset($_POST["formName"]) && $_POST["formName"]=="editNation"){
$ministryDate=$_POST["ministryDate"];
$leader = $mysqli->real_escape_string($_POST['leader']);
$region = $mysqli->real_escape_string($_POST['region']);
$name=$_POST["name"];

$n=new nation($ministryDate,$name,$region,$leader);
	
	$res=$n->Update();
	addLog($userId,"Update Nation",$res["msg"]);
 	echo jsonResponse($res);	
}

//add New Region
if(isset($_POST["formName"]) && $_POST["formName"]=="addRegionForm"){

$code = $mysqli->real_escape_string($_POST['code']);
$name=$_POST["name"];

$r=new region($code,$name);

$result=$r->Add();
addLog($userId,"New record",$result["msg"]); //logging the user action
echo jsonResponse($result);
	

}


//save new member
if(isset($_POST["formName"]) && $_POST["formName"]=="addMemberForm"){
		$firstName = $mysqli->real_escape_string($_POST['firstName']);
		$lastName = $mysqli->real_escape_string($_POST['lastName']);
		$address=$mysqli->real_escape_string($_POST['address']);
		$surburb=$mysqli->real_escape_string($_POST['surburb']);
		$city=$mysqli->real_escape_string($_POST['city']);
		$email = $mysqli->real_escape_string($_POST['email']);
		$phone = $mysqli->real_escape_string($_POST['phone']);
		$sex = $mysqli->real_escape_string($_POST['gender']);
		$country = $mysqli->real_escape_string($_POST['country']);
		$locality = $mysqli->real_escape_string($_POST['locality']);
		$tags=$mysqli->real_escape_string($_POST['tags']);
		$DOB = $mysqli->real_escape_string($_POST['DOB']);
		$houseChurch = $mysqli->real_escape_string($_POST['houseChurch']);
		$addedBy=$memberId;
		$image=$_POST['image'];
$m=new newMember($firstName,$lastName,$phone,$email,$sex,$locality,$country,$DOB,$tags,$address,$city,$surburb,$image,$addedBy,$houseChurch);

	if($m->nameExists()==1){
		$result=array('status' => 0,'msg'=>'Error! data not saved, name already exists' );
	}else{
		$result=$m->Add();
	}
	addLog($userId,"New Disciple",$result["msg"]); //logging the user action
	echo jsonResponse($result);
}

//delete Member
if(isset($_POST["formName"]) && $_POST["formName"]=="deleteMember"){
	$m=new member();
	$m->id = $mysqli->real_escape_string($_POST['memberId']);
	if($m->Delete()==1){
		echo 1;
	}else{
		echo 0;
	}
}

//Edit Member
if(isset($_POST["formName"]) && $_POST["formName"]=="saveMember"){
	$m=new member();
		$m->firstName = $mysqli->real_escape_string($_POST['firstName']);
		$m->lastName = $mysqli->real_escape_string($_POST['lastName']);
		$m->email = $mysqli->real_escape_string($_POST['email']);
		$m->phone = $mysqli->real_escape_string($_POST['phone']);
		$m->sex = $mysqli->real_escape_string($_POST['gender']);
		$m->country = $mysqli->real_escape_string($_POST['country']);
		$m->locality = $mysqli->real_escape_string($_POST['locality']);
		$m->DOB = $mysqli->real_escape_string($_POST['DOB']);
		$m->surburb=$mysqli->real_escape_string($_POST['surburb']);
		$m->city=$mysqli->real_escape_string($_POST['city']);
		$m->address=$mysqli->real_escape_string($_POST['address']);
		$m->tags=$mysqli->real_escape_string($_POST['tags']);
		$m->id=$mysqli->real_escape_string($_POST['id']);
		$m->familyId = $mysqli->real_escape_string($_POST['familyId']);
		$m->familyRole = $mysqli->real_escape_string($_POST['familyRole']);
		$m->houseChurch = $mysqli->real_escape_string($_POST['houseChurch']);
		$m->discipleMaker = $mysqli->real_escape_string($_POST['discipleMaker']);
	//create new family
	$result=0;
	if($m->familyId==0 && $m->familyRole!=0){
			$m->familyId=$m->newFamily();
			$result=1;
		}
	//update member details
	$result=$m->Update();
	addLog($userId,"Disciple Updated",$result["msg"]); //logging the user action
	echo jsonResponse($result);
}

//Edit User
if(isset($_POST["formName"]) && $_POST["formName"]=="saveUserForm"){
	$m=new member();
		$m->firstName = $mysqli->real_escape_string($_POST['firstName']);
		$m->familyId = $mysqli->real_escape_string($_POST['familyId']);
		$m->familyRole = $mysqli->real_escape_string($_POST['familyRole']);
		$m->lastName = $mysqli->real_escape_string($_POST['lastName']);
		$m->email = $mysqli->real_escape_string($_POST['email']);
		$m->phone = $mysqli->real_escape_string($_POST['phone']);
		$m->sex = $mysqli->real_escape_string($_POST['gender']);
		$m->country = $mysqli->real_escape_string($_POST['country']);
		$m->locality = $mysqli->real_escape_string($_POST['locality']);
		$m->DOB = $mysqli->real_escape_string($_POST['DOB']);
		$m->surburb=$mysqli->real_escape_string($_POST['surburb']);
		$m->city=$mysqli->real_escape_string($_POST['city']);
		$m->address=$mysqli->real_escape_string($_POST['address']);
		$m->tags=$mysqli->real_escape_string($_POST['tags']);
		$m->id=$mysqli->real_escape_string($_POST['id']);
	$u=new loggedInUser();
		$u->user_id= $userId;
		$password=$mysqli->real_escape_string($_POST['password']);

	//create new family
	if($m->familyId==0){
			$m->familyId=$m->newFamily();
		}

	$res1=$m->Update();
	$result=array();
	array_push($result, $res1);
	if(strlen($password)>0){
		if(strlen($password)>5 ){ //update password is user has entered new password
			$res2=$u->updatePassword($password);
		}else{
			$res2=array('status'=>0,'msg'=>'password should be at least 6 or more Characters long');
		}
		array_push($result, $res2);
	}
	
	echo jsonResponse($result);
	
	
	
	
}

//change admin settings for user
if(isset($_POST["formName"]) && $_POST["formName"]=="checkAdmin"){
	$u=new users;
		$u->userId = $mysqli->real_escape_string($_POST['userId']);
		
		//$u->memberid=$mysqli->real_escape_string($_POST['id']);
	$result= $u->checkAdmin();
	
		echo $result;
	
}


//make Member a User
if(isset($_POST["formName"]) && $_POST["formName"]=="makeUser"){
	
		$memberId = $mysqli->real_escape_string($_POST['memberId']);
		$email = $mysqli->real_escape_string($_POST['email']);
		$firstname = $mysqli->real_escape_string($_POST['firstName']);
	
		$user_active = 0;
		$clean_email=$email;
		$status = true;
		$clean_password="";
		$sql_failure = false;
		$mail_failure = false;
		$email_taken = false;
		$username_taken = false;
		$displayname_taken = false;
		$activation_token = 0;
		$success = NULL;
		$image=NULL;
		$errText="";
		$sText="";

		$result= array("id"=>"0","text"=>$errText.$sText);
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix,$result;
		
		//generate auto password
		$clean_password = trim(password_generate(7));

		//save credentials to a backup
		$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."sec_bkp (
			uid,
			pwd
			)");
		$stmt->bind_param("is", 
			$memberId,
			$clean_password
			);
		$stmt->execute();
		$stmt->close();

		//Prevent this function being called if there were construction errors
		if($status==true)
		{
			$secure_pass = generateHash($clean_password);
			$activation_token = generateActivationToken();
			
			if($emailActivation == true)
			{
				$user_active = 0;
				
				$mail = new wcdbMail();
				//Define more if you want to build larger structures
				$hooks = array(
					"searchStrs" => array("#SITE_URL","#ACTIVATION-KEY","#FIRSTNAME#","#USERNAME#","#PASSWORD#"),
					"subjectStrs" => array($websiteUrl,$activation_token,$firstname,$clean_email, $clean_password)
					);
				
				/* Build the template - Optional, you can just use the sendMail function 
				Instead to pass a message. */
				
				if(!$mail->newTemplateMsg("new-registration.html",$hooks))
				{
					$mail_failure = true;
					$errText=$errText."\n Failed to create mail message.";
				}
				else
				{
					//Send the mail. Specify users email here and subject. 
					//SendMail can have a third parementer for message if you do not wish to build a template.
					
					if($mail->sendMail($clean_email,"New User"))
					{
						$success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE2");
						$sText=$sText."\n Email sent successfully ";
						$result["id"]=1;
						$result["text"]=$errText.$sText;
					}
				}
				
			}
			else
			{
				//Instant account activation
				$user_active = 1;
				$success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE1");
			}	
			
			
			if(!$mail_failure)
			{
				//Insert the user into the database providing no errors have been found.
				$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."users (
					memberId,
					password,
					email,
					activation_token,
					last_activation_request,
					lost_password_request, 
					active,
					title,
					sign_up_stamp,
					last_sign_in_stamp
					)
					VALUES (
					?,
					?,
					?,
					?,
					'".time()."',
					'0',
					?,
					'New Member',
					'".time()."',
					'0'
					)");
				
				$stmt->bind_param("ssssi", 
					$memberId,
					$secure_pass, 
					$clean_email, 
					$activation_token, 
					$user_active);
				$stmt->execute();
				$inserted_id = $mysqli->insert_id;
				$stmt->close();
				$sText=$sText." user created";

				//Insert default permission into matches table
				$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."user_permission_matches  (
					user_id,
					permission_id
					)
					VALUES (
					?,
					'1'
					)");
				$stmt->bind_param("s", $inserted_id);
				$stmt->execute();
				$stmt->close();
				$sText=$sText.", permissions updated";
				$result["id"]=1;
				$result["text"]=$errText.$sText;
				
			}else{
			$errText=$errText." Mail send failure.";
			$result["id"]=0;
			$result["text"]=$errText.$sText;
			}
			
		}
		
		
			
	
	
	return  json_encode($result);
	//echo $result;
}

//save locality details
if(isset($_POST["formName"]) && $_POST["formName"]=="addLocalityForm"){
		$localityName = $mysqli->real_escape_string($_POST['localityName']);
		$datePlanted = $mysqli->real_escape_string($_POST['datePlanted']);
		$province = $mysqli->real_escape_string($_POST['province']);
		$country = $mysqli->real_escape_string($_POST['country']);
		$leaders = implode(",", $_POST['leaders']);
	$loc=new newLocality($localityName,$province,$datePlanted,$leaders,$country);

	if($loc->localityExists()==1){
		echo 0;
	}else{
		if($loc->Add()==1){
			addLog($userId,"new record","New Locality -".$localityName." added under ".$province." province in country ".$country); //logging the user action
			echo 1;
		}else{
			echo 0;
		}
	}
}



//update locality details
if(isset($_POST["formName"]) && $_POST["formName"]=="editLocalityForm"){
		$localityName = $mysqli->real_escape_string($_POST['localityName']);
		$datePlanted = $mysqli->real_escape_string($_POST['datePlanted']);
		$province = $mysqli->real_escape_string($_POST['province']);
		$country = $mysqli->real_escape_string($_POST['country']);
		$leaders = implode(",", $_POST['leaders']);

	$loc=new newLocality($localityName,$province,$datePlanted,$leaders,$country);
	$loc->id=$mysqli->real_escape_string($_POST['id']);
	
		$result=$loc->Update();
		echo jsonResponse($result);
		addLog($userId,"Updated record","Province -".$province." updated under ".$localityName."
		province in country ".$country); //logging the user action
	
}


//Delete locality
if (isset($_POST['formName']) && $_POST['formName'] == 'deleteLocalityForm') {
		$loc=new locality();
		$loc->id = $mysqli->real_escape_string($_POST['id']);
		if($loc->Delete()==1) { 
		 echo 1;
		}
		else{
		  echo 0;	
		}
	}		


//save province details
if(isset($_POST["formName"]) && $_POST["formName"]=="addProvinceForm"){
		$provinceName = $mysqli->real_escape_string($_POST['name']);
		$leader = $mysqli->real_escape_string($_POST['leader']);
		$country = $mysqli->real_escape_string($_POST['nation']);
		$disciples = $mysqli->real_escape_string($_POST['disciples']);
	$prov=new newProvince($provinceName,$leader,$country,$disciples);
	if($prov->provinceExists()==1){
		echo 0;
	}else{
		if($prov->Add()==1){
			addLog($userId,"new record","New province -".$provinceName." added in country ".$country); //logging the user action
			echo 1;
		}else{
			echo 0;
		}
	}
}

//save province details
if(isset($_POST["formName"]) && $_POST["formName"]=="editProvinceForm"){
		$prov=new province();
		$prov->provinceName = $mysqli->real_escape_string($_POST['name']);
		$prov->leader = $mysqli->real_escape_string($_POST['leader']);
		$prov->country = $mysqli->real_escape_string($_POST['nation']);
		$prov->disciples = $mysqli->real_escape_string($_POST['disciples']);
		$prov->id=$mysqli->real_escape_string($_POST['id']);
	
		$result=$prov->Update();
		addLog($userId,"record updated","Province -".$prov->provinceName." updated;
		  country".$prov->country); //logging the user
		echo jsonResponse($result);
}

//Delete Provinces
if (isset($_POST['formName']) && $_POST['formName'] == 'deleteProvinceForm') {
		$p=new province();
		$p->id = $mysqli->real_escape_string($_POST['id']);
		if($p->Delete()==1) { 
		 addLog($userId,"record deleted","Province deleted id:".$p->id); //logging the user
		 echo 1;
		}
		else{
		  echo 0;	
		}
	}


function jsonResponse($array) {
     //header('Content-type: application/json; charset=utf-8;');
     die(json_encode($array));
}
	
// Delete Nation
	if (isset($_POST['formName']) && $_POST['formName'] == 'deleteNation') {
		$n=new nation();
		$n->id = $mysqli->real_escape_string($_POST['code']);
	if($n->Delete()==1){
		echo 1;
	}else{
		echo 0;
	}
    }			

// Delete User
	if (isset($_POST['formName']) && $_POST['formName'] == 'deleteUser') {
		$u=new user();
		$u->userId = $mysqli->real_escape_string($_POST['userId']);
	if($u->Delete()==1){
		echo 1;
	}else{
		echo 0;
	}
    }

// Send user credentials to user
	if (isset($_POST['formName']) && $_POST['formName'] == 'sendUserCredentials') {
		$u=new user();
		$u->email = $mysqli->real_escape_string($_POST['email']);
	
	$result=$u->sendUserCredentials();
	return  jsonResponse($result);
    }


//Add New Report
if (isset($_POST['formName']) && $_POST['formName'] == 'addReportForm') {
		$members = $mysqli->real_escape_string($_POST['members']);
		$localities = $mysqli->real_escape_string($_POST['localities']);
		$houseChurches = $mysqli->real_escape_string($_POST['houseChurches']);
		$leaders = $mysqli->real_escape_string($_POST['leaders']);
		$notes = $mysqli->real_escape_string($_POST['notes']);
		$province = $mysqli->real_escape_string($_POST['province']);
		$date = $mysqli->real_escape_string($_POST['reportDate']);
		$report=new report($members,$localities,$houseChurches,$leaders,$notes,$province,$date,$userId);
		if($report->Add()==1) { 
		 echo 1;
		}
		else{
		  echo 0;	
		}
	}	

//Add New Report
if (isset($_POST['formName']) && $_POST['formName'] == 'saveReportForm') {
		$members = $mysqli->real_escape_string($_POST['members']);
		$localities = $mysqli->real_escape_string($_POST['localities']);
		$houseChurches = $mysqli->real_escape_string($_POST['houseChurches']);
		$leaders = $mysqli->real_escape_string($_POST['leaders']);
		$notes = $mysqli->real_escape_string($_POST['notes']);
		$province = $mysqli->real_escape_string($_POST['province']);
		$date = $mysqli->real_escape_string($_POST['reportDate']);
		$id = $mysqli->real_escape_string($_POST['id']);
		
	}
//Add New House Church
if (isset($_POST['formName']) && $_POST['formName'] == 'addHCForm') {
		$familyId = $mysqli->real_escape_string($_POST['familyId']);
		$datePlanted = $mysqli->real_escape_string($_POST['startDate']);
		$locality = $mysqli->real_escape_string($_POST['locality']);
		$leaders = implode(",", $_POST['leaders']);
		
		$hc=new newhouseChurch($familyId,$datePlanted,$leaders,$locality,$userId);
		$result=$hc->Add();
		addLog($userId,"New House Church created","New House church has been created "); //logging the user
		echo  jsonResponse($result);
	}

//Register members to an event 
if (isset($_POST['formName']) && $_POST['formName'] == 'eventRegForm') {
		$e=new event();
		$e->eventId = $mysqli->real_escape_string($_POST['id']);
		$e->members = implode(",", $_POST['members']);
		$e->registeredBy=$userId;
		$result=$e->Register();
		echo  jsonResponse($result);
	}

//check delegates in to event
if (isset($_POST['formName']) && $_POST['formName'] == 'eventCheckinForm') {
	$e=new event();
	$e->eventId = $mysqli->real_escape_string($_POST['eventId']);
	$e->sessionId = $mysqli->real_escape_string($_POST['sessionId']);
	$e->members = implode(",", $_POST['members']);
	$e->date =  $mysqli->real_escape_string($_POST['date']);
	$e->registeredBy=$userId;
	$result=$e->Checkin();
	echo  jsonResponse($result);
}

//Register members to an event automatically
if (isset($_POST['formName']) && $_POST['formName'] == 'eventRegFormAuto') {
		$e=new event();
		$e->eventId = $mysqli->real_escape_string($_POST['id']);
		$e->member = $mysqli->real_escape_string($_POST['member']);
		$e->token = $mysqli->real_escape_string($_POST['token']);
		$e->registeredBy=$userId;
		$result=$e->registerMember($e->member);
		echo  jsonResponse($result);
	}
//Update existing House Church
if (isset($_POST['formName']) && $_POST['formName'] == 'updateHCForm') {
		$familyId = $mysqli->real_escape_string($_POST['familyId']);
		$datePlanted = $mysqli->real_escape_string($_POST['startDate']);
		$locality = $mysqli->real_escape_string($_POST['locality']);
		$leaders = implode(",", $_POST['leaders']);
		$id=$mysqli->real_escape_string($_POST['id']);
		
		$hc=new newhouseChurch($familyId,$datePlanted,$leaders,$locality,$userId);
		$result=$hc->Update($id);
		echo  jsonResponse($result);
	}	

//Delete House church
if (isset($_POST['formName']) && $_POST['formName'] == 'deleteHouseChurchForm') {
		$hc=new houseChurch();
		$hc->id = $mysqli->real_escape_string($_POST['id']);
		if($hc->Delete()==1) { 
		 echo 1;
		}
		else{
		  echo 0;	
		}
	}		
	
	
// Edit Settings
	if (isset($_POST['submit']) && $_POST['submit'] == 'editSetting') {
		$id = $mysqli->real_escape_string($_POST['settingID']);
		$name = $mysqli->real_escape_string($_POST['name']);
		$value = $mysqli->real_escape_string($_POST['value']);
		
		// update setting
		if(updateSetting($id,$name,$value)==1) { 
		$alertBox='<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-check-circle" style="font-size:28px;"></i> Setting <strong>'.$name.'</strong> updated successfully!
                </div>';
		}
		else{
			$alertBox='<div class="alert alert-error alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-delete-circle" style="font-size:28px;"></i><strong> Error</strong> new data not saved.
                </div>';
		}


   }		

//delete temp table
if(isset($_POST["procedure"]) && $_POST["procedure"]=="clearTemp"){
	$userId = $mysqli->real_escape_string($_POST['userId']);
	if(clearTemp($userId)){
		echo 1;
	}else{
		echo 0;
	}
	
}
//---------------------------
//Managing Events
//---------------------------

//new event
if($type == 'new') {
  $startdate = $_POST['startdate'].'+'.$_POST['endDate'];
  $endDate=$startdate;
  $title =  $mysqli->real_escape_string($_POST['title']);
  $nation=$mysqli->real_escape_string($_POST['nation']);
  $allday='true';
  $stmt = $mysqli->prepare("
  	INSERT INTO cmfi_events
  	(eventTitle,userId, startDate, endDate, allday,nation) 
  	VALUES
  	(?,?,?,?,?,?)");
  $stmt->bind_param("sissss", 
						$title,
						$userId,
						$startdate,
						$endDate,
						$allday,
						$nation
						);
	$result = $stmt->execute();
	$stmt->close();	
  	$lastid = mysqli_insert_id($con);
  	addLog($userId,"new event","New event -".$title." starting on ".$startdate."  ending on ".$endDate); //logging the user action
  echo json_encode(array('status'=>'success','eventid'=>$lastid,'title'=>$title,'result'=>$result));
}

//--change event title
if($type == 'changetitle') {
    $eventid = $_POST['eventid'];
    $title = $_POST['title'];
    $update = mysqli_query($con,"UPDATE cmfi_events SET eventTitle='$title' where eventId=".$eventid."");
    if($update){
    	addLog($userId,"update event","Event title changed to -".$title); //logging the user action
      	echo json_encode(array('status'=>'success'));
    }else{
      	echo json_encode(array('status'=>'failed'));
    }
}

//modify event date
if($type == 'resetdate') {
    $title = $_POST['title'];
    $startdate = $_POST['start'];
    $enddate = $_POST['end'];
    $eventid = $_POST['eventid'];
    $update = mysqli_query($con,"UPDATE cmfi_events SET eventTitle='$title', startDate = '$startdate', endDate = '$enddate' where eventId=".$eventid."");
    if($update){
    	addLog($userId,"update event","Event dates reset - Title:".$title." starting on ".$startdate."  ending on ".$enddate); //logging the user action
      echo json_encode(array('status'=>'success'));
    }else{
      echo json_encode(array('status'=>'failed'));
    }
}

//remove event from database
if($type == 'remove') {
    $eventid = $_POST['eventid'];
    $delete = mysqli_query($con,"DELETE FROM cmfi_events where eventId=".$eventid);
    if($delete){
      echo json_encode(array('status'=>'success'));
    }
    else{
      echo json_encode(array('status'=>'failed'));
    }
}

//update event dates
	if($type == 'update')
	{
		$title = $_POST['title'];
		$startdate = $_POST['start'];
		$enddate = $_POST['end'];
		$eventid = $_POST['eventid'];
		$update = mysqli_query($con,"UPDATE cmfi_events SET eventTitle='$title', startDate = '$startdate', endDate = '$enddate' where eventId=".$eventid."");

	if($update){
		addLog($userId,"update event","Event dates updated - Title:".$title." starting on ".$startdate."  ending on ".$enddate); //logging the user action
		echo json_encode(array('status'=>'success','title'=>$title));
	}else{
		echo json_encode(array('status'=>'failed'));
	}
	}

//Full event update
	if($type == 'fullUpdate')
	{
		$title = $_POST['title'];
		$startdate = $_POST['start'];
		$enddate = $_POST['end'];
		$eventId = $_POST['eventId'];
		$eventColor=$_POST['eventColor'];
		$eventDesc=$_POST['eventDesc'];
		$nation=$_POST['nation'];
		$update = mysqli_query($con,"UPDATE cmfi_events SET eventTitle='$title', startDate = '$startdate', endDate = '$enddate', eventColor='$eventColor',eventDesc='$eventDesc',nation='$nation' where eventId=".$eventId."");

	if($update){
		addLog($userId,"update event","Event full update - Title:".$title." starting on ".$startdate."  ending on ".$enddate); //logging the user action
		echo json_encode(array('status'=>'success'));
	}else{
		echo json_encode(array('status'=>'failed'));
	}
	}
?>