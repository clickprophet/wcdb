<?php
class database{
	private $host = "localhost";
    private $db_name = "wcdb";
    private $username = "root";
    private $secret = "mysql";
    public $conn ;
    public $table_prefix="cmfi_";
 
    // get the database connection
    public function getConnection(){
 
        $this->conn = null;
 
        try{

            
             $this->conn = new mysqli($this->host, $this->username, $this->secret, $this->db_name);

            
        }catch(mysqli_sql_exception $exception){
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }
}


	

?>