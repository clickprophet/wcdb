<?php
/*
WCDB Version: 1.0.0
*/
class xregion{
	// database connection and table name
    private $conn;
    private $table_prefix = "cmfi_";
    //object properties
	private $code = "";
	private $name="";
	private $leader=0;
	private $dateModified = "0000-00-00";

	public function __construct($db){
        $this->conn = $db;
    }

    function read(){
	 $stmt = $this->conn->prepare("SELECT 
		id,
		code,
		name	
	FROM cmfi_regions 
	ORDER BY name asc");
	if (!$stmt)
	{
		echo "false";
	}
	else{
		//$stmt->bind_param("s", $data);
	
		$stmt->execute();
		$stmt->store_result();
		$num_returns = $stmt->num_rows;
		if ($num_returns > 0)
		{
			$stmt->bind_result($id,$code,$name);
			while ($stmt->fetch())
			{
				$row[] = array(
					'id' => $id, 
					'code'=>$code, 
					'name'=>$name
				);
			}
			$stmt->close();
			// set response code - 200 OK
	   		http_response_code(200);
	 
	    	// show products data in json format
	    	return $row;
			
		}else{
 
	    	// set response code - 404 Not found
	    	http_response_code(404);
	 
	    	// tell the user no products found
	    	
	        $row=array("message" => "No regions found.");
	        return $row;
    	
	}
}
}
	

	/*public function Add() {
	global $mysqli,$db_table_prefix; 
	$time=date("Y-m-d");
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."regions (
		code,
		name,
		leader,
		modifiedDate
		)
		VALUES (
		?,
		?,
		?,
		?
		)");
	$stmt->bind_param("ssss", 
						$this->code,
						$this->name,
						$this->leader,
						$time
						);
	
	if ($stmt->execute()) { 
		   $stmt->close();
		   return 1;
		} else {
			$stmt->close();
		   return 0;
		}
	
}*/
}






?>