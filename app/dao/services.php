<?php
include_once ('DBConnection.php');
include_once ('dao.php');

// Connection class instance
$dbConClass = new DBConnection();
$con = $dbConClass->getConnection();
// DAO object
$services = new Dao($con);

// Fetch all countries
if(isset($_POST['cFetch'])){
    echo $services->findAllCountries();
}

// Filter events for Ajax Queries
if(isset($_POST['eventId'])){
    $eventId = $_POST['eventId'];
    echo $services->findEventById($eventId);
}

// Filter events fir Axios queries
if(isset($_GET['eventId'])){
    $eventId = $_GET['eventId'];
    echo $services->findEventById($eventId);
}