<?php

class Dao{

    // A instance of DB Connexion
    private $bdCon;

    /**
     * Dao constructor.
     */
    public function __construct($con)
    {
        $this->bdCon = $con;
    }

    // get list of countries
    public function findAllCountries(){
        try{
            $req = $this->bdCon->prepare("SELECT * FROM cmfi_countries ORDER BY name ASC");
            $req->execute();
            // Return coutries to JSon format
            return json_encode($req->fetchAll(\PDO::FETCH_ASSOC));
        }catch (\Exception $e){
            throw new \RuntimeException("cmfievent.findAllCountries.error : " .$e->getMessage());
        }
    }

    // Find Event By ID
    public function findEventById($eventId){
        try{
            $req = $this->bdCon->prepare("SELECT * FROM cmfi_events cf WHERE cf.eventId = :id");
            $req->execute([':id' => $eventId]);
            // Reesult to JSon
            return json_encode($req->fetch(PDO::FETCH_ASSOC));
        }catch (\Exception $e){
            throw new \RuntimeException("cmfievent.findEventById.error : " .$e->getMessage());
        }
    }
}
