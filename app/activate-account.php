<?php 
/*
Quest Version: 1.0.0
*/
require_once("config/config.php");
global $websiteUrl;
if (!securePage($_SERVER['PHP_SELF'])){die();}
$header="";


//Get token param
if(isset($_GET["token"]))
{	
	$token = $_GET["token"];	
	if(!isset($token))
	{
		$errors[] = lang("FORGOTPASS_INVALID_TOKEN");
	}
	else if(!validateActivationToken($token)) //Check for a valid token. Must exist and active must be = 0
	{
		$errors[] = lang("ACCOUNT_TOKEN_NOT_FOUND");
	}
	else
	{
		//Activate the users account
		if(!setUserActive($token))
		{
			$errors[] = lang("SQL_ERROR");
			$header='<h2 style="text-align: center;margin:2px;">Activation Failure! Try Again.</h2>';
		}
	}
}else{
	$errors[] = lang("FORGOTPASS_INVALID_TOKEN");
	
}
if(count($errors) == 0){
	$header='<h2 style="color:#66b266;text-align: center;margin:2px;">Activation Success! Welcome to WCDB</h2>';
  //redirect($websiteUrl."login.php");
	$successes[] = lang("ACCOUNT_ACTIVATION_COMPLETE");
	
}
?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>wcdb| account activaton </title>

  <!-- Bootstrap core CSS -->

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="css/custom.css" rel="stylesheet">
  <link href="css/icheck/flat/green.css" rel="stylesheet">


  <script src="js/jquery.min.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

<style>
.input-group{
	width:100%;
}

body{
	background-image:url("images/collage1.png");
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-position: center;
}
</style>
</head>
<body class="nav-md">

  <div class="container body">


    <div class="main_container">
    
    <!--wraps the whole content-->
	  <div id="wrapper">
	  <div id="login" class="animate form well" style="background-color:rgba(255, 255, 255, 0.84);">
	  <div>
			<h3><img src="images/logo.png" style="width: 26px;"></img> Christian Missionary Fellowship International</h3>
		</div>
	  <?php  echo $header; ?>
	  <br/>
         <?php  echo resultBlock($errors,$successes); 
         header( "refresh:4; url=$websiteUrl" );

         ?>

         </div>
	 </div>
	 
   </div>
</div>
	
  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
    <br>
    <a type="button" class="btn btn-primary" href="<?php echo $websiteUrl;?>">Go to Login</a>


  </div>
  
  
  <script src="js/bootstrap.min.js"></script>

  <!-- gauge js -->
  <script type="text/javascript" src="js/gauge/gauge.min.js"></script>
  <script type="text/javascript" src="js/gauge/gauge_demo.js"></script>
  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

  
  <!-- input mask -->
  <script src="js/input_mask/jquery.inputmask.js"></script>
  <!-- chart js -->
  <script src="js/chartjs/chart.min.js"></script>

  <script src="js/custom.js"></script>
<!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script>

</body>
</html>

