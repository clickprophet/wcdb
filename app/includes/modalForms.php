
	 <!-- Modal Forms -->
	 <!-- New Province -->
     <div class="modal fade newProvince" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add New Province</h4>
                      </div>
                      <div class="modal-body">
                        <div class="x_panel">
						
						<div class="x_content">
						  <br />
						   <form class="form-horizontal form-label-left input_mask" action="" method="POST">

							<div class="form-group">
							  <label class="control-label col-md-3 col-sm-3 col-xs-12">Name<span class="required">*</span></label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" class="form-control" name="provinceName" placeholder="province" required="required">
							  </div>
							</div>
							<div class="form-group">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12">Country Name</label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<select name="nation" class="form-control">
								  <option>Choose option</option>
								  <?php 
									foreach ($countries as $c) 
										{
											echo '<option value="'.$c["code"].'">'.$c["name"].'</option>';
										}
								  ?>
								</select>
							  </div>
							</div>
						</div>
					  </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" name="submit" value="AddProvince">Save changes</button>
                      </div>
					</form>
                    </div>
                  </div>
                </div>
				
<!-- New Locality -->				
			<div class="modal fade newLocality" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add New Locality</h4>
                      </div>
                      <div class="modal-body">
                        <div class="x_panel">
						
						<div class="x_content">
						  <br />
						   <form id="addLocalityForm" class="form-horizontal form-label-left input_mask" action="" method="POST">

							<div class="form-group">
							  <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" class="form-control" name="localityName" id="localityName" placeholder="locality" required="required">
							  </div>
							</div>
							<div class="form-group">
							  <label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date<span class="required">*</span>
							  </label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<input id="datePlanted" name="datePlanted" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" >
								</div>
							</div>
							<script>
											$( function() {
												$( "#datePlanted" ).datepicker({ changeMonth: true,
      									changeYear: true});
												$( "#datePlanted" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
												 $( "#datePlanted" ).datepicker( "option", "showAnim", "clip");
											} );
											</script>
							<div class="form-group">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12">Country Name</label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<select name="lnation" id="lnation" class="form-control">
								  <option>Choose option</option>
								  <?php 
									foreach ($countries as $c) 
										{
											echo '<option value="'.$c["code"].'">'.$c["name"].'</option>';
										}
								  ?>
								</select>
							  </div>
							
							</div>
							<div class="form-group">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12">Province</label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
							  <select name="lprovince" id="lprovince" class="select2_group form-control">
								 <?php
									$countryProv=getCountryWithProvince();
									foreach ($countryProv as $c) 
										{
											echo '<optgroup  label="'.$c["name"].'">';
											$Cprovinces=getProvinces($c["code"]);
											foreach ($Cprovinces as $cp) 
												{
												  echo '<option value="'.$cp["id"].'">'.$cp["name"].'</option>';
												}
											echo '</optgroup>';
											
										}
								  ?>
								</select>
							  </div>
							
							</div>
							</form>
						</div>
					  </div>
				
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" onclick="addLocality()"  class="btn btn-primary" name="submit" value="AddLocality">Save changes</button>
                      </div>
					
                    </div>
                  </div>
                </div>
				
	<!-- Add New Member-->		
				<div id="newMember" class="modal fade newMember" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add New Member</h4>
                      </div>
                      <div class="modal-body">
                        <div class="x_panel">
						
						<div class="x_content">
						  <br />
						   <form id="addMemberForm" class="form-horizontal form-label-left input_mask" action="" method="post">

							<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							  <input type="text" class="form-control has-feedback-left" name="firstName" id="firstName" placeholder="First Name">
							  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							  <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name">
							  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							  <input type="text" class="form-control has-feedback-left" id="email" name="email" placeholder="Email">
							  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							  <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
							  <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
							</div>
							<div class="form-group">
							  <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth
							  </label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<input id="birthday" name="DOB" class="date-picker form-control col-md-7 col-xs-12" type="text" >
								</div>
								 
								  <script>
											$( function() {
												$( "#birthday" ).datepicker({ changeMonth: true,
      									changeYear: true});
												$( "#birthday" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
												 $( "#birthday" ).datepicker( "option", "showAnim", "clip");
											} );
											</script>
							</div>
							<div class="form-group">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender  <span class="required">*</span></label>
								
								 <div class="col-md-9 col-sm-9 col-xs-12">
								 <p>
								  M:
								  <input type="radio" class="flat" name="gender" id="genderM" value="M" checked="" required /> F:
								  <input type="radio" class="flat" name="gender" id="genderF" value="F" />
								</p>
								</div>
							</div>

							<div class="form-group">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								
								<select id="countryName" name="country" class="form-control">
								  <option>Choose option</option>
								 <?php 
									foreach ($nations as $n) 
										{
											echo '<option  value="'.$n["code"].'">'.$n["name"].'</option>';
										}
								  ?>
								</select>
							  </div>
							
							</div>
							<div class="form-group">
							  <label class="control-label col-md-3 col-sm-3 col-xs-12">Locality</label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<select id="localities" name="locality" class="form-control">
								  <option>Choose option</option>
								 </select>
							  </div>
							</div>
							</form>							
						</div>
					  </div>
                      </div>
					  
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button  onclick="addMember()" type="submit" class="btn btn-primary" name="submit" value="AddMember">Submit</button>
                      </div>
						
                    </div>
                  </div>
                </div>
				   		
				
	<!-- Add New Event-->		
				<div id="newEvent" class="modal fade newEvent" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add New Event</h4>
                      </div>
                      <div class="modal-body">
                        <div class="x_panel">
						
						<div class="x_content">
						  <br />
						   <form id="addEventForm" class="form-horizontal form-label-left input_mask" action="" method="post">

							<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
							  <input type="text" class="form-control has-feedback-left" name="eventTitle" id="eventTitle" placeholder="Event Title">
							  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
							  <input type="text" class="form-control has-feedback-left" id="eventDesc" name="eventDesc" placeholder="Description">
							  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							  <input type="text" class="date date-picker form-control has-feedback-left" id="startDate" name="startDate" placeholder="Start Date">
							  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							  <input type="text" class="date date-picker form-control" id="endDate" name="endDate" placeholder="End Date">
							  <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
							</div>
							<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Event Color</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="demo1 form-control" value="#5367ce" />
                      </div>
                    </div>					

							<div class="form-group">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								
								<select id="countryName" name="country" class="form-control">
								  <option>Choose option</option>
								 <?php 
									foreach ($nations as $n) 
										{
											echo '<option  value="'.$n["code"].'">'.$n["name"].'</option>';
										}
								  ?>
								</select>
							  </div>
							
							</div>
							<div class="form-group">
							  <label class="control-label col-md-3 col-sm-3 col-xs-12">Locality</label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<select id="localities" name="locality" class="form-control">
								  <option>Choose option</option>
								 </select>
							  </div>
							</div>
							</form>							
						</div>
					  </div>
                      </div>
					  
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button  onclick="addMember()" type="submit" class="btn btn-primary" name="submit" value="AddMember">Submit</button>
                      </div>
						
                    </div>
                  </div>
                </div>
				   		
			
	<!-- Add new Nation -->			
				
				<div id="newNation" class="modal fade newNation" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add New Nation</h4>
                      </div>
                      <div class="modal-body">
                        <div class="x_panel">
						
						<div class="x_content">
						  <br />
						   <form id="addNation" class="form-horizontal form-label-left input_mask" action="" method="post">

							
							<div class="form-group">
							  <label class="control-label col-md-3 col-sm-3 col-xs-12">Ministry Start Date<span class="required">*</span>
							  </label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<input style="z-index:99999 !important;" id="nbirthday" name="nationDate" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" >
							  </div>
							</div>
							 <script>
											$( function() {
												$( "#nbirthday" ).datepicker({ changeMonth: true,
      									changeYear: true});
												$( "#nbirthday" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
												 $( "#nbirthday" ).datepicker( "option", "showAnim", "clip");
											} );
											</script>

							<div class="form-group">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12">Country Name</label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<select id="nationName" name="nationName" class="form-control">
								  <option>Choose option</option>
								 <?php 
									foreach ($countries as $c) 
										{
											echo '<option  value="'.$c["code"].'">'.$c["name"].'</option>';
										}
								  ?>
								</select>
							  </div>
							
							</div>
							
						</div>
					  </div>
                      </div>
					  </form>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button onclick="saveNation()" id="submit" class="btn btn-primary" value="AddNation" >Submit</button>
                      </div>
						
                    </div>
                  </div>
                </div>
				
<!-- Add new report modal -->				
				<div class="modal fade newReport" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add New Report</h4>
                      </div>
                      <div class="modal-body">
                        <div class="x_panel">
						
						<div class="x_content">
						  <br />
						   <form class="form-horizontal form-label-left input_mask" action="" method="post">	
							
							<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							  <input type="number" class="form-control has-feedback-left" name="members" id="inputSuccess2" placeholder="Members">
							  <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							  <input type="number" class="form-control has-feedback-left" name="localities" id="inputSuccess2" placeholder="Localities">
							  <span class="fa fa-institution form-control-feedback left" aria-hidden="true"></span>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							  <input type="number" class="form-control has-feedback-left" name="houseChurches" id="inputSuccess2" placeholder="House Churches">
							  <span class="fa fa-home form-control-feedback left" aria-hidden="true"></span>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							  <input type="number" class="form-control has-feedback-left" name="leaders" id="inputSuccess2" placeholder="Leaders">
							  <span class="fa fa-male form-control-feedback left" aria-hidden="true"></span>
							</div>
							
							<div class="form-group">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								
								<select id="countryName" name="nation" class="form-control">
								  <option>Choose option</option>
								   <?php 
									foreach ($nations as $n) 
										{
											echo '<option  value="'.$n["code"].'">'.$n["name"].'</option>';
										}
								  ?>
								</select>
							  </div>
							</div>
							
							
							<div class="form-group">
							  <label class="control-label col-md-3 col-sm-3 col-xs-12">Date as at<span class="required">*</span>
							  </label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<input id="reportDate" name="reportDate" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" >
							  </div>
							</div>
							 <script>
											$( function() {
												$( "#reportDate" ).datepicker({ changeMonth: true,
      									changeYear: true});
												$( "#reportDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
												 $( "#reportDate" ).datepicker( "option", "showAnim", "clip");
											} );
											</script>
							<div class="form-group">
							  <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
							  <div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="notes" rows="5" class="form-control" placeholder="Notes"></textarea>
							  </div>
							</div>
						</div>
					  </div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								<button type="submit" class="btn btn-primary" name="submit" value="AddReport">Submit</button>
							</div>
				</form>
			</div>
		</div>
	</div>


	 <!--/ Modal Forms -->