   <!-- footer content -->
   <div class="col-md-12 col-sm-12 col-xs-12" style="background: white">
      <footer>
        <div class="pull-right">
         World Conquest Database <a href="https://www.cmfionline.org">cmfionline</a>
        </div>
        <div class="clearfix"></div>
      </footer>
    </div>
      <!-- /footer content -->
    </div>
  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>


  <script src="js/bootstrap.min.js"></script>


  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- tags -->
  <script src="js/tags/jquery.tagsinput.min.js"></script>
  <!-- switchery -->
  <script src="js/switchery/switchery.min.js"></script>
  <!-- select2 -->
  <script src="js/select/select2.full.js"></script>
  <!-- input mask -->
  <script src="js/input_mask/jquery.inputmask.js"></script>
    <!-- color picker -->
  <script src="js/colorpicker/bootstrap-colorpicker.min.js"></script>
  <script src="js/colorpicker/docs.js"></script>
 
 <!-- form wizard -->
  <script type="text/javascript" src="js/wizard/jquery.smartWizard.js"></script>
  

 <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- flot js -->
  <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]>
  <script type="text/javascript" src="js/flot/jquery.flot.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
  <script type="text/javascript" src="js/flot/date.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
  <script type="text/javascript" src="js/flot/curvedLines.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script-->
  


   <!-- Datatables -->

        <!-- Datatables-->
        <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.keyTable.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
        <script src="js/datatables/dataTables.scroller.min.js"></script>
		
		
  
  <!-- PNotify -->
  <!--script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script-->
  <script type="text/javascript" src="js/notify/pnotify.custom.min.js"></script>

  <script src="js/streamtables/stream_table.js" type="text/javascript"></script>
  
 <script src="js/mustache.min.js" type="text/javascript"></script>
  
  
  <script src="js/custom.js"></script>


 
  <!-- skycons -->
  <script src="js/skycons/skycons.min.js"></script>
  


<!-- chart js -->
  <script src="js/chartjs/chart.min.js"></script>
 
  
 <!-- input_mask -->
  <script>
    $(document).ready(function() {
      $(":input").inputmask();
      
      });
  </script>
  <!-- /input mask -->
<script>
  $(window).on('load', function(){
    $("#loaderImg").hide(); //hide loader image
    
    //route to home page if no hash route is given
    var str=window.location.href;
    if(!str.includes('#')){
      routie('/');
    }
   

});
</script>
  <!-- /footer content -->

  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  
</body>

</html>
