<?php
//Get Nations
$nations=getNations();
$countries=getCountries();
$nationName=nationName($loggedInUser->nation);
if ($nationName==""){$nationName= lang("WORLD_CONQUEST");}
?>

	  
	  <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a style="font-size=10px;" href="." class="site_title"><i class="icon-globe"></i> <span><?php echo $nationName;?></span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">
              <img src="images/members/<?php echo $loggedInUser->image;?>" alt="..." class="img-circle profile_img" onerror="if (this.src != 'images/members/user.png') this.src = 'images/members/user.png';">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2><?php echo $loggedInUser->fullName;?></h2>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
              <h3><?php echo $loggedInUser->title;?></h3>
              <ul class="nav side-menu">
                <!--li aria-expanded="false"><a a onclick="openPage(4)"><i class="icon-globe"></i> <?php echo lang("NATIONS");?> </a></li-->
                  <!--ul class="nav child_menu " style="display: none">
				  <li><a data-toggle="modal" data-target=".newNation">Add New</a>
                    </li>
					<?php if(!empty($nations)&&$loggedInUser->nation=="Global"){
							foreach ($nations as $n) {
							echo '<li><a href="#">'.$n['name'].'<span class="fa fa-chevron-down"></span></a>'; //list all the nations on top of hierarchy
							$provinces=getProvinces($n['code']);
								echo'<ul class="nav child_menu" style="display: none">
								<li><a data-toggle="modal" data-target=".newProvince">Add Province</a></li>
								';
								if(!empty($provinces)){
									foreach ($provinces as $p) {
									echo '<li><a href="#">'.$p['name'].'<span class="fa fa-chevron-down"></span></a>'; //list all the provinces 
									
										$localities=getLocalities($p['id'],$n['code']);
										echo'<ul class="nav child_menu" style="display: none">
												';
										if(!empty($localities)){
											foreach ($localities as $l) {
											echo '<li><a href="#">'.$l['localityName'].'</a></li>'; //list all the localities 
											}
										}
										echo '</ul></li>';
									
									}
								}
								echo '</ul></li>';
								
							} 
						} else {
							
						}
						
						
						?>
                    
                  </ul>
                </li-->
                <?php if($loggedInUser->isAdmin==1){?>
				<li><a><i class="icon-plus"></i>&nbsp; <?php echo lang("NEW");?> <span class="fa fa-chevron-down"></span></a>
				  <ul class="nav child_menu" style="display: none">
				  	<li><a onclick="openPage(19)"><?php echo lang("DISCIPLE");?></a></li>
					<!--li><a data-toggle="modal" data-target=".newMember">Member</a></li>
					<li><a data-toggle="modal" data-target=".newLocality">Locality</a></li>
					<li><a data-toggle="modal" data-target=".newProvince">Province</a></li>
					<li><a data-toggle="modal" data-target=".newEvent">Event</a></li>
					<li><a data-toggle="modal" data-target=".newReport">Report</a></li-->
					<?php if($loggedInUser->nation=="Global"){echo'<li><a  onclick="openPage(22)">'.lang("NATION").'</a></li>';} ?>
				  </ul>
				</li>
				
                <li><a><i class="icon-eyeglasses"></i> <?php echo lang("MANAGE");?> <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
					<!--li><a onclick="openPage(5)"><?php echo lang("DISCIPLES");?></a></li>
					<!li><a onclick="openPage(18)"><?php echo lang("EVENTS");?></a></li>
					<!li><a onclick="openPage(3)">Provinces</a></li-->
					<li><a onclick="openPage(4)"><?php echo lang("NATIONS");?></a></li>
					<!--li><a onclick="openPage(6)">Reports</a></li-->
                  </ul>
                </li><?php }?>
				<!--li><a><i class="icon-book-open"></i> <?php echo lang("NEWSLETTER");?> <span class="fa fa-chevron-down"></span></a>
				 <ul class="nav child_menu" style="display: none">
					<li><a onclick="javascript:;">New</a></li>
					<li><a onclick="javascript:;">Past Editions</a></li>
                  </ul>
				</li-->
                <!--li><a><i class="fa fa-users"></i> Members <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a data-toggle="modal" data-target=".newMember">Add New</a>
				
                    </li>
                    <li><a onclick="openPage(5)">Manage</a>
                    </li>
                  </ul>
                </li-->
			  <!--li><a  onclick="openPage(2)" ><i class="icon-calendar"></i> <?php echo lang("CALENDAR");?> </a>
			  </li-->
			  <li><a ><i class="icon-wrench"></i> <?php echo lang("TOOLS");?> <span class="fa fa-chevron-down"></span></a>
			  <ul class="nav child_menu" style="display: none">
                    <li><a onclick="openPage(11)"><?php echo lang("IMPORT");?></a>
                    </li>
                    <li><a href="#"><?php echo lang("SYSTEM_LOGS");?></a>
                    </li>
                  </ul>
			  </li>
				<?php if($loggedInUser->isAdmin==1){?>
                <li><a><i class="icon-settings"></i> <?php echo lang("ADMIN");?> <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu" style="display: none">
							<li><a onclick="openPage(9)"><?php echo lang("USERS");?></a>
							</li>
							<li><a onclick="openPage(8)"><?php echo lang("SETTINGS");?></a>
							</li>
							<li><a onclick="openPage(25)">Page Settings</a>
							</li>
							<li><a onclick="openPage(13)"><?php echo lang("BACKUP");?></a>
							</li>
						</ul>
                  
                </li><?php }?>
              </ul>
            </div>
           

          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a href="logout.php" data-toggle="tooltip" data-placement="top" title="Logout">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

	  
	 <!-- Modal Forms -->
	 

	
<!-- Add new report modal -->				
<div id="newReport" class="modal fade newReport" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Add New Report</h4>
					</div>
					<div class="modal-body">
						<div class="x_panel">
						
						<div class="x_content">
						  <br />
						   <form id="addReportForm" class="form-horizontal form-label-left input_mask" action="" method="post">
							
										<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
											<input type="number" class="form-control has-feedback-left" name="members" id="members" placeholder="Members">
											<span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
											<input type="number" class="form-control has-feedback-left" name="localities" id="localities" placeholder="Localities">
											<span class="fa fa-institution form-control-feedback left" aria-hidden="true"></span>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
											<input type="number" class="form-control has-feedback-left" name="houseChurches" id="houseChurches" placeholder="House Churches">
											<span class="fa fa-home form-control-feedback left" aria-hidden="true"></span>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
											<input type="number" class="form-control has-feedback-left" name="leaders" id="leaders" placeholder="Leaders">
											<span class="fa fa-male form-control-feedback left" aria-hidden="true"></span>
										</div>
										
										<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
											
											<select id="rCountryName" name="nation" class="form-control">
												<option>Choose option</option>
												<?php 
												foreach ($nations as $n) 
													{
														echo '<option  value="'.$n["code"].'">'.$n["name"].'</option>';
													}
												?>
											</select>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12">Date as at<span class="required">*</span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
											<input id="reportDate" name="reportDate" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" >
											</div>
										</div>
										<script>
														$( function() {
															$( "#reportDate" ).datepicker({ changeMonth: true,
															changeYear: true});
															$( "#reportDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
															$( "#reportDate" ).datepicker( "option", "showAnim", "clip");
														} );
														</script>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
											<textarea id="notes" name="notes" rows="5" class="form-control" placeholder="Notes"></textarea>
											</div>
										</div>
									</div>
									</form>
									</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										<button onclick="addReport()" id="submitRpt" type="submit" class="btn btn-primary" name="submit" value="AddReport">Submit</button>
									</div>
					</div>
				</div>
			</div>


<!-- Add New Event-->   
<!--div id="eventModal" class="modal fade newEvent" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-md">
            <div class="modal-content" >

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">New Event</h4>
              </div>
              <div class="modal-body well">
                <div class="x_panel">
            
            <div class="x_content">
              <br />
               <form id="addEventForm" class="form-horizontal form-label-left input_mask" action="" method="post">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <input type="text" class="form-control has-feedback-left" name="eventTitlex" id="eventTitlex" placeholder="Event Title"  value="">
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <input type="text" class="form-control has-feedback-left" id="eventDescx" name="eventDescx" placeholder="Description" value="">
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <input type="text" class="form-control has-feedback-left" id="reservation" name="reservation" placeholder="Description" value="03/18/2018 - 03/23/2018">
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
              </div>

              
              <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <input type="text" class="date date-picker form-control has-feedback-left" id="startDatex" name="startDatex" placeholder="Start Date" value="">
                <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <input type="text" class="date date-picker form-control" id="endDatex" name="endDatex" placeholder="End Date">
                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
              </div>
              <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Event Color</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="input-group demo2">
                          <input type="text" id="eventColorx" name="eventColorx" value="" class="form-control" />
                          <span class="input-group-addon"><i></i></span>
                        </div>
                      </div>
                    </div>
              <input type="hidden" id="eventIdx" name="eventIdx" value=""/>
              
              </form>             
            </div>
            </div>
            </div>
    
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button  onclick="saveEvent()" type="submit" class="btn btn-primary" name="submit" value="AddMember" data-dismiss="modal">Submit</button>
            </div>
  
          </div>
        </div>
      </div-->								
	
			
	<!-- Add new Region -->			
				
<div id="newRegion" class="modal fade newRegion" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-md">
						<div class="modal-content">

							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
								</button>
								<h4 class="modal-title" id="myModalLabel">Add New Region</h4>
							</div>
							<div class="modal-body">
								<div class="x_panel">
						
									<div class="x_content">
										<br />
										<form id="addRegionForm" class="form-horizontal form-label-left input_mask" action="" method="post">

									<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Region Code">
						              <input type="text" class="form-control has-feedback-left" id="regionCode" name="regionCode" placeholder="Region Code" required>
						              <span class="icon-key form-control-feedback left" aria-hidden="true"></span>
						            </div>
						            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Region Name">
						              <input type="text" class="form-control has-feedback-left" id="regionName" name="regionName" placeholder="Region Name">
						              <span class="icon-pointer form-control-feedback left" aria-hidden="true"></span>
						            </div>
										
									</form>
									</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
									<button onclick="saveRegion()" id="submit" class="btn btn-primary" value="AddNation" data-dismiss="modal">Submit</button>
								</div>
				
							</div>
						</div>
					</div>
				


	 <!--/ Modal Forms -->



