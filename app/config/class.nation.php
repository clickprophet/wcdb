<?php
/*
WCDB Version: 1.0.0
*/


class nation 
{
	public $date = "0000-00-00";
	public $code="";
	public $region="";
	public $leader=0;
	public $id=0;
	private $modifiedDate="0000-00-00";
	
	
	function __construct($date_, $code_,$region_,$leader_)
	{
		//Used for display only
		$this->date = $date_;
		$this->code = $code_;
		$this->leader=$leader_;
		$this->region=$region_;
		$this->modifiedDate=date("Y-m-d H:i:s");
		
	}
	
	public function Add()
	{
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;
		

		//Insert the user into the database providing no errors have been found.
		$stmt = $mysqli->prepare("INSERT INTO cmfi_nations (
			nationName,
			nationDate,
			nationRegion,
			nationLeader,
			modifiedDate
			)
			VALUES (
			?,
			?,			
			?,
			?,
			?			
			)");
		
		$stmt->bind_param("sssis", $this->code, $this->date,$this->region,$this->leader,$this->modifiedDate);
		try{
			$stmt->execute();
		   	$result=array('status'=> 1,'msg'=>'New Nation '.$this->code.' successfully created!');
		   	$stmt->close();
		} catch (Exception $e) {
			$stmt->close();
		   	$result=array('status'=> 0,'msg'=>$e->getMessage());
		}
		return $result;
		
	}

	 function Update()
	{
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;
		

		//Insert the user into the database providing no errors have been found.
		$stmt = $mysqli->prepare("UPDATE  ".$db_table_prefix."nations 
			SET (
			nationDate=?,
			nationRegion=?,
			nationLeader=?,
			modifiedDate
			)
			WHERE nationName=?");
		
		$stmt->bind_param("ssiss", $this->date,$this->region,$this->leader,$this->modifiedDate,$this->code);
		try{
			$stmt->execute();
			$result=array('status' => 1, 'msg'=>'successfully updated nation');
		   	$stmt->close();
		} catch (Exception $e) {
			$stmt->close();
		   	$result=array('status'=> 0,'msg'=>$e->getMessage());
		}
		return $result;
		
	}

	function exists(){
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;
		$stmt = $mysqli->prepare("SELECT 
				nationName
			FROM ".$db_table_prefix."nations 
			WHERE nationName=?
		");
		$stmt->bind_param("s",$this->code);
		$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		$stmt->close();
		if($check > 0){
			return "true";
		}else{
			return "false";
		}
	}

	public function Delete(){
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."nations 
		WHERE nationID=? ");
		$stmt->bind_param('i', 	$this->id);
		$stmt->execute();
		$stmt->close();
		return 1;

	}
	
	public function Get()
	{
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;
		$stmt = $mysqli->prepare("SELECT 
				name,
				region,
				continent,
				CONCAT(firstName, ', ', LastName) as Leader, 
				phone, 
				email 
			FROM ".$db_table_prefix."nations 
			LEFT JOIN ".$db_table_prefix."countries 
			ON ".$db_table_prefix."nations.nationName=".$db_table_prefix."countries.code
			LEFT JOIN ".$db_table_prefix."members 
			ON ".$db_table_prefix."members.id=".$db_table_prefix."nations.nationLeader
		");
	$stmt->execute();
	$stmt->bind_result($name, $region,$continet,$leader,$phone,$email);
	
	while ($stmt->fetch()){
		$row[] = array('taskId' => $taskId, 'taskTitle' => $taskTitle, 'lastUpdated' => $LastUpdated);
	}
	$stmt->close();
	return ($row);
	
		
	}


	
}



?>