<?php
/*
DSP Version: 1.0.0
*/


class News 
{
	public $title = "No title";
	public $datestamp;
	public $announcement=NULL;
	public $active=0;
	public $url =NULL;
	public $postedby = NULL;

	
	function __construct($title_,$datestamp_, $active_, $url_, $announcement_,$postedby_)
	{
		//Used for display only
		$this->title = $title_;
		
		//Sanitize
		$this->datestamp = $datestamp_;
		$this->active = $active_;
		$this->url = $url_;
		$this->announcement = $announcement_;
		$this->postedby = $postedby_;
		
	}
	
//Add new announcement
	public function addNews() 
	{
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."announcements (
		title,
		datestamp,
		active,
		url,
		announcement,
		postedby
		)
		VALUES (
		?,
		?,
		?,
		?,
		?,
		?
		)
		");
		/* Execute the statement */
		$stmt->bind_param("ssisss",$this->title, $this->datestamp, $this->active, $this->url, $this->announcement,$this->postedby);
		
		$stmt->execute();
		$stmt->close();
	$i++;
	return $i;
}


}



?>