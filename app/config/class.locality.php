<?php
/*
wcdb Version: 1.0.0
*/

class locality{
	public $id=0;

	public function Delete()
	{
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."localities 
		WHERE id=? ");
		$stmt->bind_param('i', 	$this->id);
		$stmt->execute();
		$stmt->close();
		return 1;
	}

}

class newLocality 
{
	public $country = "";
	private $localityName="UNKOWN";
	public $province = 0;
	public $datePlanted="0000-00-00";
	public $leaders="";
	public $id=0;
	

	
	function __construct($localityName_,$province_,$datePlanted_,$leaders_,$country_)
	{
		//assign values
		$this->country = $country_;
		$this->localityName=$localityName_;
		$this->province=$province_;
		$this->datePlanted=$datePlanted_;
		$this->leaders=$leaders_;
		
	}
	
	
//Functions that interact with locality data
//------------------------------------------------------------------------------

//Add new properties
	public function Add() 
	{
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$time=date("Y-m-d h:i:s");
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."localities (
		localityName,
		province,
		leader,
		datePlanted,
		country,
		modifiedDate
		
		)
		VALUES (
		?,
		?,
		?,
		?,
		?,
		?
		)");
		/* Execute the statement */
		$stmt->bind_param("sissss",$this->localityName, $this->province, $this->leaders, $this->datePlanted, $this->country, $time);
		
		$result=$stmt->execute();
		$stmt->close();
	return $result;
}

	public function Update() 
	{
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$time=date("Y-m-d h:i:s");
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."localities 
		SET
			localityName=?,
			province=?,
			leader=?,
			datePlanted=?,
			country=?,
			modifiedDate=?
		WHERE id=?
		LIMIT 1
		");
	try{
		/* Execute the statement */
		$stmt->bind_param("sissssi",$this->localityName, $this->province, $this->leaders, $this->datePlanted, $this->country, $time,$this->id);
		
		$stmt->execute();
		$stmt->close();
		$result = array('status' => 1, 'msg'=>'Locality successfully updated');
	}catch(Exception $e){
		$result = array('status' => 0, 'msg'=>$e);
	}
	return $result;
}
public	function localityExists()
{
	global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("SELECT localityName FROM ".$db_table_prefix."localities 
		WHERE localityName=? and province=?");
		$stmt->bind_param('ss', $this->localityName,$this->province);
		$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		$stmt->close();
		if($check != 0){
			return 1;
		}else{
			return 0;
		}
}

}



?>