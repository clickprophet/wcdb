<?php
require_once("../config-small.php");
global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix,$result;

        $memberId = $mysqli->real_escape_string($_POST['memberId']);
		$email = $mysqli->real_escape_string($_POST['email']);
		$firstname = $mysqli->real_escape_string($_POST['firstName']);
	
		$user_active = 0;
		$clean_email=$email;
		$status = true;
		$clean_password="";
		$sql_failure = false;
		$mail_failure = false;
		$email_taken = false;
		$username_taken = false;
		$displayname_taken = false;
		$activation_token = 0;
		$success = NULL;
		$image=NULL;
		$errText="";
		$sText="";

		$result= array("id"=>"0","text"=>$errText.$sText);
		
		//generate auto password
		$clean_password = trim(password_generate(7));

		//save credentials to a backup
		

		//Prevent this function being called if there were construction errors
		if($status==true)
		{
			$secure_pass = generateHash($clean_password);
			$activation_token = generateActivationToken();
			
			if($emailActivation == true)
			{
				$user_active = 0;
				
				$mail = new wcdbMail();
				//Define more if you want to build larger structures
				$hooks = array(
					"searchStrs" => array("#SITE_URL","#ACTIVATION-KEY","#FIRSTNAME#","#USERNAME#","#PASSWORD#"),
					"subjectStrs" => array($websiteUrl,$activation_token,$firstname,$clean_email, $clean_password)
					);
				
				/* Build the template - Optional, you can just use the sendMail function 
				Instead to pass a message. */
				
				if(!$mail->newTemplateMsg("new-registration.html",$hooks))
				{
					$mail_failure = true;
					$errText=$errText."\n Failed to create mail message.";
				}
				else
				{
					//Send the mail. Specify users email here and subject. 
					//SendMail can have a third parementer for message if you do not wish to build a template.
					
						$mail->sendMail($clean_email,"New User");
					
						$success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE2");
						$sText=$sText."\n Email sent successfully ";
						$result["id"]=1;
						//$sText= $sText.savetoDB($memberId,$clean_password);
					
				}
				
			}
			else
			{
				//Instant account activation
				$user_active = 1;
				$success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE1");
			}	
			
			
			if(!$mail_failure)
			{
				//Insert the user into the database providing no errors have been found.
				$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."users (
					memberId,
					password,
					email,
					activation_token,
					last_activation_request,
					lost_password_request, 
					active,
					title,
					sign_up_stamp,
					last_sign_in_stamp
					)
					VALUES (
					?,
					?,
					?,
					?,
					'".time()."',
					'0',
					?,
					'New Member',
					'".time()."',
					'0'
					)");
				
				$stmt->bind_param("ssssi", 
					$memberId,
					$secure_pass, 
					$clean_email, 
					$activation_token, 
					$user_active);
				$stmt->execute();
				$inserted_id = $mysqli->insert_id;
				$stmt->close();
				$sText=$sText." user created";

				//Insert default permission into matches table
				$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."user_permission_matches  (
					user_id,
					permission_id
					)
					VALUES (
					?,
					'1'
					)");
				$stmt->bind_param("s", $inserted_id);
				$stmt->execute();
				$stmt->close();
				$sText=$sText.", permissions updated";
				$result["id"]=1;
				
			}else{
			$errText=$errText." Mail send failure.";
			$result["id"]=0;
			}
			
		}
		
		$result["text"]=$errText.$sText;
			

	
    echo  json_encode($result,JSON_FORCE_OBJECT);
		
/* 	function savetoDB($uid,$pwd){
		try{
			$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."sec_bkp (
				uid,
				pwd
				)");
			$stmt->bind_param("is", 
				$uid,
				$pwd
				);
			$stmt->execute();
			$stmt->close();
			$response=" bkp success";
		}catch(Exception $e){
			$response= $e.getMessage();
		}
		return $response;
	
	} */
    
    ?>