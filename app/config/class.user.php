<?php
/*
wcdb Version: 1.0.0

*/

class user{
	public $userId=0;
	public $userFirst=NULL;
	public $userLast=NULL;
	public  $user_active = 0;
	public $email;
	public  $status = false;
	private $clean_password;
	private $memberId;
	public  $sql_failure = false;
	public  $mail_failure = false;
	public  $email_taken = false;
	public  $username_taken = false;
	public  $displayname_taken = false;
	public  $activation_token = 0;
	public  $success = NULL;
	public  $image=NULL;
	
	
	function sendUserCredentials(){
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;
		$user=fetchUserDetails($this->email);
		$this->userFirst=$user["userFirst"];
		$this->userLast=$user["userLast"];
		$this->user_active=$user["active"];
		$this->userId=$user["id"];


		$errText="";
		$sText="";

		$result= array("id"=>"0","text"=>$errText.$sText);
		
		//generate auto password
		$this->clean_password = trim(password_generate(7));

		
			//Construct a secure hash for the plain text password
			$secure_pass = generateHash($this->clean_password);
			
			//Construct a unique activation token
			$this->activation_token = generateActivationToken();
			
			//Do we need to send out an activation email?
			if($emailActivation == true)
			{
				//User must activate their account first
				$this->user_active = 0;
				
				$mail = new wcdbMail();
				
				//Build the activation message
				//$activation_message = lang("ACCOUNT_ACTIVATION_MESSAGE",array($websiteUrl,$this->activation_token));
				
				//Define more if you want to build larger structures
				$hooks = array(
					"searchStrs" => array("#SITE_URL","#ACTIVATION-KEY","#FIRSTNAME#","#USERNAME#","#PASSWORD#"),
					"subjectStrs" => array($websiteUrl,$this->activation_token,$this->userFirst,$this->email, $this->clean_password)
					);
				
				/* Build the template - Optional, you can just use the sendMail function 
				Instead to pass a message. */
				
				if(!$mail->newTemplateMsg("new-registration.html",$hooks))
				{
					$this->mail_failure = true;
					$errText=$errText." Failed to create mail message.";
				}
				else
				{
					//Send the mail. Specify users email here and subject. 
					//SendMail can have a third parementer for message if you do not wish to build a template.
					
					if(!$mail->sendMail($this->email,"Reset user account"))
					{
						$this->mail_failure = true;
						
					}
				}
				$this->success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE2");
				$sText=$sText." Email sent successfully ";
			}
			else
			{
				//Instant account activation
				$this->user_active = 1;
				$this->success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE1");
			}	
			
			
			if(!$this->mail_failure)
			{
				//Insert the user into the database providing no errors have been found.
				$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users 
					SET
					password=?,
					activation_token=?,
					last_activation_request=?,
					active=?
					WHERE id=?
					");
				
				$stmt->bind_param("sssii", 
					$secure_pass,  
					$this->activation_token, 
					time(),
					$this->user_active,
					$this->userId
				);
				$stmt->execute();
				$inserted_id = $mysqli->insert_id;
				$stmt->close();
				$sText=$sText.", user updated";
				$result= array("id"=>"1","text"=>$errText.$sText);
				return $result;
			}
			$errText=$errText." Mail send failure.";
			$result= array("id"=>"0","text"=>$errText.$sText);
			return $result;
		
			
	}
	}




 function Delete(){
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."users 
		WHERE id=? ");
		$stmt->bind_param('i', 	$this->userId);
		$stmt->execute();
		$stmt->close();
		return 1;

}



class loggedInUser {
	public $email = NULL;
	public $hash_pw = NULL;
	public $user_id = NULL;
	public $memberId=NULL;
	public $image=NULL;
	public $facebook=NULL;
	public $twitter=NULL;
	public $linkedin=NULL;
	public $pinterest=NULL;
	public $lastChatWith=NULL;
	public $lastSignIn=NULL;
	public $lastChat=0;
	public $isAdmin=0;
	public $userFirst=NULL;
	public $userLast=NULL;
	public $recEmails= NULL;
	public $weatherLoc= NULL;
	public $nationId= NULL;
	public $notes=NULL;
	public $dob=NULL;
	public $visits=0;
	public $lang="en";
	public $xpic="user.png";
	public $password=NULL;
	
	//Simple function to update the last sign in of a user
	public function updateLastSignIn()
	{
		global $mysqli,$db_table_prefix;
		$time = time();
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET
			last_sign_in_stamp = ?
			WHERE
			id = ?");
		$stmt->bind_param("ii", $time, $this->user_id);
		$stmt->execute();
		$stmt->close();	
	}
	
	//Simple function to update the avatar of the user
	public function updateImage()
	{
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET
			image = ?
			WHERE
			id = ?");
		$stmt->bind_param("si", $this->image,$this->user_id);
		$stmt->execute();
		$stmt->close();	
	}
	
	//Simple function to increment the number of time user has visited the page
	public function addVisit()
	{
		global $mysqli,$db_table_prefix;
		$q="SELECT visits from ".$db_table_prefix."users where id=".$this->user_id;
		$res=$mysqli->query($q);
		$result=mysqli_fetch_assoc($res);
		$visits=($result['visits'])+1;
		
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET
			visits = ?
			WHERE
			id = ?");
		$stmt->bind_param("si", $visits,$this->user_id);
		$stmt->execute();
		$stmt->close();	
	}
	
	//Return the timestamp when the user registered
	public function signupTimeStamp()
	{
		global $mysqli,$db_table_prefix;
		
		$stmt = $mysqli->prepare("SELECT sign_up_stamp
			FROM ".$db_table_prefix."users
			WHERE id = ?");
		$stmt->bind_param("i", $this->user_id);
		$stmt->execute();
		$stmt->bind_result($timestamp);
		$stmt->fetch();
		$stmt->close();
		return ($timestamp);
	}
	
	//get user name for specified userid
		public function getFriendName($id)
	{
		global $mysqli,$db_table_prefix;
		if($id=='0'){return("Group");}
		else{
		$stmt = $mysqli->prepare("SELECT user_name
			FROM ".$db_table_prefix."users
			WHERE id = ?");
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt->bind_result($username);
		$stmt->fetch();
		$stmt->close();
		
		return ($username);
		}
	}
	
	//Update a users password
	public function updatePassword($pass)
	{
		global $mysqli,$db_table_prefix;
		$secure_pass = generateHash($pass);
		$this->hash_pw = $secure_pass;
		try{
			$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
				SET
				password = ? 
				WHERE
				id = ?");
			$stmt->bind_param("si", $secure_pass, $this->user_id);
			$stmt->execute();
			$stmt->close();	
			$res = array('status' =>1 ,'msg'=>'password successfully changed' );
		}catch(Exception $e){
			$res = array('status' =>0 ,'msg'=>'Error password change failed!'.$e->getMessage() );
		}
	return $res;
	}
	
	

	//Update a users email
	public function updateEmail($email)
	{
		global $mysqli,$db_table_prefix;
		$this->email = $email;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET 
			email = ?
			WHERE
			id = ?");
		$stmt->bind_param("si", $email, $this->user_id);
		$stmt->execute();
		$stmt->close();	
	}
	
	//Is a user has a permission
	public function checkPermission($permission)
	{
		global $mysqli,$db_table_prefix,$master_account;
		
		//Grant access if master user
		
		$stmt = $mysqli->prepare("SELECT id 
			FROM ".$db_table_prefix."user_permission_matches
			WHERE user_id = ?
			AND permission_id = ?
			LIMIT 1
			");
		$access = 0;
		foreach($permission as $check){
			if ($access == 0){
				$stmt->bind_param("ii", $this->user_id, $check);
				$stmt->execute();
				$stmt->store_result();
				if ($stmt->num_rows > 0){
					$access = 1;
				}
			}
		}
		if ($access == 1)
		{
			return true;
		}
		if ($this->user_id == $master_account){
			return true;	
		}
		else
		{
			return false;	
		}
		$stmt->close();
	}
	
	
	//Logout
	public function userLogOut()
	{
		destroySession("wcdbUser");
	}	
}

?>