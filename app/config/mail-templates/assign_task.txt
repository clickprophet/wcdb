<div bgcolor="#f5f5f5">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f5f5f5">
  <tbody><tr>
    <td>
<table align="center">
  <tbody><tr>
    <td>

<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f5f5f5">
  <tbody><tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
      <tbody><tr>
        <td width="70" valign="bottom"><div style="min-height:45px"><a href="http://www.ftstasks.com/quest/" target="_blank"><img src="http://www.ftstasks.com/logo.png" width="120" height="45" border="0" alt="Quest" style="display:block;color:#333333;font-family:Helvetica Neue Light,Helvetica Neue Regular,Helvetica,Arial;font-size:10px" class="CToWUd"></a></div></td>
        <td width="570" valign="middle"><!--<div align="right"><font style="font-size:14px" face="Helvetica Neue Light, Helvetica Neue Regular, Helvetica, Arial" color="#666666">24/7 Support: </font><font style="font-size:16px" face="Helvetica Neue Light, Helvetica Neue Regular, Helvetica, Arial" color="#7db701"><strong><a href="tel:%00263%4%782872" value="+2634782872" target="_blank">+263 4 782872</a></strong></font></div>--></td>
      </tr>
    </tbody></table>
  
      <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
          <td align="left" bgcolor="#7db701">
     <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#7db701">
<tbody><tr>
<td align="left" style="padding:30px" bgcolor="#7db701"><font style="font-size:14px;font-weight:bold" face="Helvetica Neue Light, Helvetica Neue Medium, Helvetica, Arial" color="#ffffff">Dear #USERFIRST# #USERLAST#,
<br/><br/>
</font><font style="font-size:36px" color="#ffffff" face="Helvetica Neue Light, Helvetica Neue Regular, Helvetica, Arial">A new task has been assigned to you.</font>
<br/><br/>
<font style="font-size:25px" color="#ffffff" face="Helvetica Neue Light, Helvetica Neue Regular, Helvetica, Arial">#TASKTITLE#</font></td>
</tr>
      </tbody></table>
   </td>
        </tr>
      <tr>
    <td align="left"><div style="min-height:16px"><img src="https://ci3.googleusercontent.com/proxy/-k_kH3WFjFwKSAx9USSG7ENGRZw1XRS6oWhWegLqbI-1Jur8-A4HRHYhMleA77A1D0P5k17kMNf6ANSmVKF8QLnWxbwDvEfj4xpmpnLfelDkiQto1I2vV6FYgnH76Grih60RCQDkxTKvhg=s0-d-e1-ft#http://imagesak.secureserver.net/promos/htmlemails/template/template_triangle_02.gif" border="0" width="60" height="16" style="display:block" class="CToWUd"></div></td>
  </tr>
        <tr>
           <td align="left" style="padding:20px" width="100%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tbody><tr>
    <td align="left">
  
  <table width="240" border="0" cellspacing="0" bgcolor="#7db701" style="padding-bottom:15px;padding-top:15px;padding-left:5px;padding-right:5px;border-bottom:3px solid #719500">
      <tbody><tr>
        <td><center><a href="http://www.ftstasks.com/quest/index.php?page=opentasks" style="text-decoration:none" target="_blank"><font style="font-size:16px;line-height:16px;text-decoration:none" face="Helvetica Neue Light, Helvetica Neue Medium, Helvetica, Arial" color="#ffffff">View Task</font></a></center></td>
      </tr>
    </tbody></table>
  </td>
               </tr>
             </tbody></table>

<br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
                 <td colspan="2" valign="top" style="padding:10px"><font style="font-size:20px" face="Helvetica Neue Light, Helvetica, Arial" color="#666666">Task Information</font></td>
                 </tr>
               <tr>
                 <td width="21%" valign="top" style="padding:10px"><font style="font-size:14px" face="Helvetica Neue Regular, Helvetica, Arial" color="#a7a7a7">Created By: </font></td>
                 <td width="79%" valign="top" style="padding:10px"><font style="font-size:14px" face="Helvetica Neue Regular, Helvetica, Arial" color="#666666">#AUTHOR#</font></td>
               </tr>
               <tr>
                 <td valign="top" style="padding:10px"><font style="font-size:14px" face="Helvetica Neue Regular, Helvetica, Arial" color="#a7a7a7">Starting On:</font></td>
                 <td valign="top" style="padding:10px"><font style="font-size:14px" face="Helvetica Neue Regular, Helvetica, Arial" color="#666666">#STARTDATE#</font></td>
               </tr>
               <tr>
                 <td valign="top" style="padding:10px"><font style="font-size:14px" face="Helvetica Neue Regular, Helvetica, Arial" color="#a7a7a7">Ending On: </font></td>
                 <td valign="top" style="padding:10px"><font style="font-size:14px" face="Helvetica Neue Regular, Helvetica, Arial" color="#666666">#ENDDATE#</font></td>
               </tr>
               <tr>
                 <td valign="top" style="padding:10px"><font style="font-size:14px" face="Helvetica Neue Regular, Helvetica, Arial" color="#a7a7a7">Details:</font></td>
                 <td valign="top" style="padding:10px"><font style="font-size:14px" face="Helvetica Neue Regular, Helvetica, Arial" color="#666666">#DESCRIPTION#</font></td>
               </tr>
           </tbody></table>
             
<br>

           </td>
        </tr>
      </tbody></table>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody><tr>
        <td align="left"><font color="#999999" style="font-size:10px;font-family:Arial,Helvetica,sans-serif">
<br>
Copyright &copy; 2013-2015 <span class="il">Quest</span> powered by <a href="http://www.antedotetech.com">Antedote</a>, 1 Armagh Avenue, Eastlea, Harare, Zimbabwe. All rights reserved.
<br>
</font></td>
      </tr>
    </tbody></table></td>
  </tr>
</tbody></table>
</td></tr></tbody></table>

</div>