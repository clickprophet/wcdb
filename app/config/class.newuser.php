<?php
/*
wcdb Version: 1.0.0
*/


class newUser 
{
	public  $user_active = 0;
	private $clean_email;
	public  $status = false;
	private $clean_password;
	private $memberId;
	private $firstname;
	public  $sql_failure = false;
	public  $mail_failure = false;
	public  $email_taken = false;
	public  $username_taken = false;
	public  $displayname_taken = false;
	public  $activation_token = 0;
	public  $success = NULL;
	public  $image=NULL;
	
	function __construct($memberId_,$email_,$firstname_)
	{
		//Used for display only
		$this->memberId = $memberId_;
		//Sanitize
		//$this->clean_email = sanitize($email_);
		$this->clean_email = $email_;
		$this->firstname=$firstname_;
	    //$this->username = sanitize($user);
		
		/*if(usernameExists($this->username))
		{
			$this->username_taken = true;
		}
		else if(displayNameExists($this->displayname))
		{
			$this->displayname_taken = true;
		}*/
		if(emailExists($this->clean_email))
		{
			$this->email_taken = true;
			$this->status = false;
		}
		else
		{
			//No problems have been found.
			$this->status = true;
		}
	}


	
	public function AddUser()
	{
		$errText="";
		$sText="";

		$result= array("id"=>"0","text"=>$errText.$sText);
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix,$result;
		
		//generate auto password
		$this->clean_password = trim(password_generate(7));

		//Prevent this function being called if there were construction errors
		if($this->status==true)
		{
			//Construct a secure hash for the plain text password
			$secure_pass = generateHash($this->clean_password);
			
			//Construct a unique activation token
			$this->activation_token = generateActivationToken();
			
			//Do we need to send out an activation email?
			if($emailActivation == true)
			{
				//User must activate their account first
				$this->user_active = 0;
				
				$mail = new wcdbMail();
				
				//Build the activation message
				//$activation_message = lang("ACCOUNT_ACTIVATION_MESSAGE",array($websiteUrl,$this->activation_token));
				
				//Define more if you want to build larger structures
				$hooks = array(
					"searchStrs" => array("#SITE_URL","#ACTIVATION-KEY","#FIRSTNAME#","#USERNAME#","#PASSWORD#"),
					"subjectStrs" => array($websiteUrl,$this->activation_token,$this->firstname,$this->clean_email, $this->clean_password)
					);
				
				/* Build the template - Optional, you can just use the sendMail function 
				Instead to pass a message. */
				
				if(!$mail->newTemplateMsg("new-registration.html",$hooks))
				{
					$this->mail_failure = true;
					$errText=$errText."\n Failed to create mail message.";
				}
				else
				{
					//Send the mail. Specify users email here and subject. 
					//SendMail can have a third parementer for message if you do not wish to build a template.
					
					$result=$mail->sendMail($this->clean_email,"New User");
					if($result!="true")
					{
						$this->mail_failure = true;
						$errText=$errText."\n Failed to send mail message. >> ".$result;
						return $res= array("id"=>"0","text"=>$errText.$sText);
					}else{
						$this->success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE2");
						$sText=$sText."\n Email sent successfully ";
						return $res= array("id"=>"1","text"=>$errText.$sText);
					}
					
				}
				
			}
			else
			{
				//Instant account activation
				$this->user_active = 1;
				$this->success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE1");
			}	
			
			
			if(!$this->mail_failure)
			{
				//Insert the user into the database providing no errors have been found.
				$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."users (
					memberId,
					password,
					email,
					activation_token,
					last_activation_request,
					lost_password_request, 
					active,
					title,
					sign_up_stamp,
					last_sign_in_stamp
					)
					VALUES (
					?,
					?,
					?,
					?,
					'".time()."',
					'0',
					?,
					'New Member',
					'".time()."',
					'0'
					)");
				
				$stmt->bind_param("ssssi", 
					$this->memberId,
					$secure_pass, 
					$this->clean_email, 
					$this->activation_token, 
					$this->user_active);
				$stmt->execute();
				$inserted_id = $mysqli->insert_id;
				$stmt->close();
				$sText=$sText." user created";

				//Insert default permission into matches table
				$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."user_permission_matches  (
					user_id,
					permission_id
					)
					VALUES (
					?,
					'1'
					)");
				$stmt->bind_param("s", $inserted_id);
				$stmt->execute();
				$stmt->close();
				$sText=$sText.", permissions updated";
				$result= array("id"=>"1","text"=>$errText.$sText);
				//$result="true";
				return $result;
			}
			$errText=$errText." Mail send failure.";
			$result= array("id"=>"0","text"=>$errText.$sText);
			//$result="false";
			return $result;
		}
		$errText=$errText." User already exists.";
		$result= array("id"=>"0","text"=>$errText.$sText);
		//$result="false";
		return $result;
			
	}
}

?>