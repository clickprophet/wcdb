<?php
/*
World conquest DB: 1.0.0

*/

require_once("config/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}

//User has confirmed they want their password changed 
if(!empty($_GET["confirm"]))
{
	$token = trim($_GET["confirm"]);
	
	if($token == "" || !validateActivationToken($token,TRUE))
	{
		$errors[] = lang("FORGOTPASS_INVALID_TOKEN");
	}
	else
	{
		$rand_pass = getUniqueCode(15); //Get unique code
		$secure_pass = generateHash($rand_pass); //Generate random hash
		$userdetails = fetchUserDetails(NULL,$token); //Fetchs user details
		$mail = new questMail();		
		
		//Setup our custom hooks
		$hooks = array(
			"searchStrs" => array("#GENERATED-PASS#","#USERNAME#"),
			"subjectStrs" => array($rand_pass,$userdetails["display_name"])
			);
		
		if(!$mail->newTemplateMsg("your-lost-password.txt",$hooks))
		{
			$errors[] = lang("MAIL_TEMPLATE_BUILD_ERROR");
		}
		else
		{	
			if(!$mail->sendMail($userdetails["email"],"Your new password"))
			{
				$errors[] = lang("MAIL_ERROR");
			}
			else
			{
				if(!updatePasswordFromToken($secure_pass,$token))
				{
					$errors[] = lang("SQL_ERROR");
				}
				else
				{	
					if(!flagLostPasswordRequest($userdetails["user_name"],0))
					{
						$errors[] = lang("SQL_ERROR");
					}
					else {
						$successes[]  = lang("FORGOTPASS_NEW_PASS_EMAIL");
					}
				}
			}
		}
	}
}

//User has denied this request
if(!empty($_GET["deny"]))
{
	$token = trim($_GET["deny"]);
	
	if($token == "" || !validateActivationToken($token,TRUE))
	{
		$errors[] = lang("FORGOTPASS_INVALID_TOKEN");
	}
	else
	{
		
		$userdetails = fetchUserDetails(NULL,$token);
		
		if(!flagLostPasswordRequest($userdetails["user_name"],0))
		{
			$errors[] = lang("SQL_ERROR");
		}
		else {
			$successes[] = lang("FORGOTPASS_REQUEST_CANNED");
		}
	}
}

//Forms posted
if(!empty($_POST))
{
	$email = $_POST["email"];
	//$username = sanitize($_POST["username"]);
	
	//Perform some validation
	//Feel free to edit / change as required
	
	if(trim($email) == "")
	{
		$errors[] = lang("ACCOUNT_SPECIFY_EMAIL");
	}
	//Check to ensure email is in the correct format / in the db
	else if(!isValidEmail($email) || !emailExists($email))
	{
		$errors[] = lang("ACCOUNT_INVALID_EMAIL");
	}
	
	/* if(trim($username) == "")
	{
		$errors[] = lang("ACCOUNT_SPECIFY_USERNAME");
	}
	else if(!usernameExists($username))
	{
		$errors[] = lang("ACCOUNT_INVALID_USERNAME");
	} */
	
	if(count($errors) == 0)
	{
		
		//Check that the username / email are associated to the same account
		//if(!emailUsernameLinked($email,$username))
		//{
		//	$errors[] =  lang("ACCOUNT_USER_OR_EMAIL_INVALID");
		//}
		//else
		//{
			//Check if the user has any outstanding lost password requests
			$userdetails = fetchUserDetails($email);
			if($userdetails["lost_password_request"] == 1)
			{
				$errors[] = lang("FORGOTPASS_REQUEST_EXISTS");
			}
			else
			{
				//Email the user asking to confirm this change password request
				//We can use the template builder here
				
				//We use the activation token again for the url key it gets regenerated everytime it's used.
				
				$mail = new questMail();
				$confirm_url = lang("CONFIRM")."\n".$websiteUrl."forgot-password.php?confirm=".$userdetails["activation_token"];
				$deny_url = lang("DENY")."\n".$websiteUrl."forgot-password.php?deny=".$userdetails["activation_token"];
				
				//Setup our custom hooks
				$hooks = array(
					"searchStrs" => array("#CONFIRM-URL#","#DENY-URL#","#USERNAME#"),
					"subjectStrs" => array($confirm_url,$deny_url,$userdetails["user_name"])
					);
				
				if(!$mail->newTemplateMsg("lost-password-request.txt",$hooks))
				{
					$errors[] = lang("MAIL_TEMPLATE_BUILD_ERROR");
				}
				else
				{
					if(!$mail->sendMail($userdetails["email"],"Lost password request"))
					{
						$errors[] = lang("MAIL_ERROR");
					}
					else
					{
						//Update the DB to show this account has an outstanding request
						if(!flagLostPasswordRequest($userdetails["user_name"],1))
						{
							$errors[] = lang("SQL_ERROR");
						}
						else {
							
							$successes[] = lang("FORGOTPASS_REQUEST_SUCCESS");
						}
					}
				}
			}
		//}
	}
}
$addCss="";

?>

<?php


?>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
	
	<title>Home</title>
	<meta name="description" content="This application is to provide information and resources for Barclays employees" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />


 

  <!-- Bootstrap core CSS -->

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="css/custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.3.css" />
  <link href="css/icheck/flat/green.css" rel="stylesheet" />
  <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

  <script src="js/jquery.min.js"></script>
  <script src="js/nprogress.js"></script>
		
     



</head>
<body>

<div class="wrapper wrapperHome">

	<header class="home">

	</header>
	<br/><br/>
		
			
	<div id="wrapper" class="login_box">
    

		<div class="form-header header-primary login_content">
            	<h3><i class="fa fa-repeat"></i>Reset Password Form</h3>
            </div><!-- end .form-header section -->
 			<br/>
			
		<div class="x_content well">
				
			      <?php  echo resultBlock($errors,$successes);  ?>
						
			<p class="medium-text fine-grey" align="center">Please enter your email address below.</p>
			 
			<?php 
			echo"<form class='login' name='login' action='".$_SERVER['PHP_SELF']."' method='post'> ";
			?>
            <!--form action="process-login.php" method="post" class="login"-->
			<div class="form-body">
				<script type="text/javascript">
				if(navigator.userAgent.indexOf('MSIE 8') != -1){
					document.write('<p class="formIntro"><br />Username:</p>');
				}
				
				if(navigator.userAgent.indexOf('MSIE 9') != -1){
					document.write('<p class="formIntro"><br />Username:</p>');
				}
				</script>
				
                <br/>
                
				<div class="row">   
                <div class="col-md-10 col-sm-10 col-xs-10">
                	<label class="field prepend-icon">
						<input type="email" name="email" value="" placeholder="email address" class="form-control" style="width:250px;" />
                		<span for="username" class="form-control-feedback right">
                			<i class="fa fa-user"></i>
                        </span>
                </label>
                </div>
				</div>
			
				<!--button class="submit lbButton" value="Sign In" type="submit"><span class="fa fa-sign-in on-left"></span>Sign In</button-->
                <br/>
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <div class="input-group">
						<a href="resend-activation.php"><button class="btn btn-primary" id="activation" style="">
						                 <i class="fa fa-pencil"></i>
						               Resend Activation</button></a> 
                    	

                    </div>
                </div>
				<div class="col-md-6 col-sm-6 col-xs-12">
				<a href="login.php" class="nextToButton" >Back to Login?</a>
				</div>

			</form>

		</div>
	</div>
	</div>

	

</div>




</body>
</html>


