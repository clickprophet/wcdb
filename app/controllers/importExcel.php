<?php
//---------------------------wcdb version 1.0-------------------------------------------
//------------Function/Procedure to import excel file into DB---------------------------
//-------------------------Tapiwa Jeka 14/07/2016 --------------------------------------

include_once'../config-small.php'; //load configuration settings
session_start();	//start session 
$userId=$_SESSION["wcdbUser"]->user_id; //get the current user's id

	$file = $_FILES['file']['tmp_name'];
	$country=$_POST['wnation'];
	$locality=$_POST['wlocality'];
	$city=$_POST['wcity'];
	$DOB='0000-00-00';
	$handle = fopen($file, "r");
	$c = 0;
	while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
	{
		$firstName = $filesop[0];
		$lastName = $filesop[1];
		$sex=$filesop[2];
		$address=$filesop[3];
		$surburb=$filesop[4];
		$phone=$filesop[5];
		$email=$filesop[6];
		$tags=$filesop[7];
		
		$m=new member;
		$m->firstName=$firstName;
		$m->lastName=$lastName;
		$m->sex=$sex;
		$m->address=$address;
		$m->surburb=$surburb;
		$m->phone=$phone;
		$m->email=$email;
		$m->tags=$tags;
		$m->country=$country;
		$m->city=$city;
		$m->locality=$locality;
		$m->userId=$userId;
		
		if($m->AddTemp()==1){
			$c++;
		}
		
	}
	if($c>0){
		echo "success";
	}else{
		echo "error";
	}
	
	

?>