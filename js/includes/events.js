function eventEdit(id){
          $.getJSON("api/funcs.php?fn=fetchEvent&id="+id, function(res) {
            document.getElementById("eventTitley").value=res.title;
            document.getElementById("eventDescy").value=res.eventDesc
            document.getElementById("eventColory").value=res.color;
            $('#eventColory').trigger('change');
            document.getElementById("reservation").value=res.start+' - '+res.end;
            $('#reservation').daterangepicker({
                "startDate": res.start,
                "endDate": res.end,
                "format": "YYYY-MM-DD"
            }, function(start, end, label) {
              console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
            });
            $('#eventRegiony').val(res.nation);
            $('#eventRegiony').trigger('change');
            $('#evTitle').html('<h2>Edit Event <small>edit event properties</small></h2><div class="clearfix"></div>');
            $('#eventSubmit').html('<button  onclick="editEvent('+id+')" type="submit" class="btn btn-primary" name="submit" value="AddLocality"><i class="icon-check"></i>  Submit</button>');
            //alert(response.name);
        });
}

$('#eventBtn').click(function(){
    changeIcon("eventIcon");
});