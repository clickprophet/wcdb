<?php
require_once("../../app/config-small2.php");
   // Edit upload location here
	global $mysqli,$db_table_prefix;
  

   $result = 0;

   if(isset($_POST['filetype']) && $_POST['filetype']=='medicalform'){
		$destination_path ="../files/members/";
		$new_file_name=basename( $_FILES['medicalform']['name']);
		$id=$_POST['memberId'];
		$fname=$id.'.pdf';
		$target_path = $destination_path.$fname ;
			
		if(@move_uploaded_file($_FILES['medicalform']['tmp_name'], $target_path)) {
			$result = 1;
				
				$stmt = $mysqli->prepare("UPDATE 
														".$db_table_prefix."event_registration
														SET
															medicalForm= ?
														WHERE
														memberId = ?
														LIMIT 1"	 
														);
				
						$stmt->bind_param('ss',
											$fname,
											$id
						);
						$stmt->execute();
						$stmt->close();		
		}
		
		sleep(1);
		echo $result;
   }

   if(isset($_POST['filetype']) && $_POST['filetype']=='passport'){
	   try{
		$destination_path ="../files/passports/";
		$name= $_FILES['passportPage']['name'];
		$id=$_POST['regId'];
		$ext = end((explode(".", $name))); //get file extension only
		$fname=$id.".".$ext;
		$target_path = $destination_path.$fname;
		
		//if (@file_put_contents($target_path, $_FILES['passportPage']['tmp_name'])){
		if(move_uploaded_file($_FILES['passportPage']['tmp_name'], $target_path)) {
			$result = $fname;
				
				$stmt = $mysqli->prepare("UPDATE 
										".$db_table_prefix."event_registration
										SET
											passImg= ?
										WHERE
										id = ?
										LIMIT 1"	 
										);
				
						$stmt->bind_param('ss',
											$fname,
											$id
						);
						$stmt->execute();
						$stmt->close();		
		}
		
		sleep(1);
		echo $result;
	}catch(Exception $e){
		echo $e.getMessage();
	}
   }
   

   if(isset($_POST['filetype']) && $_POST['filetype']=='airticket'){
		$destination_path ="../files/tickets/";
		$name= $_FILES['airTicket']['name'];
		$id=$_POST['regId'];
		$ext = end((explode(".", $name))); //get file extension only
		$fname=$id.".".$ext;
		$target_path = $destination_path.$fname ;
			
		if(@move_uploaded_file($_FILES['airTicket']['tmp_name'], $target_path)) {
			$result = $fname;
				
				$stmt = $mysqli->prepare("UPDATE 
										".$db_table_prefix."event_registration
										SET
											airticket= ?
										WHERE
										id = ?
										LIMIT 1"	 
										);
				
						$stmt->bind_param('ss',
											$fname,
											$id
						);
						$stmt->execute();
						$stmt->close();		
		}
		
		sleep(1);
		echo $result;
	}
if($_FILES['image_file']){
	if(isset($_POST['filetype']) && $_POST['filetype']=='image'){
		$destination_path ="../../images/events/";
		$new_file_name=basename( $_FILES['image_file']['name']);
		$target_path = $destination_path.$new_file_name ;
			
		if(@move_uploaded_file($_FILES['image_file']['tmp_name'], $target_path)) {
			$result = $new_file_name;
	
		}

		echo $result;
	}
}
?>
 

