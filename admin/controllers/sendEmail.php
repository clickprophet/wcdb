<?php

require_once("../app/config-small.php");
//User must activate their account first
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix,$result;
		
		$mail = new wcdbMail();
		
		//Build the activation message
		//$activation_message = lang("ACCOUNT_ACTIVATION_MESSAGE",array($websiteUrl,$this->activation_token));
		
		//Define more if you want to build larger structures
		$hooks = array(
			"searchStrs" => array("#SITE_URL","#TOKEN","#FIRSTNAME#","#USERNAME#","#PASSWORD#","#EVENTTITLE#","#STARTDATE#","#ARRIVAL#","#DEPARTURE#"),
			"subjectStrs" => array($websiteUrl,$this->token,$this->firstname,$this->clean_email, $this->clean_password)
			);
		
		/* Build the template - Optional, you can just use the sendMail function 
		Instead to pass a message. */
		
		if(!$mail->newTemplateMsg("new-registration.html",$hooks))
		{
			$this->mail_failure = true;
			$errText=$errText." Failed to create mail message.";
		}
		else
		{
			//Send the mail. Specify users email here and subject. 
			//SendMail can have a third parementer for message if you do not wish to build a template.
			
			if(!$mail->sendMail($this->clean_email,"New User"))
			{
				$this->mail_failure = true;
				
			}
		}
		$this->success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE2");
		$sText=$sText." Email sent successfully ";


?>