<?php
require_once("../../app/config-small2.php");
   // Edit upload location here
	global $mysqli,$db_table_prefix;
   $destination_path ="../../images/events/";

   $result = 0;

   if($_FILES['image_file2'])
	{
		$new_file_name=basename( $_FILES['image_file2']['name']);
		$error = $_FILES["image_file2"]["error"];

		if($error > 0){
			$response = array(
				"status" => "error",
				"error" => true,
				"message" => "Error uploading the file!"
			);
		}else 
		{
			$id=$_POST['eventId'];
			//$fname=$id.'.jpg';
			$target_path = $destination_path.$new_file_name ;
				
			if(@move_uploaded_file($_FILES['image_file2']['tmp_name'], $target_path)) {
				$result = 1;
					if ($id!=0){
					$stmt = $mysqli->prepare("UPDATE 
													cmfi_events
													SET
														badgeImage= ?
													WHERE
														eventId = ?
													LIMIT 1"	 
													);
					
							$stmt->bind_param('si',
												$new_file_name,
												$id
							);
							$stmt->execute();
							$stmt->close();	
							
							$response = array(
								"status" => "success",
								"error" => false,
								"message" => "File ".$new_file_name." uploaded successfully"
							);
						}
			}
		}
		
	}else{
		$response = array(
			"status" => "error",
			"error" => true,
			"message" => "No file was sent!"
		);
	}

	sleep(1);
	echo json_encode($response);
?>
 

