<?php
// generate json web token
include_once '../../config/core.php';
include_once '../../libs/php-jwt/src/BeforeValidException.php';
include_once '../../libs/php-jwt/src/ExpiredException.php';
include_once '../../libs/php-jwt/src/SignatureInvalidException.php';
include_once '../../libs/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;
// required headers
try{
    require_once("../../app/api/funcs.php");
    require_once("../../app/config/db-settings.php"); //Require DB connection
    require_once("../../app/config/class.user.php");
    require_once("../../app/config/languages/en.php");
    date_default_timezone_set('Africa/Harare');
    //echo "successfully imported dependencies";
    http_response_code(200);

    if (isset($_POST['submit']) && $_POST['submit'] == 'Login') {
        $errors = array();
	    $username = sanitize(trim($_POST["username"]));
	    $password = trim($_POST["password"]);

        //echo json_encode(array("username"=>$username,"password"=>$password));
            //Perform some validation
            //Feel free to edit / change as required
            if($username == "")
            {
                $errors[] = lang("ACCOUNT_SPECIFY_EMAIL");
                echo json_encode(array("message" => $errors,"status"=>"Login Error"));
            }
            if($password == "")
            {
                $errors[] = lang("ACCOUNT_SPECIFY_PASSWORD");
                echo json_encode(array("message" => $errors,"status"=>"Login Error"));
            }

            if(count($errors) == 0)
            {
                //A security note here, never tell the user which credential was incorrect
                if(!emailExists($username))
                {
                    $errors[] = lang("ACCOUNT_EMAIL_OR_PASS_INVALID");
                    echo json_encode(array("message" => $errors,"status"=>"Login Error"));
                }
                else
                {
                    $userdetails = fetchUserDetails($username);
                    //See if the user's account is activated
                    if($userdetails["active"]==0)
                    {
                        $errors[] = lang("ACCOUNT_INACTIVE");
                        echo json_encode(array("message" => $errors,"status"=>"Login Error"));
                    }
                    else
                    {
                        //Hash the password and use the salt from the database to compare the password.
                        $entered_pass = generateHash($password,$userdetails["password"]);
                        
                        if($entered_pass != $userdetails["password"])
                        {
                            //Again, we know the password is at fault here, but lets not give away the combination incase of someone bruteforcing
                            $errors[] = lang("ACCOUNT_PASS_OR_EMAIL_INVALID");
                            echo json_encode(array("message" => $errors,"status"=>"Login Error"));
                        }
                        else
                        {
                            
                            //Passwords match! we're good to go'
                            
                            //Construct a new logged in user object
                            //Transfer some db data to the session object
                            $loggedInUser = new loggedInUser();
                            $loggedInUser->email = $userdetails["email"];
                            $loggedInUser->user_id = $userdetails["id"];
                            $loggedInUser->memberId=$userdetails["memberId"];
                            $loggedInUser->hash_pw = $userdetails["password"];
                            $loggedInUser->title = $userdetails["title"];
                            $loggedInUser->phone = $userdetails["phone"];
                            $loggedInUser->displayname = $userdetails["display_name"];
                            $loggedInUser->username = $userdetails["user_name"];
                            $loggedInUser->userFirst = $userdetails["userFirst"];
                            $loggedInUser->image=$userdetails["image"];
                            $loggedInUser->linkedin=$userdetails["linkedin_profile"];
                            $loggedInUser->facebook=$userdetails["facebook_profile"];
                            $loggedInUser->twitter=$userdetails["twitter_profile"];
                            $loggedInUser->pinterest=$userdetails["pinterest_profile"];
                            $loggedInUser->lastChatWith=0;
                            $loggedInUser->lastSignIn=$userdetails["last_sign_in_stamp"];
                            $loggedInUser->lastChat=0;
                            $loggedInUser->isAdmin= $userdetails['isAdmin'];
                            $loggedInUser->fullName= $userdetails['userFirst'].' '.$userdetails['userLast'];
                            $loggedInUser->recEmails= $userdetails['recEmails'];
                            $loggedInUser->weatherLoc= $userdetails['weatherLoc'];
                            $loggedInUser->notes=$userdetails['userNotes'];
                            $loggedInUser->dob=$userdetails['DOB'];
                            $loggedInUser->nation=$userdetails['nationId'];
                            $loggedInUser->visits=$userdetails['visits'];
                            $loggedInUser->reportTo=$userdetails['reportTo'];
                            
                            //Update last sign in
                            $loggedInUser->updateLastSignIn();
                            $loggedInUser->addVisit();

                            

                            $_SESSION["wcdbUser"] = $loggedInUser;
                            
                            addLog($loggedInUser->user_id,'signin','signin');

                            $token = array(
                                "iss" => $iss,
                                "aud" => $aud,
                                "iat" => $iat,
                                "nbf" => $nbf
                             );
                            
                             // set response code
                            http_response_code(200);
                        
                            // generate jwt
                            $jwt = JWT::encode($token, $key);
                            $data=array();
                            echo json_encode(array(
                                "message" => "Successful login.",
                                "status"=>"success",
                                "data"=>array("user"=> $loggedInUser, "token"=>$jwt)
                            ));
                            
                        }
                    }
                }
            }

    }else{
        // set response code
        http_response_code(401);
    
        // tell the user login failed
        echo json_encode(array("message" => "Login failed.","status"=>"Login Error "));
    }
}
catch(Exception $e){
    echo $e.getMessage();
}


?>