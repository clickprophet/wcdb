<?php
require_once('../../app/config-small2.php');
// Edit upload location here
 global $mysqli,$db_table_prefix;
 


 if(isset($_POST['process']) && $_POST['process']=='commit'){
     try{
        $file = fopen('../css/badge.css','w') or die('Unable to open file!');
        $txt = 'John Doe\n';
        $badgeId=$_POST['badgeId'];
        $design=getBadgeSettings($badgeId);

        //declare defaults
        $image_width=125;
        $p_bg="white";
        $b_bg="#fff";
        $img_top=95;
        $img_left=20;
        $role_bg="#EEA91E";  //#EEA91E
        $role_font_size=10;
        $bc_width=120;
        $bc_height=20;
        $b_width=10.3;
        $b_height=14.8;
        $role_font_size="#000000";
        $display_role="block";
        $bg_img_opacity=1;
        $profilePic_visible='visible';

        //assign variables
        foreach ($design as $item){
           if($item['prop']=='b_width'){$b_width=$item['value'];}
           if($item['prop']=='b_height'){$b_height=$item['value'];}
           if($item['prop']=='img_width'){$image_width=$item['value'];}
           if($item['prop']=='p_bg'){$p_bg=rawurldecode($item['value']);}
           if($item['prop']=='b_bg'){$b_bg=rawurldecode($item['value']);}
           if($item['prop']=='img_y'){$img_top=$item['value'];}
           if($item['prop']=='img_x'){$img_left=$item['value'];}
           if($item['prop']=='bg_img_opacity'){$bg_img_opacity=$item['value'];}
           if($item['prop']=='role_bg'){
            $role_bg=rawurldecode($item['value']);
            $tagpos=strpos($role_bg,'<'); 
            $role_bg=substr($role_bg,0,$tagpos);
            }
           if($item['prop']=='role_font_size'){$role_font_size=$item['value'];}
           if($item['prop']=='bc_width'){$bc_width=$item['value'];}
           if($item['prop']=='bc_height'){$bc_width=$item['value'];}
           if($item['prop']=='display_role'){
             $display_role=$item['value'];
             if ($display_role=="true"){$display_role="block";}else{$display_role="none";}
            }
            if($item['prop']=='profilePic_visible'){
              $profilePic_visible=$item['value'];
              if ($profilePic_visible=="true"){$profilePic_visible="visible";}else{$profilePic_visible="hidden";}
             }
           if($item['prop']=='role_font_color'){$role_font_color=rawurldecode($item['value']);}
        }

        $css="
        /*  ---------- ID CARD -----------------------*/
.id-bg {
  background-color: #d7d6d3;
  font-family: 'verdana';
}

.id-card-holder {
  width: 230px;
  padding: 4px;
  margin: 0 auto;
  background-color: #1f1f1f;
  border-radius: 5px;
  position: relative;
}

.id-card-holder:after {
  content: '';
  width: 7px;
  display: block;
  background-color: #0a0a0a;
  height: 100px;
  position: absolute;
  top: 105px;
  border-radius: 0 5px 5px 0;
}

.id-card-holder:before {
  content: '';
  width: 7px;
  display: block;
  background-color: #0a0a0a;
  height: 100px;
  position: absolute;
  top: 105px;
  left: 219px;
  border-radius: 5px 0 0 5px;
}

.id-card {

  background-color: {$b_bg};
  padding: 0px;
  border-radius: 10px;
  text-align: center;
  box-shadow: 0 0 1.5px 0px #b9b9b9;
}

.id-card img {
  margin: 0 auto;
}

.id-card h2 {
  font-size: 22px;
  font-weight: 400;
}

.header img {
  width: 100%;
  margin-top: 0px;
  min-height: 200px;
  top: 0;
  left: 0;
  background: black;
  opacity: {$bg_img_opacity};
  position: relative;
}

.photo {
  position: absolute;
  top: 95px;
  left: 25px;
}

.photo img {
  width: 80px;
  margin-top: 15px;
}

#idCard h2 {
  font-size: 15px;
  margin: 5px 0;
}

#idCard h3 {
  font-size: 12px;
  margin: 2.5px 0;
  font-weight: 300;
}

.qr-code img {
  width: {$bc_width}px;
}

/* #idCard p {
  font-size: 5px;
  margin: 2px;
} */

.id-card-hook {
  background-color: #000;
  width: 70px;
  margin: 0 auto;
  height: 15px;
  border-radius: 5px 5px 0 0;
}

.id-card-hook:after {
  content: '';
  background-color: #d7d6d3;
  width: 47px;
  height: 6px;
  display: block;
  margin: 0px auto;
  position: relative;
  top: 6px;
  border-radius: 4px;
}

.id-card-tag-strip {
  width: 45px;
  height: 40px;
  background-color: #0950ef;
  margin: 0 auto;
  border-radius: 5px;
  position: relative;
  top: 9px;
  z-index: 1;
  border: 1px solid #0041ad;
}

.id-card-tag-strip:after {
  content: '';
  display: block;
  width: 100%;
  height: 1px;
  background-color: #c1c1c1;
  position: relative;
  top: 10px;
}

.id-card-tag {
  width: 0;
  height: 0;
  border-left: 100px solid transparent;
  border-right: 100px solid transparent;
  border-top: 100px solid #0958db;
  margin: -10px auto -30px auto;
}

.id-card-tag:after {
  content: '';
  display: block;
  width: 0;
  height: 0;
  border-left: 50px solid transparent;
  border-right: 50px solid transparent;
  border-top: 50px solid #d7d6d3;
  margin: -10px auto -30px auto;
  position: relative;
  top: -90px;
  left: -50px;
}

#eventsRegTable tr .links {
  display: none;
}

#eventsRegTable tr:hover .links {
  display: block;
}

.imgContainer {
  position: relative;
  text-align: center;
  color: white;
  width: 100%;
}

/* Centered text */
.centered {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

.card_delegate_role {
  background-color: #00008b;
  color: #fff;
  margin-top: -10px;
  padding: 0px;
}

.card_eventTitle {
  font-size: 12px;
  top: 26px;
  width: 100%;
  font-weight: 700;
  line-height: 17px;
  text-transform: uppercase;
  letter-spacing: 0.5px;
  text-shadow: 1px 1px #333333;
  color: #ffffff;
}

/*styles for printing badges*/
body {
  background: rgb(204, 204, 204);
}



.profilePic {
  visibility:{$profilePic_visible};
  position: absolute;
  cursor: move;
  top: {$img_top}px;
  left: {$img_left}px;
}

.profilePic img {
  visibility:{$profilePic_visible};
  width: {$image_width}px;
  margin-top: 0px;
}

#roleDiv {
  /* font-size: {$role_font_size}px !important; */
  background-color: {$role_bg} !important;
  display: {$display_role};
  width: 100%;
  height: 25px;
}

#role { 

  font-size: {$role_font_size}px !important;
  color:{$role_font_color} !important;
  text-shadow: 1px 1px #333333;
}

page {
  background: {$p_bg};
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
}

page[size='A4'] {
  box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
}

page[size='A4'] {
  width: 21cm;
  height: 29.7cm;
}

page[size='A4'][layout='landscape'] {
  width: 29.7cm;
  height: 21cm;
}

page[size='A3'] {
  width: 29.7cm;
  height: 42cm;
}

page[size='A3'][layout='landscape'] {
  width: 42cm;
  height: 29.7cm;
}

page[size='A5'] {
  width: 14.8cm;
  height: 21cm;
}

page[size='A5'][layout='landscape'] {
  width: 21cm;
  height: 14.8cm;
}

page[size='A6'] {
  /*width: 8.1cm;*/
  /*width: 10.3cm; current*/
  width: {$b_width}cm;
  /*normal A6 is 10.5cm*/
  /*height: 11.5cm;*/
  height: {$b_height}cm;
  /*height: 14.8cm; curret*/
  /*normal A6 is 14.8cm*/
}

page[size='A6'][layout='landscape'] {
  width: 14.8cm;
  height: 10.5cm;
  float: left;
}

page[size='B7'] {
  width: 9.1cm;
  height: 12.5cm;
}


@media print {

  body,
  page {
    margin: 0;
    box-shadow: 0;
  }
}

        
        ";


        //write to css file
        echo fwrite($file,$css);
        fclose($file);
        echo 'Done!';
     }catch(Exception $e){
         echo $e->getMessage();
     }
    
 }


?>