export default {
    data: () => ({
    }),
    template:`
        <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <!--div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div-->
            <div class="nav toggle" style="display: block;">
              <a id="logo" href=".">
                <img src="./images/CMFI-logo.png" alt="CMFI World Conquest Database" width="160">
              </a>
            </div>
            <div id="back" class="nav toggle" style="display:none;">
              <a href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                  <i class="fa fa-chevron-left"></i>
                </a>
            </div>
      
      
            <ul class="nav navbar-nav navbar-right">
        <li>
          
        </li>      
              
       <!--li class="pull-left" style="max-width:300px;padding-top:10px;">
              <div class="form-group top_search">
                
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <form action="" id="searchForm">
                    <div class="input-group"> 
                      <input type="text"  id="autocomplete-custom-append" name="autocomplete-custom-append"class="form-control" placeholder="Select nation here.." class="form-control" style="float: left;width: 350px;" >
                     <span class="input-group-btn" style="position: relative; ">
                            <button class="btn btn-default" type="submit" id="countrySearch" >Go!</button>
                        </span>
                        <li>
                    <div id="loaderImg"><img src="images/loading1.gif" width="30px"></div>
                  </li>
                      </div> 
                      <div id="autocomplete-container" style="position: relative; float: left; width: 400px; margin: 0px;"></div>
                    </form> 
                </div>
              </div>
            </li-->
            <!-- Register button -->
            <li role="presentation" >
            	 <router-link to = "/register" class="circularbtn"><i class="icon-pencil"></i> &nbsp;&nbsp;Register&nbsp;&nbsp;</router-link>
               
              </li>

            <!-- Login button -->
            <li role="presentation" >
            	 <router-link to = "/login" class="circularbtn"><i class="icon-lock"></i> &nbsp;&nbsp;Login&nbsp;&nbsp;</router-link>
               
              </li>

            <!-- twitter -->
            <li role="presentation" class="dropdown" data-toggle="tooltip" data-placement="bottom" title="twitter">
                <a href="." >
                  <i class="icon-social-twitter"></i>
                  
                </a>
              </li>
             <!-- facebook -->
            <li role="presentation" class="dropdown" data-toggle="tooltip" data-placement="bottom" title="facebook">
                <a href="." >
                  <i class="icon-social-facebook"></i>
                  
                </a>
              </li>
        
              <!-- Language Switch -->
              <li role="presentation" class="dropdown" data-toggle="tooltip" data-placement="bottom" title="Change site language">
                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="icon-directions"></i>
                  
                </a>
                <form id="lang" method="get" action=""></form>
                <ul id="menu2" class="dropdown-menu dropdown-usermenu pull-right" role="menu">
                  
                  <li>
                    <a id="en" name="en" value="en" onclick="Lang('en')">
                      <span>English </span>                     
                    </a>
                  </li>    

                  <li>
                    <a id="fr" name="fr" value="fr" onclick="Lang('fr')">
                      <span>French </span> 
                    </a>
                  </li>
              
                </ul>
              
            </li>
            
         
            <!-- Home -->
            <li role="presentation" class="dropdown" data-toggle="tooltip" data-placement="bottom" title="Home">
                <a href="." >
                  <i class="icon-home"></i>
                  
                </a>
              </li>
            </ul>
          </nav>
        </div>

      </div>

    `
}