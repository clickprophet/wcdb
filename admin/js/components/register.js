export default {
    data: () => ({
    }),
    template:`
    	<div class="x_panel">
     <div class="x_title">
          <h2>Delegate Registration Form <small>CMFI World Convention: Koume 2019</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
      <div class="x_content">
      <!-- Registration Wizard -->
              <div id="wizard" class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps">
                      <li>
                        <a href="#step-1">
                          <span class="step_no">1</span>
                          <span class="step_descr">
                              Step 1<br />
                              <small>Personal Information</small>
                          </span>
                        </a>
                      </li>
                      <li>
                        <a href="#step-2">
                          <span class="step_no">2</span>
                          <span class="step_descr">
                              Step 2<br />
                              <small>Transport Information</small>
                          </span>
                        </a>
                      </li>
                      <li>
                        <a href="#step-3">
                          <span class="step_no">3</span>
                          <span class="step_descr">
                              Step 3<br />
                              <small>Health Information</small>
                          </span>
                        </a>
                      </li>
                      <li>
                        <a href="#step-4">
                          <span class="step_no">4</span>
                          <span class="step_descr">
                              Step 4<br />
                              <small>Additional Details</small>
                          </span>
                        </a>
                      </li>
                    </ul>

<form name="regForm" id="regForm" method="post" action="" novalidate>
        <!-- STEP 1 -->
          <div id="step-1">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                  
                  <span class="section"> Personal Details</span>
                    
                          <!-- Default input -->
                          <div class="item form-group col-md-6">
                            <label id="fn" for="firstname">First Name</label>
                            <input type="text" class="form-control" name="firstname" id="firstname" placeholder="First Name"  required="required">
                            <span id="e1"></span>
                          </div>
                          <!-- Default input -->
                          <div class="item form-group col-md-6">
                            <label id="ln" for="surname">Surname</label>
                            <input type="text" class="form-control" name="lastname" placeholder="lastname" required="required">
                          </div>
                    
                   
                          <!-- Default input -->
                          <div class="item form-group col-md-6">
                            <label id="e1" for="email">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Email" name="email"
                            required="required">
                          </div>
                          <!-- Default input -->
                          <div class="item form-group col-md-6">
                            <label id="e2" for="confirm_email">Confirm Email</label>
                            <input type="email" class="form-control" id="email2" name="confirm_email"
                             placeholder="Confirm Your Email" required="required">
                          </div>
                          <div class="item form-group col-md-6">
                            <label id="ph" for="phone">Phone</label>
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number"
                            required="required">
                          </div>
                          <div class="item form-group col-md-6">
                            <label id="pr" for="profession">Profession</label>
                            <input type="text" class="form-control" name="profession" placeholder="Profession"
                            required="required">
                          </div>
                          <div class="item form-group col-md-6">
                            <label id="ct" for="city">City/Town</label>
                            <input type="text" class="form-control" id="city" name="city" 
                             placeholder="City/Town" required="required">
                          </div>
                          <div class="item form-group col-md-6">
                            <label id="cy" for="country">Country</label>
                            <select id='country' name='country' class="form-control country" required="required">
                              <option value="">Select nation</option>
                            </select>
                          </div>
                          <div class="item form-group col-md-6">
                            <label id="nat" for="nationality">Nationality</label>
                            <select id='nationality' name='nationality' class="form-control country" required="required">
                              <option value="">Select nation</option>
                            </select>
                          </div>
                            <div class="item form-group col-md-6">
                              <label for="language">What is your first language</label>
                              <input type="text" name="language" id="autocomplete-custom-append" class="form-control col-md-6" style="float: left;" />
                              <div id="autocomplete-container" style="position: relative; float: left; width: 400px; margin: 10px;"></div>
                          </div>
                          <div class="item form-group col-md-6">
                                <label for="gender">Gender</label>
                             <select id='gender' name='gender' class="form-control" required="required">
                                  <option value="">Select your gender</option>
                                  <option value="M">Male</option>
                                  <option value="F" >Female</option>
                             </select>
                          </div>
                          <div class="item form-group col-md-6">
                            <label id="dob" for="birthday">Date of Birth</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" 
                             placeholder="0000-00-00" required="required">
                          </div>
                          
                    
                    </div>
                  </div>
                </div>
                    



                  <!-- Step 2 of the wizard Transport Info-->

        <div id="step-2">
          <div class="col-sm-2"></div>
          <div class="col-sm-8">
              <span class="section">Transport Information</span>
               <div class="form-row">   
                <div class="item dropdown col-md-6">
                      <label for="meansoftransport">Means Of Transportation</label>
                   <select id="meansoftransport" name="meansoftransport" class="form-control select2_single" required="required">
                        <option value="">Select your means of transport</option>
                        <option  value="Air" >Air</option>
                        <option  value="Road" >Road</option>
                        <option  value="Water" >Water</option>
                   </select>
                </div>
                </div>  
                 <div class="form-row">   
                    <div class="dropdown col-md-6" id="port"></div>
                 </div>
                    <div class="item form-group col-md-6">
                          <label for="pickup">Pick up from Port of Entry</label>
                       <select id="pickupport" name="pickupport" class="form-control" required="required">
                         <option value="">Select </option>
                         <option value="Yes">Yes</option>
                         <option value="No">No</option>                              
                       </select>
                    </div>
                
                 <div class="form-row">
                      <div class="item form-group col-md-6">
                          <label for="arrivaldate">Possible Arrival Date</label>
                          <input type="text" class="form-control" id="arrivaldate" name="arrivaldate" 
                          required="required" placeholder="Your Arrival Date">
                      </div>
                      <!-- Default input -->
                      <div class="item form-group col-md-6">
                        <label for="depaturedate">Possible Depature Date</label>
                        <input type="text" class="form-control" id="depaturedate"
                        name="depaturedate" required="required" placeholder="Your Depature Date">
                      </div>
                 </div><br>
                 <div class="item form-group col-md-6">
                          <label for="children">Are you coming with children (requiring care)?</label>
                       <select id='children' name='children' class="form-control" required="required">
                         <option value="">Select</option>
                            <option value="Yes">Yes</option>
                            <option value="No" >No</option>
                       </select>
                    </div>
                        
                         
                    <div id="childrenDiv"></div>

                  <div class="form-row">   
                      <div class="item form-group col-md-6">
                          <label for="transporToKoume"> Any transportation arrangement to Koume?</label>
                        <select id="transporToKoume" name="transporToKoume" class="form-control"
                         required="required">
                             <option  value="">Select </option>
                             <option  value="Yes">Yes</option>
                             <option  value="No">No</option>
                         </select>
                      </div>
                  </div><br>

                  

                   <div id="transportDiv">
                  
                  
                   </div><br>
          
          </div>
        </div>


                <!-- Step 3 of the wizard Health Isuues -->
          <div id="step-3">
            <div class="col-sm-2">
              </div>
              <div class="col-sm-8">
                    <span class="section">Health Information</span>  
                         <div class="item form-group col-md-12">
                           <label for="dietRestrictions">Do you have any Special Dietary Restrictions?</label>
                           <select id="dietRestrictions" name="dietRestrictions" class="form-control" required="required">
                                   <option value="">Select </option>
                                   <option  value="Yes">Yes</option>
                                   <option  value="No">No</option>

                           </select>
                         </div>
                    <div class="form-row col-md-12"> 
                              <div class="form-group purple-border" id="specifydietRestrictions">

                              </div>
                    </div><br>

                     <div class="span6 ">
                                <div class="ba-htmltext tool ">
                                  <label class="col-md-12" style="font-size: 16px;">Please download&nbsp;
                                    <strong>
                                      <a data-mce-href=
                                        "https://download.ztfministry.org/files/279/MEDICAL-FILES/975/Informations-médicales_avant_CM2.pdf" 
                                        href="https://download.ztfministry.org/files/279/MEDICAL-FILES/975/Informations-médicales_avant_CM2.pdf">
                                        this medical form
                                      </a>
                                    </strong>&nbsp;and fill it out. Once done, upload it using the link below.
                                 </label><br>
                            </div><br>
                   <div class="ba-upload tool ">
                                <label style="font-size: 16px;  color: rgba(51, 51, 51, 1);"
                                 class="col-md-12">
                                    <span >
                                        Upload the filled medical report form (downloaded above):</span>
                                </label><br>
                                <label class="file">
                                    <input  class="col-md-12" type="file" id="medicalform" aria-label="File browser example" required>
                                    <span class="file-custom"></span>
                               </label>

                  </div>
                 </div>
              </div>
        </div>


                <!-- Step 4 of the wizard Health Isuues -->
        <div id="step-4">
            <div class="col-sm-2"></div>
              <div class="col-sm-8">
                <span class="section"> Accommodation & Additional Info</span>
                 <div class="form-row">   
                    <div class="item form-group col-md-12">
                     <label for="accommodation">
                       Have you arranged for a private accommodation
                       in Bertoua (not provided by the convention organizers)?
                     </label>
                     <select id="accommodation" name="accommodation" class="form-control" 
                     required="required">
                          <option value="">Select </option>
                             <option  value="Yes">Yes</option>
                             <option  value="No">No</option>
                         
	                     </select>
	                    </div>
	              </div>
             
                <div id="accommodationDiv"></div>
                
               
                <div class="form-group purple-border col-md-12">
                    <label for="testimony">
                       Testimonies *Optional*
                    </label><br>
                    <textarea class="form-control z-depth-1" rows="4" id="testimony" 
                     placeholder="Tell us your testimony ...">
                      
                    </textarea>
                  </div><br>

                  <div class="ba-upload tool col-md-12">
                      <label style="font-size: 16px; color: rgba(51, 51, 51, 1);">
                          <span>
                            Attach a file to your Testimony
                          </span>
                      </label><br>
                      <label class="file">
                          <input type="file" id="testimony" aria-label="">
                          <span class="file-custom"></span>
                     </label>
                   
                    </div><br>
                    <div class="form-group purple-border col-md-12">
                        <label for="additionalInfo">
                           Additional *Optional*
                        </label><br>
                        <textarea class="form-control z-depth-1" rows="4" id="additionalInfo" 
                          placeholder="Any other thing you may want to know about yourself or the convention?">
                          
                        </textarea>
                     </div>
                     </div>
                    </div>
                    </form>
                     </div>
                     
                  </div>
                </div>
               </div>
             </div>
         </div>
              
      <!--/ Registration Wizard-->

    </div>
    `,
}