// ************************************************************ ++
// **                    WCDB Event Management App           ** ||
// ************************************************************ ++


//base url variable
const api_base_url = '../app/api/api.php/records';
//alerts count
var alerts = 0;
//assign current user variable
var userId = 1;
if (sessionStorage.data) {
    var data = $.parseJSON(sessionStorage.data);
    userId = data.user.user_id;
}

//barcodes array
var barcodes = [];

//define common variables
var today = moment().format('YYYY-MM-DD');
var time = moment().format('hh:mm');
var tomorrow = (moment(today).add(1, 'd')).format('YYYY-MM-DD');

// ********************************** ++
// **  WebSite Multilingual Utils  ** ||
// ********************************** ++

var mylang = 'en';

/** Translating function **/
window.translate = function (lang) {
    if (lang === 'en') {
        mylang = 'en';
        $('[data-localize]').localize("js/lang/language", {
            language: lang
        });
    } else {
        mylang = 'fr';
        $('[data-localize]').localize("js/lang/language", {
            language: lang
        });
    }
};

/** Function Returning current setted language **/
window.getLang = function () {
    return mylang;
};

// Language Object for handling error
window.Lang = {};
window.CMFIConv = {};
CMFIConv.eventDesc = 'La convention mondiale de la Communauté Missionnaire Chrétienne Internationale est prévue du 8 au 22 décembre 2019 avec le double objectif de: (1) Présenter l\'objectif accompli de la phase 2 à notre Seigneur Jésus-Christ avec louange, témoignage et célébration: du 8 au 17 décembre 2019; (2) Planification et lancement de la phase 3 avec les principaux responsables et les missionnaires: du 18 au 22 décembre 2019';
CMFIConv.fees = 'GRATUIT';
CMFIConv.criteria = 'Par Invitation Seulement';
CMFIConv.lieu = 'QG CMFI à Koumé, Bertoua';

// Default error messages are in english
function setEnglish() {
    Lang.entry_port_err = 'Entry port not selected, you need to select the port of entry in order to proceed.';
    Lang.entry_airport_err = 'Entry airport not selected, you need to select the airport of entry in order to proceed.';
    Lang.entry_bcrossing_err = 'Entry border crossing not selected, you need to select the border crossing of entry in order to proceed.';
    Lang.label_cropie = 'Crop & Upload Image';
    Lang.profile_avatar_err = 'Profile avatar not selected, you need to upload profile picture in order to proceed.';
    Lang.form_title = 'Delegate Registration Form';
    Lang.lblNext = 'Next/Suivant';
    Lang.lblPrevious = 'Previous/Précédent';
    Lang.lblfinish = 'Finish/Enregistrer';
}

/** On Langugage country Click **/
$('#lang-fr').click(function () {
    // Translating somme labelle which can't be proceed by jquery-localize
    Lang.entry_port_err = 'Port d\'entrée non sélectionné, vous devez sélectionner le port d\'entrée pour pouvoir continuer.';
    Lang.entry_airport_err = 'Aéroport d\'entrée non sélectionné, vous devez sélectionner l\'aéroport d\'entrée pour pouvoir continuer.';
    Lang.entry_bcrossing_err = 'Frontière d\'entrée non sélectionné, vous devez sélectionner la frontière d\'entrée pour pouvoir continuer.';
    Lang.label_cropie = 'Rogner & Uploader l\'image';
    Lang.profile_avatar_err = 'Photo de profile non sélectionnée, vous devez télécharger une photo de profile pour pouvoir continuer.';
    Lang.form_title = 'Formulaire d\'inscription des délégués';
    Lang.lblNext = 'Suivant';
    Lang.lblPrevious = 'Précédent';
    Lang.lblfinish = 'Enregistrer';

    // Live url change, on language change
    ///window.location.hash = '/register/'+Evenement.id+'/lang?=fr';
    translate('fr');

});

$('#lang-en').click(function () {
    setEnglish();
    // Live url change, on language change
    //window.location.hash = '/register/'+Evenement.id+'/lang?=en';
    translate('en');

});
// **** END Multilangual Utils *******




// ********************************************************* ++
// **                    Application Routing              ** ||
// ********************************************************* ++
routie({
    '/register/:id/:lang': function (id, lang) {
        $('#contentDiv').load('views/register.html').show();
        $('#btnRegister').hide();
        ///window.evenId = id;
        //console.log('lang : '+lang);
        //alert(lang.split('=')[1]);
        // We can translate with the current choosen language
        translate(lang.split('=')[1]);

    },
    '/login': function () { //show landing page
        if(isAuth()){
            $('#contentDiv').load('views/events_admin.html').show();
        }else{
            $('#contentDiv').load('views/login.html').show();
        }
    },
    '/': function () { //show landing page
        $('#contentDiv').load('views/landing.html').show();
    },
    '/event/:eventId': function (eventId) { //show landing page
        $('#contentDiv').load('views/eventProfile.html').show();
        console.log(eventId);
    },
    '/admin': function () { //show landing page
        if (isAuth()) {
            $('#contentDiv').load('views/admin.html').show();
        } else {
            routie('/login')
            sessionStorage.setItem("reqURL", window.location.href);
        }
    },
    '/events': function () { //show landing page
        if (isAuth()) {
            //$('#body').load('admin/index.html').show();
            $('#contentDiv').load('views/events_admin.html').show();
            /*  setTimeout(function () {
                 $('#admin_content').load('views/events_admin.html').show();
             }, 1000 );*/

        } else {
            routie('/login');
            //errorNote("Insufficient privileges to access page", "You do not have permission to access this page; please login as admin first!");
            $('#contentDiv').load('views/landing.html').show();
            sessionStorage.setItem("reqURL", window.location.href);
            logout();
        }
    },
    '/admin/event/:eventId': function (eventId) { //show landing page
        //console.log('1');
        if (isAuth()) {
            $('#contentDiv').load('views/admin_dashboard.html').show();
            /*  console.log('2');
             setTimeout(function () {
                 console.log('3');
                 $('#admin_content').load('views/admin_dashboard.html').show();
             }, 1000); */
        } else {
            routie('/login');
            //errorNote("Insufficient privileges to access page", "You do not have permission to access this page; please login as admin first!");
            $('#contentDiv').load('views/landing.html').show();
            sessionStorage.setItem("reqURL", window.location.href);
            logout();
        }
    },
    '/registration/:token': function () { //show landing page
        $('#contentDiv').load('views/registrationProfile.html').show();
    },
    '/test/:eventId': function () { //show landing page
        $('#contentDiv').load('views/badges.html').show();
    }
});


function showPage(page, id) {
    $.get(api_base_url + '/cmfi_pages?filter=sname,eq,' + page, function (data) {
        var res = data.records[0];
        var v = Math.random().toString(36).substring(2, 15);
        $('#contentDiv').load(`views/${res.page}?ver=${v}`).show();
    })

}

var sresponse = "Success";
var fresponse = "Something's wrong";
var xp = 3600;
var tkx = new Date();
tkx.setSeconds(new Date().getSeconds() + parseInt(this.xp))

function ConfirmDelete() {
    var x = confirm("Are you sure you want to delete?");
    if (x)
        return true;
    else
        return false;
}

//get the id from window url
function getId(x) {
    var str = window.location.href;
    var idArr = str.split("/");
    var l = idArr.length;

    console.log("str: " + str);
    console.log("idArr: " + idArr);
    console.log("idArr length: " + l);
    console.log("X: " + x);
    console.log("EVT ID: " + idArr[l - x]);



    return idArr[l - x];

    ///register/:id/:lang

    //http://localhost/cmfi_events/#/register/53/lang?=fr
}

//function to add days to a date
Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + parseInt(days));
    return this;
}

//get filename from image src
String.prototype.filename = function (extension) {
    var s = this.replace(/\\/g, '/');
    s = s.substring(s.lastIndexOf('/') + 1);
    return extension ? s.replace(/[?#].+$/, '') : s.split('.')[0];
}

function monthName(monthNumber) {
    var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return month[monthNumber];
}

function getImage(img) {
    var image = 'event-default.jpg';
    if (img != null) {
        image = img;
    }
    return image;
}

function show(div) {
    var icon = div + "_icon";
    var x = document.getElementById(div);
    if (x.style.display == 'none') {
        x.style.display = 'block';
        changeIcon(icon);
    }
}

function hide(div) {
    var x = document.getElementById(div);
    x.style.display = 'none';
}

function showHide(div) {
    var x = document.getElementById(div);
    if (x.style.display == 'block') {
        x.style.display = 'none';
    } else {
        x.style.display = 'block';
    }
}

function showHideForm(div) {
    var icon = div + "_icon";
    changeIcon(icon);
    var x = document.getElementById(div);
    if (x.style.display == 'block') {
        x.style.display = 'none';
    } else {
        x.style.display = 'block';
    }
}

function changeIcon(icon) {
    var x = document.getElementById(icon);
    if (x) {
        if (x.classList.contains("glyphicon-plus-sign") == true) {
            x.classList.remove("glyphicon-plus-sign");
            x.classList.remove("green");
            x.classList.add("glyphicon-remove-sign");
            x.classList.add("red");
        } else {
            x.classList.remove("glyphicon-remove-sign");
            x.classList.remove("red");
            x.classList.add("glyphicon-plus-sign");
            x.classList.add("green");

        }
    }
}

function formatDate(oldDate) {
    var modifiedDateValue = oldDate.split("-"); // Gives Output as 2016,01,06 00:43:06
    var date_ = new Date(modifiedDateValue[0] + "/" + modifiedDateValue[1] + "/" + modifiedDateValue[2]);
    return date_;
}

function getImageName(img_input) {
    var fullPath = document.getElementById(img_input).src;
    //var filename = fullPath.replace(/^.*[\\\/]/, '');
    // or, try this,
    var filename = fullPath.split("/").pop();
    return filename;
}

// New Methode to load countries
function fillCountryCombo() {
    $.ajax({
        type: 'post',
        url: '../app/dao/services.php',
        data: {
            'cFetch': ""
        },
        dataType: 'json',
        success: function (response) {
            $.each(response, function (index, value) {
                $(".countries").append('<option value="' + value.code + '">' + value.name + '</option>');
            });
        }
    });
}

// Get array of dates that fall between two dates
function getDatesBetween(start, end) {
    var between = [];
    while (start <= end) {
        between.push(new Date(start));
        start.setDate(start.getDate() + 1);
    }

    return between;
}


function initialiseCountryCombo() {
    // $('.countries').html(`<option value=0>Choose country</option>`);
    var opt1 = $(".countries");
    $.getJSON(api_base_url + "/cmfi_countries?order=name,asc", function (response) {
        $.each(response.records, function () {
            opt1.append($("<option />").val(this.code).text(this.name));
        });
    });
}

function initialiseRoleCombo() {
    // $('.countries').html(`<option value=0>Choose country</option>`);
    var opt1 = $(".roles");
    $.getJSON(api_base_url + "/cmfi_roles?order=role,asc", function (response) {
        $.each(response.records, function () {
            opt1.append($("<option />").val(this.roleId).text(this.role));
        });
    });
}

function initialiseSessionsCombo() {
    // $('.countries').html(`<option value=0>Choose country</option>`);
    var opt1 = $(".sessions");
    $.getJSON(api_base_url + "/cmfi_event_sessions?order=name,asc", function (response) {
        $.each(response.records, function () {
            opt1.append($("<option />").val(this.id).text(this.name));
        });
    });
}


function AppLogin() {
    var d = new FormData();
    var user = document.getElementById("username").value;
    var pass = document.getElementById("password").value;
    /*console.log("user:" + username + " password:" + password);*/
    d.append("submit", "Login");
    d.append("username", user);
    d.append("password", pass);


    $.ajax({
        url: "controllers/login.php",
        type: "POST",
        data: d,
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {

            console.log(response)
            var res = $.parseJSON(response);
            //console.log("data:", response);

            if (res.status == 'success') {
                sessionStorage.setItem('data', JSON.stringify(res.data)); //set user in session memory
                sessionStorage.setItem('expires', tkx);
                //login user
                showLoggedInMenu();
                routie('/events');
            } else {
                errorNote(res.status, res.message);
            }
        },
        error: function (data) {
            console.log("error: ", data.responseText);
            errorNote(fresponse, "Login error: " + data.responseText);
        }
    });

}




function logout() {
    sessionStorage.removeItem("data");
    console.log("user data cleared")
    sessionStorage.clear();
    localStorage.clear();
    showLoggedOutMenu();


}

function showLoggedOutMenu() {
    $("#user").html(`<a onclick="notifyLoginNeed();" class="user-profile  aria-expanded="false">
  <img src="images/user.png" alt="">User Login
  <!--span class=" fa fa-angle-down"></span-->
</a>
`);
    $("#alert_bell").css("display", "none");
    $(".profile").html('');
    //hide main menu in side bar
    $("#main_admin_menu").css("display", "none");
}

function showLoggedInMenu() {
    //show alerts
    $("#alert_bell").css("display", "block");
    //show main menu in side bar
    $("#main_admin_menu").css("display", "block");

    //show user menu
    var data = $.parseJSON(sessionStorage.getItem("data"));
    var x = "";
    //console.log("data:",data);
    var user = data.user;
    console.log("user :", user);
    if (user.isAdmin == 1) {
        x = `<li><a onclick="routie('/admin/main')"><i class="icon-wrench pull-right"></i> Admin</a></li>`;
    }
    $("#user").html(`<a id="loggedInUser" href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <img src="../app/images/members/${user.image}" alt=""><span class="username">${user.fullName}</span>
                <span class=" fa fa-angle-down"></span>
              </a>
              <ul class="dropdown-menu dropdown-usermenu pull-right">
                <li><a onclick="routie('/user/profile')"><i class="icon-user pull-right"></i>  Profile</a>
                </li>
                <li><a href="https://wcdb.cmfionline.org/events/../app/" target="_blank"><i class="icon-globe pull-right"></i>  Ministry Database (wcdb)</a>
                </li>
                ${x}
                <li><a onclick="logout(); window.location.assign('.');"><i class="icon-logout pull-right"></i> Log Out</a>
                </li>
              </ul>`);


    $(".profile").html(`<div class="profile_pic">
                    <img src="../app/images/members/${user.image}" alt="..." class="img-circle profile_img">
                </div>
                <div class="profile_info">
                    <span>Welcome,</span>
                    <h2>${user.fullName}</h2>
                </div>`);
}

function isAuth() {
    if (sessionStorage.data) {
        //check if token is valid
        if (validtoken() == false) {
            return false;
        } else {
            return true;
        }

    } else {

        return false
    }

}

function validtoken() {
    if (sessionStorage.expires) {
        var t = new Date();
        //console.log(t);
        if (moment(sessionStorage.expires) <= moment(t)) {
            //destroy session and go to login
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}


//build side menu

function buildMenu() {
    $.get(`${api_base_url}/cmfi_menu`, function (data) {
        var res = data.records;
        res.forEach(function (item) {
            if (item.parent == 0) {
                //parent items - level 0 menu - form parents with their ids


            } else {
                //child items assign child to parent id

            }
        })
    })
}




//************************************** */
//         Edit badge properties
/*************************************** */
//Edit settings
function editSet(id, func) {
    var rowId = "row" + id;
    var currentTD = $("#editset" + id).parents('tr').find('td');
    var x = $("#editset" + id).html();
    //alert (x);
    if ($("#editsetIcon" + id).hasClass('glyphicon-pencil')) {
        $.each(currentTD, function () {
            $(this).prop('contenteditable', true),
                $(this).prop('style', 'color:red')
        });
        $("#editset" + id).html(`<i id="editsetIcon${id}" class="glyphicon glyphicon-ok"></i> Save`);

    } else {
        $.each(currentTD, function () {
            $(this).prop('contenteditable', false),
                $(this).prop('style', 'color:black')

        });
        var id_ = id;
        var name_ = $("#name" + id).html();
        var value_ = $("#value" + id).html();
        if (func == "settings") {
            saveSetting(id_, name_, value_);
        }
        if (func == "badgeSet") {
            saveBadgeSetting(id_, name_, value_);
        }
        if (func == "roles") {
            var value_ = $("#role" + id).html();
            saveRole(id_, value_);
        }
        if (func == "pages") {
            var parent_ = $("#parent" + id).html();
            var menu_ = $("#menu" + id).html();
            var sname_ = $("#sname" + id).html();
            var page_ = $("#page" + id).html();
            var icon_ = $("#icon" + id).html();
            var data = {
                id: id,
                menu: menu_,
                page: page_,
                sname: sname_,
                active: 1,
                parent: parent_,
                icon: icon_
            }
            savePage(data, id);
        }

        $("#editset" + id).html(`<i id="editsetIcon${id}" class="glyphicon glyphicon-pencil"></i>`);


    }
}

//add new badge
function newBadge(){
    var name_=$('#badgeName').val();
    var desc_=$('#badgeDesc').val();
    $.ajax({
        url: api_base_url + "/cmfi_badges/" ,
        type: "POST",
        data: {
            name: name_,
            description: desc_,
            isDefault:0
        },
        success: function (response, status) { // Required Callback Function

            successNote(sresponse, 'New badge <strong>' + name_+ '</strong> created sussessfully!');
            $('#badge_datatable').DataTable().ajax.reload();
            $('#badgeForm')[0].reset();
        },
        error: function (err) {
            errorNote(fresponse, 'Badge  not created! ' + err.responseText)
        }
    });

}
//add new badge setting
function newBadgeSetting(){
    var prop_=$('#propName').val();
    var value_=$('#propValue').val();
    var badgeId_=sessionStorage.getItem("badgeId");
    $.ajax({
        url: api_base_url + "/cmfi_badge_design/" ,
        type: "POST",
        data: {
            badgeId: badgeId_,
            prop: prop_,
            value: value_
        },
        success: function (response, status) { // Required Callback Function

            successNote(sresponse, 'New setting <strong>' + prop_+ '</strong> created sussessfully!');
            $('#badge_set_datatable').DataTable().ajax.reload();
            $('#badgeSettingsForm')[0].reset();
            if($('#btnCommit').hasClass('btn-success')){
                $('#btnCommit').removeClass('btn-success');
                $('#btnCommit').addClass('btn-danger');
                
            }
        },
        error: function (err) {
            errorNote(fresponse, 'Setting  not created! ' + err.responseText)
        }
    });

}

//************************************* */
//          Page functions
//************************************* */
//save page
function savePage(data_, id) {
    //console.log(data_)
    $.ajax({
        url: api_base_url + "/cmfi_pages/" + id,
        type: "PUT",
        data: data_,
        success: function (response, status) { // Required Callback Function

            successNote(sresponse, 'Page <strong>' + data_.sname + '</strong> changed sussessfully!');

        },
        error: function (err) {
            errorNote(fresponse, 'Page data not saved! ' + err)
        }
    });

}

//add new page
function addPage() {
    $.ajax({
        url: api_base_url + "/cmfi_pages/",
        type: "POST",
        data: {
            page: pagesForm.page.value,
            sname: pagesForm.sname.value,
            menu: pagesForm.menu.value,
            parent: pagesForm.parent.value,
            icon: pagesForm.icon.value,
        },
        success: function (response, status) { // Required Callback Function
            successNote(sresponse, 'New Page  <strong>' + pagesForm.sname.value + '</strong> created sussessfully!')
            $('#pages_datatable').DataTable().ajax.reload();
            $('#pagesForm')[0].reset();
        },
        error: function (err) {
            errorNote(fresponse, 'Page data not saved! ' + err.responseText)
        }
    });
}

// delete page
function deletePage(id) {
    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Are you sure you want to delete this page?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {
            'dir1': 'down',
            'dir2': 'right',
            'modal': true
        }
    })).get().on('pnotify.confirm', function () {
        $.ajax({
            url: api_base_url + '/cmfi_pages/' + id,
            type: 'DELETE',
            success: function (response, status) {
                successNote(sresponse, 'Page deleted sussessfully!');
                $('#pages_datatable').DataTable().ajax.reload(); //refresh registrations table
            },
            error: function (err) {
                errorNote(fresponse, 'Delete not successful!' + err.responseJSON);
            }
        });
    });
}


//************************************** */
//         Assets functions
/*************************************** */
//add new Asset
function addAsset() {
    var steward_ = $("#membersDropDown").val();
    $.ajax({
        url: api_base_url + "/cmfi_assets/",
        type: "POST",
        data: {
            name: assetsForm.name.value,
            description: assetsForm.description.value,
            value: assetsForm.value.value,
            date: assetsForm.purchaseDate.value,
            department: assetsForm.department.value,
            serialNumber: assetsForm.serialNumber.value,
            steward: steward_,
            code: assetsForm.code.value,
        },
        success: function (response, status) { // Required Callback Function
            successNote(sresponse, 'New Asset  <strong>' + pagesForm.name.value + '</strong> created sussessfully!')
            $('#assets_datatable').DataTable().ajax.reload();
            $('#assetsForm')[0].reset();
        },
        error: function (err) {
            errorNote(fresponse, 'Asset data not saved! ' + err.responseText)
        }
    });
}

// delete asset
function deleteAsset(id) {
    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Are you sure you want to delete this asset?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {
            'dir1': 'down',
            'dir2': 'right',
            'modal': true
        }
    })).get().on('pnotify.confirm', function () {
        $.ajax({
            url: api_base_url + '/cmfi_assets/' + id,
            type: 'DELETE',
            success: function (response, status) {
                successNote(sresponse, 'Asset deleted sussessfully!');
                $('#assets_datatable').DataTable().ajax.reload(); //refresh registrations table
            },
            error: function (err) {
                errorNote(fresponse, 'Delete not successful!' + err.responseJSON);
            }
        });
    });
}

//************************************** */
//         Role functions
/*************************************** */

//save role
function saveRole(id_, value_) {
    $.ajax({
        url: api_base_url + "/cmfi_roles/" + id_,
        type: "PUT",
        data: {
            role: value_
        },
        success: function (response, status) { // Required Callback Function

            successNote(sresponse, 'Role <strong>' + value_ + '</strong> changed sussessfully!');

        },
        error: function (err) {
            errorNote(fresponse, 'Role data not saved! ' + err)
        }
    });

}

//add new role
function addRole() {
    var value_ = rolesForm.role.value;
    $.ajax({
        url: api_base_url + "/cmfi_roles/",
        type: "POST",
        data: {
            role: value_
        },
        success: function (response, status) { // Required Callback Function
            successNote(sresponse, 'New Role  <strong>' + value_ + '</strong> created sussessfully!')
            $('#roles_datatable').DataTable().ajax.reload();
            $('#rolesForm')[0].reset();
        },
        error: function (err) {
            errorNote(fresponse, 'Role data not saved! ' + err)
        }
    });
}

// delete role
function deleteRole(id) {
    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Are you sure you want to delete this role?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {
            'dir1': 'down',
            'dir2': 'right',
            'modal': true
        }
    })).get().on('pnotify.confirm', function () {
        $.ajax({
            url: api_base_url + '/cmfi_roles/' + id,
            type: 'DELETE',
            success: function (response, status) {
                successNote(sresponse, 'Role deleted sussessfully!');
                $('#roles_datatable').DataTable().ajax.reload(); //refresh registrations table
            },
            error: function (err) {
                errorNote(fresponse, 'Delete not successful!' + err.responseJSON);
            }
        });
    });
}

//************************************** */
//          template function
//************************************* */


//************************************** */
//         Settings functions
/*************************************** */

//save setting data to file	
function saveBadgeSetting(id_, name_, value_) {
    var val=escape(value_);
    $.ajax({
        url: api_base_url + "/cmfi_badge_design/" + id_,
        type: "PUT",
        data: {
            prop: name_,
            value: val
        },
        success: function (response, status) { // Required Callback Function
            console.log(response)
            successNote(sresponse, 'Badge setting <strong>' + name_ + '</strong> changed sussessfully!')
            //change color of commit btn after setting update
            if($('#btnCommit').hasClass('btn-success')){
                $('#btnCommit').removeClass('btn-success');
                $('#btnCommit').addClass('btn-danger');
                
            }
            
        },
        error: function (err) {
            errorNote(fresponse, 'Setting data not saved! ' + err)
        }
    });
}

function addBadgeSetting() {
    var name_ = badgeSettingsForm.name.value;
    var value_ = badgeSettingsForm.value.value;
    $.ajax({
        url: api_base_url + "/cmfi_badge_design/",
        type: "POST",
        data: {
            prop: name_,
            value: value_
        },
        success: function (response, status) { // Required Callback Function
            successNote(sresponse, 'Badge setting <strong>' + name_ + '</strong> created sussessfully!')
            $('#badge_set_datatable').DataTable().ajax.reload();
            $('#badgeSettingsForm')[0].reset();
        },
        error: function (err) {
            errorNote(fresponse, 'Setting data not saved! ' + err)
        }
    });
}

function deleteBadgeSetting(id) {
    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Are you sure you want to delete this setting?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {
            'dir1': 'down',
            'dir2': 'right',
            'modal': true
        }
    })).get().on('pnotify.confirm', function () {
        $.ajax({
            url: api_base_url + '/cmfi_badge_design/' + id,
            type: 'DELETE',
            success: function (response, status) {
                successNote(sresponse, 'Setting deleted sussessfully!');
                $('#badge_set_datatable').DataTable().ajax.reload(); //refresh registrations table
            },
            error: function (err) {
                errorNote(fresponse, 'Delete not successful!' + err.responseJSON);
            }
        });
    });
}

//print badge settings
function commitBadgeCSS() {
    var badgeId=sessionStorage.getItem("badgeId");
    console.log('badgeIs on commit:',badgeId)
    $.ajax({
        url: `controllers/badge_css_update.php`,
        type: 'POST',
        data: {
            process: 'commit',
            badgeId: badgeId
        },
        success: function (res) {
            console.log(res);
            //showPage('badge_settings');
            showBadgeTemplate(badgeId);
            if($('#btnCommit').hasClass('btn-danger')){
                $('#btnCommit').removeClass('btn-danger');
                $('#btnCommit').addClass('btn-success');
                
            }
        },
        error: function (err) {
            console.log(err)
        }
    })
}
//alert user
//this function alerts user of some live information
function alertArrivalsToday() {

    //get arrivals
    $.get(`${api_base_url}/cmfi_event_registration?filter=arrivalDate,eq,${today}`, function (res) {
        //console.log(res);
        var count = res.records.length;
        var r = a = d = l = y = 0;
        res.records.forEach(function (item) {
            if (item.entryPort === 'Douala') {
                d++;
            }
            if (item.entryPort === 'Yaounde') {
                y++;
            }
            if (item.transportMode === 'Road') {
                r++;
            }
            if (item.transportMode === 'Air') {
                a++;
            }
            if (item.transportMode === 'Local') {
                l++;
            }
        })
        var stmt = '';
        if (a > 0) {
            stmt = stmt + `${a} by air `
        }
        if (d > 0) {
            stmt = stmt + `${d} at Douala airport `
        }
        if (y > 0) {
            stmt = stmt + ` ${y} at Yaounde airport`;
        }
        if (r > 0) {
            stmt = stmt + ` ${r} by road `
        }
        var html = ` <li  onclick="showPage('pg_arr_today')">
        <a>
          <span class="image"><img src="images/arr.png" alt="Profile Image"></span>
          <span>
            <span>System Worker</span>
            <span class="time">${time}</span>
          </span>
          <span class="message">
            ${count} delegates are arriving today. ${stmt}
          </span>
        </a>
      </li>`;
        if (count) {
            $('#system_alerts').append(html);
            alerts++;
            $('#alert_count').html(alerts);
        }
    });
    //get departures
    $.get(`${api_base_url}/cmfi_event_registration?filter=departureDate,eq,${today}`, function (res) {
        //console.log(res);
        var count = res.records.length;
        var html = ` <li  onclick="showPage('pg_arr_today')">
        <a>
          <span class="image"><img src="images/arr.png" alt="Profile Image"></span>
          <span>
            <span>System Worker</span>
            <span class="time">${time}</span>
          </span>
          <span class="message">
            ${count} delegates are departing today
          </span>
        </a>
      </li>`;
        if (count) {
            $('#system_alerts').append(html);
            alerts++;
            $('#alert_count').html(alerts);
        }
    });
}

function alertArrivalsTomorrow() {

    //get arrivals
    $.get(`${api_base_url}/cmfi_event_registration?filter=arrivalDate,eq,${tomorrow}`, function (res) {
        //console.log(res);
        var count = res.records.length;
        var r = a = d = l = y = 0;
        res.records.forEach(function (item) {
            if (item.entryPort === 'Douala') {
                d++;
            }
            if (item.entryPort === 'Yaounde') {
                y++;
            }
            if (item.transportMode === 'Road') {
                r++;
            }
            if (item.transportMode === 'Air') {
                a++;
            }
            if (item.transportMode === 'Local') {
                l++;
            }
        })
        var stmt = '';
        if (a > 0) {
            stmt = stmt + `${a} by air `
        }
        if (d > 0) {
            stmt = stmt + `${d} at Douala airport `
        }
        if (y > 0) {
            stmt = stmt + ` ${y} at Yaounde airport`;
        }
        if (r > 0) {
            stmt = stmt + ` ${r} by road `
        }

        var html = ` <li  onclick="showPage('pg_arr_today')">
        <a>
          <span class="image"><img src="images/arr.png" alt="Profile Image"></span>
          <span>
            <span>System Worker</span>
            <span class="time">${time}</span>
          </span>
          <span class="message">
            ${count} delegates are arriving tomorrow. ${stmt}
          </span>
        </a>
      </li>`;
        if (count) {
            $('#system_alerts').append(html);
            alerts++;
            $('#alert_count').html(alerts);
        }
    });
    //get departures
    $.get(`${api_base_url}/cmfi_event_registration?filter=departureDate,eq,${tomorrow}`, function (res) {
        //console.log(res);
        var count = res.records.length;
        var html = ` <li  onclick="showPage('pg_arr_today')">
        <a>
          <span class="image"><img src="images/arr.png" alt="Profile Image"></span>
          <span>
            <span>System Worker</span>
            <span class="time">${time}</span>
          </span>
          <span class="message">
            ${count} delegates are departing tomorrow
          </span>
        </a>
      </li>`;
        if (count) {
            $('#system_alerts').append(html);
            alerts++;
            $('#alert_count').html(alerts);
        }
    });
}




//save airticket image file
function uploadAirticketFile() {
    var imageForm = document.getElementById('regForm1');
    var file_data = $('#airTicket').prop('files')[0];
    var filepath = imageForm.airTicket.value;
    var imageFile = filepath.split(/[\\\/]/).pop();
    var data_ = new FormData();
    data_.append('airTicket', file_data);
    data_.append('regId', sessionStorage.getItem("regId"));
    data_.append('filetype', 'airticket');

    if (imageFile == '') {
        alert("Please select valid air ticket or flight booking file first");
    } else {
        $.ajax({
            url: "controllers/fileUpload.php", // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: data_, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false
            success: function (data) // A function to be called if request succeeds
            {
                console.log('air ticket uploaded sussessfully! ', data);

                $('#ticketViewer').html(`<iframe src = "ViewerJS/#../files/tickets/${data}" width='500' height='350' frameBorder="0" allowfullscreen webkitallowfullscreen></iframe>`);
            },
            error: function (err) {
                console.log('airticket not uploaded successfully! :', err);
            }
        });
    }
}
//save passport image file
function uploadPassportFile() {
    var imageForm = document.getElementById('regForm1');
    var file_data = $('#passportPage').prop('files')[0];
    var filepath = imageForm.passportPage.value;
    var imageFile = filepath.split(/[\\\/]/).pop();
    var data_ = new FormData();
    data_.append('passportPage', file_data);
    data_.append('regId', sessionStorage.getItem("regId"));
    data_.append('filetype', 'passport');

    if (imageFile == '') {
        alert("Please select valid passport file first");
    } else {
        $.ajax({
            url: "controllers/fileUpload.php", // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: data_, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false
            success: function (data) // A function to be called if request succeeds
            {

                console.log('passport uploaded sussessfully! ', data);
                $('#passportViewer').html(`<iframe src = "ViewerJS/#../files/passports/${data}" width='500' height='350' frameBorder="0" allowfullscreen webkitallowfullscreen></iframe>`);
            },
            error: function (err) {
                console.log('passport not uploaded successfully! :', err);
            }
        });
    }
}

//save member image file
function saveImageFromFile(form) {
    var imageForm = document.getElementById(form);
    var filepath = imageForm.image_file.value;
    var imageFile = filepath.split(/[\\\/]/).pop();
    var data_ = new FormData(imageForm);
    // data_.append('memberId',imageForm.id.value);
    sessionStorage.setItem("delegateImage", imageFile);
    //var imageFile= "user.png";  //research on how to get the filename from the input

    if (imageFile == '') {
        alert("Please choose image file first");
    } else {
        $.ajax({
            url: "controllers/delegatesImageUpload.php", // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: data_, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false
            success: function (data) // A function to be called if request succeeds
            {
                successNote(sresponse, 'Image uploaded sussessfully!')
                $("#" + form + "_avatar").html('<img id="delegateImg" class="img-responsive avatar-view" src="../app/images/members/' + imageFile + '" alt="Avatar" title="Change the avatar" >');
            },
            error: function (data) {
                errorNote(data, 'image not uploaded successfully!')
            }
        });
    }
    $("#uploadForm").hide(500);
}

//save member image file
function saveImageFromFile_existingDel(form) {
    var imageForm = document.getElementById(form);
    var filepath = imageForm.image_file.value;
    var imageFile = filepath.split(/[\\\/]/).pop();
    var id = imageForm.memberId.value;
    console.log(id);
    var data_ = new FormData(imageForm);
    sessionStorage.setItem("delegateImage", imageFile);
    //var imageFile= "user.png";  //research on how to get the filename from the input

    if (imageFile == '') {
        alert("Please choose image file first");
    } else {
        $.ajax({
            url: "controllers/existingDelegatesImgUpload.php", // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: data_, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false
            success: function (data) // A function to be called if request succeeds
            {
                successNote(sresponse, 'Image uploaded sussessfully!')
                $("#" + form + "_avatar").html('<img id="delegateImg" class="img-responsive avatar-view" src="../app/images/members/' + id + '.jpg" alt="Avatar" title="Change the avatar" >');
            },
            error: function (data) {
                errorNote(data, 'image not uploaded successfully!')
            }
        });
    }
    $("#uploadForm").hide(500);
}

//Add New Member data to file	short form
function addShortMember(img) { //alert("now in");

    //save webcam snap
    var formName_ = 'addMemberFormx';
    var locality_ = 2;
    var country_ = $("#rcountry option:selected").val();
    var role_ = $("#rrole option:selected").val();
    console.log(country_);
    var tags_ = "";
    var address_ = "";
    var surburb_ = "";
    var discipleMaker_ = 0;
    var houseChurch_ = 0;
    //var data = $.parseJSON(sessionStorage.data);
    var eventId_ = $("#eventId").val();
    var firstName_ = addMemberFormx.rfirstName.value;
    var lastName_ = addMemberFormx.rlastName.value;
    var email_ = addMemberFormx.remail.value;
    //var image_ = $("#imageName").val();
    //var image_ = sessionStorage.getItem("webcam");
    console.log('image received:', img);
    var token_ = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);


    //alert(locality_);
    var gender_ = 'M';
    if ($('#genderF').isCheck == true) {
        gender_ = 'F';
    }
    if (country_ == '') {
        alert("Please fill out the nation name");
    } else {
        $.ajax({
            url: api_base_url + "/cmfi_members", //Required URL of the page on server
            type: "POST",
            data: { // Data Sending With Request To Server
                firstName: firstName_,
                lastName: lastName_,
                email: email_,
                phone: addMemberFormx.rphone.value,
                gender: gender_,
                surburb: surburb_,
                city: addMemberFormx.rcity.value,
                address: address_,
                country: addMemberFormx.rcountry.value,
                houseChurch: houseChurch_,
                discipleMaker: discipleMaker_,
                DOB: addMemberFormx.rDOB.value,
                image: img,
                tags: tags_,
                locality: locality_,
                addedBy: userId,
                familyID: 0,
                familyRole: 1,
                role: role_
            },
            success: function (id) { // Required Callback Function
                successNote(sresponse, 'New delegate saved successfully!');
                console.log(id);
                addMemberFormx.leaderId.value = id;
                //new member is now being registered to event
                $.post(api_base_url + "/cmfi_event_registration", //Required URL of the page on server
                    { // Data Sending With Request To Server
                        eventId: eventId_,
                        memberId: id,
                        remark: 'Web',
                        token: token_
                    },
                    function (id, status) {
                        console.log("event reg id: " + id);
                        if (id) {
                            successNote(sresponse, '<p> Member has been registered sussessfully!</p>');
                            //send email to registrant after successful registration
                            if (email_ != "") {
                                $.get(api_base_url + "/cmfi_events?filter=eventId,eq," + eventId_,
                                    function (data) {
                                        var event = data.records[0];
                                        $.post("../app/config/sendEmail.php", {
                                                firstname: firstName_,
                                                lastname: lastName_,
                                                email: email_,
                                                eventTitle: event.eventTitle,
                                                eventVenue: event.eventVenue,
                                                startDate: event.startDate,
                                                endDate: event.endDate,
                                                token: token_
                                            },

                                            function (response) { // Required Callback Function

                                                var res = response;
                                                console.log(res);

                                                if (res.id == 1) {
                                                    successNote(sresponse, res.text);
                                                } else {
                                                    errorNote(fresponse, res.text);
                                                }
                                            })
                                    });
                            }
                            $('#eventsRegTable').DataTable().ajax.reload();
                            $('#addMemberFormx')[0].reset();
                        } else {
                            errorNote(fresponse, 'registration failed try again!');
                        }
                    });

            },
            error: function (err) {
                errorNote(fresponse, err.responseJSON.message);
            }

        });

    }


}



/*==================================
 * USER Functions
 *=================================*/
//change admin permissions
function checkAdmin(id_) {
    console.log('changing admin role..for: ', id_)
    $.ajax({
        url: "../app/api/post.php", //Required URL of the page on server
        type: "POST",
        data: { // Data Sending With Request To Server

            formName: 'checkAdmin',
            userId: id_
        },
        success: function (response) { // Required Callback Function
            console.log(response);
            if (response == 1) {
                successNote(sresponse, 'Setting data saved sussessfully!')
            } else {
                errorNote(fresponse, 'Setting data not saved!')
            }
        },
        error: function (err) {
            errorNote(fresponse, 'Setting data not saved!' + err)
        }
    });

}


/*==================================
 * send registration notification email
 *=================================*/
function resendRegEmail(token, eventId, memberId) {
    //get event record
    $.get(api_base_url + "/cmfi_events?filter=eventId,eq," + eventId,
        function (data) {
            var event = data.records[0];

            $.get(api_base_url + "/cmfi_members?filter=id,eq," + memberId,
                function (data) {
                    var member = data.records[0];
                    //send email
                    $.ajax({
                        url: "../app/config/sendEmail.php",
                        type: "POST",
                        data: {
                            firstname: member.firstName,
                            lastname: member.lastName,
                            email: member.email,
                            eventTitle: event.eventTitle,
                            eventVenue: event.eventVenue,
                            startDate: event.startDate,
                            endDate: event.endDate,
                            token: token
                        },
                        success: function (data) {
                            console.log("email sent successfully");
                            successNote(sresponse, "email sent successfully");
                        },
                        error: function (err) {
                            errorNote(fresponse, err.responseText);
                        }
                    });

                });
        });
}

/*==================================
 * Register new delegate to an event
 *=================================*/
function registerMember(id, eventId_) {
    var preferredAcc_ = $("#preferredAcc option:selected").val();
    var room_ = $("#room option:selected").val();
    var babySitting_ = $("#needBabySitting option:selected").val();
    var transportMeans_ = $("#meansoftransport option:selected").val();
    var transporttoKoume_ = $("#transporToKoume option:selected").val();
    var accommodation_ = $("#accommodation option:selected").val();
    var arrivaldate_ = $("#arrivaldate").val();
    var depaturedate_ = $("#depaturedate").val();
    var healthQn_ = $("#healthQn option:selected").val();
    var time = Date.now();
    var childrenNames_ = "";
    var pickUpEntry_ = $("#pickupport option:selected").val();
    var dietRestrictions_ = $("#dietRestrictions option:selected").val();
    var dietRestrictionsSpec_ = "";
    var transDetails_ = $('#transportDetails').val();
    var accDetails_ = "";
    var healthConditionSpec_ = "";
    var email_ = regForm.email.value;
    if (email_ === '') {
        email_ = regForm.refEmail.value;
    }
    var firstname_ = regForm.firstname.value;
    var lastname_ = regForm.lastname.value;
    var entryPort_ = $("select.port option:selected").val();
    var token_ = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

    var medfile = "";
    var medForm = document.getElementById('regForm2');
    //var imgVal = $('#medicalform').val();
    /*if(imgVal!=''){
      var filepath=medForm.medicalform.value;
      medfile= filepath.split(/[\\\/]/).pop();
    }*/


    if (regForm1.children == "1") {
        childrenNames_ = regForm1.elements["childrennames"].value;
    }
    if (dietRestrictions_ == "1") {
        dietRestrictionsSpec_ = $("#dietRestrictionSpecification").val();
    }
    if (regForm1.transporToKoume == "1") {
        transDetails_ = regForm1.elements["transportDetails"].value;
    }
    if (regForm3.accommodation == "1") {
        accDetails_ = regForm3.elements["accommodationDetails"].value;
    }
    if (healthQn_ == "1") {
        healthConditionSpec_ = regForm2.healthConditionSpec.value;
    }
    if (preferredAcc_ == "") {
        preferredAcc_ = 0;
    }
    if (dietRestrictions_ == "") {
        dietRestrictions_ = 0;
    }
    if (transporttoKoume_ == "") {
        transporttoKoume_ = 0;
    }
    if (babySitting_ == "") {
        babySitting_ = 0;
    }
    if (pickUpEntry_ == "") {
        pickUpEntry_ = 0;
    }



    //check if delegate is already registered
    $.get(api_base_url + "/cmfi_event_registration?filter=eventId,eq," + eventId_ + "&filter=memberId,eq," + id, function (data) {
        if (data.records.length != 0) {
            console.log(data);
            var token = data.records[0].token;
            //delegate exists
            new PNotify({
                title: 'Registration Previously done',
                text: `Would you like to update your profile for this event instead?  \n
        Use the button/link in the email you received after your initial registration to update your details\n
        If you have not received a registration alert email please click the button below \n\n
        <a onclick="resendRegEmail('${token}',${eventId_},${id})" class="btn btn-sm btn-default" >Resend Email</a> `,
                type: 'notice'
            });
        } else {
            //delegate does not exist .. proceed to register
            //uploadMedicalForm(id);
            $.ajax({
                url: api_base_url + "/cmfi_event_registration", //Required URL of the page on server
                type: "POST",
                data: { // Data Sending With Request To Server
                    eventId: eventId_,
                    memberId: id,
                    children: regForm.children.value,
                    needBabySitting: babySitting_,
                    childrenNames: childrenNames_,
                    transportMode: transportMeans_,
                    entryPort: entryPort_,
                    pickUpEntry: pickUpEntry_,
                    arrivalDate: arrivaldate_,
                    departureDate: depaturedate_,
                    arrangedTransport: transporttoKoume_,
                    dietRestrictions: dietRestrictions_,
                    dietRestSpec: dietRestrictionsSpec_,
                    accommodation: accommodation_,
                    transDetails: transDetails_,
                    accDetails: accDetails_,
                    preferredAcc: preferredAcc_,
                    healthCond: healthConditionSpec_,
                    //medicalForm:medfile,
                    remark: 'Web',
                    accType: room_,
                    token: token_
                },
                success: function (id, status) {
                    console.log(">>successfully registered to event with registration id: " + id);
                    if (id) {
                        successNote(sresponse, '<p> you have been registered sussessfully! \n\nPlease check your email for more information on how to update your registration details.</p>');
                        //send email to registrant after successful registration
                        $.get(api_base_url + "/cmfi_events?filter=eventId,eq," + eventId_,
                            function (data) {
                                var event = data.records[0];

                                $.ajax({
                                    url: "../app/config/sendEmail.php",
                                    type: "POST",
                                    data: {
                                        firstname: firstname_,
                                        lastname: lastname_,
                                        email: email_,
                                        eventTitle: event.eventTitle,
                                        eventVenue: event.eventVenue,
                                        startDate: event.startDate,
                                        endDate: event.endDate,
                                        token: token_
                                    },
                                    success: function (data) {
                                        console.log("email sent successfully");
                                    },
                                    error: function (err) {
                                        errorNote(fresponse, err.responseText);
                                    }
                                });


                            });

                    }
                },
                error: function (err) {
                    errorNote(fresponse, 'registration failed try again! ' + err.responseText);
                }
            });
        }
    })
    setTimeout(function () {
        routie('/event/' + eventId_);
    }, 2500);
}

function updateMember(id) {
    var country_ = $("#country option:selected").val();
    var nationality_ = $("#nationality option:selected").val();
    var gender_ = $("#gender option:selected").val();
    var img = sessionStorage.getItem("delegateImage");

    var passport_ = $("#passportNo").val();

    var img = getImageName('delegateImg');
    console.log(">>image File at save:", img);

    var data = { // Data Sending With Request To Server
        firstName: regForm.firstname.value,
        lastName: regForm.lastname.value,
        email: regForm.email.value,
        city: regForm.city.value,
        phone: regForm.phone.value,
        profession: regForm.profession.value,
        sex: gender_,
        country: country_,
        language: regForm.language.value,
        nationality: nationality_,
        passport: passport_,
        DOB: regForm.birthday.value,
        image: img
    }
    $.ajax({
        type: 'PUT',
        url: api_base_url + "/cmfi_members/" + id,
        contentType: 'application/json',
        data: JSON.stringify(data),
    }).done(function () {
        console.log('updated member details successfully');
    }).fail(function (msg) {
        console.log('Failed to save member details: ' + msg)
    });

}

function checkIn() {
    //check if delegate has already checked in
    var eventId_ = getId(1);
    var members_ = $("#membersDropDown").val();
    var session_ = $("#sessionCombo option:selected").val();
    //if not then checkin delegate
    $.ajax({
        url: "../app/api/post.php", //Required URL of the page on server
        type: "POST",
        data: { // Data Sending With Request To Server
            formName: 'eventCheckinForm',
            id: eventId_,
            members: members_,
            session: session_
        },
        success: function (id, status) {
            $('#eventsRegTable').DataTable().ajax.reload();
        },
        error: function (err) {

        }
    });

}

function uploadMedicalForm(id) {
    var data_ = new FormData();
    var medform = document.getElementById('medicalform');
    var file = medform.files[0];
    data_.append('medicalform', file);
    data_.append('memberId', id);
    console.log(data_);
    /*if(medFile=='')
      {
      alert("Please select medical form file first");
      }
      else{*/
    $.ajax({
        url: "controllers/fileUpload.php", // Url to which the request is send
        type: "POST", // Type of request to be send, called as method
        data: data_, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false, // To send DOMDocument or non processed data file it is set to false
        success: function (data) // A function to be called if request succeeds
        {
            //successNote(sresponse,'medical form uploaded sussessfully! ')
            console.log("medical form uploaded successfully!");
        },
        error: function (data) {
            errorNote(fresponse, 'medical form not uploaded successfully!')
        }
    });
    // }
}

//save member using event manager
function createNewMember(eventId_) { //alert("now in");
    var country_ = $("#country option:selected").val();
    var nationality_ = $("#nationality option:selected").val();
    var gender_ = $("#gender option:selected").val();
    var firstname_ = regForm.firstname.value;
    var lastname_ = regForm.lastname.value;
    var email_ = regForm.email.value;
    var refEmail_ = regForm.refEmail.value;
    var passport_ = $("#passportNo").val();


    //checking if the delegate already exists in the database
    $.get(api_base_url + "/cmfi_members?filter=firstName,eq," + firstname_ + "&filter=lastName,eq," + lastname_,
        function (data) {
            var data2 = data.records;
            if (data2 && data2.length) {
                var id = data2[0].id;
                //this member is already in the database
                updateMember(id);
                registerMember(id, eventId_);

            } else {
                //not registered
                var img = getImageName('delegateImg');
                console.log(">>image File at save:", img);
                console.log(">>Email:", email_);
                console.log(">> Ref Email:", refEmail_);
                //check if email already exists
                $.getJSON(api_base_url + '/cmfi_members?filter=email,eq,' + email_, function (data) {
                    if (email_.length > 5 && data.records.length != 0) {
                        console.log(data);
                        var token = data.records[0].token;
                        //delegate exists
                        new PNotify({
                            title: 'Cannot Create Record',
                            text: `This email address is already in use by another user. Please check and use a different email`,
                            type: 'notice'
                        });
                    } else {
                        $.ajax({
                            url: api_base_url + "/cmfi_members", //Required URL of the page on server
                            type: "POST",
                            data: { // Data Sending With Request To Server
                                firstName: regForm.firstname.value,
                                lastName: regForm.lastname.value,
                                email: regForm.email.value,
                                city: regForm.city.value,
                                phone: regForm.phone.value,
                                profession: regForm.profession.value,
                                sex: gender_,
                                country: country_,
                                language: regForm.language.value,
                                nationality: nationality_,
                                DOB: regForm.birthday.value,
                                passport: passport_,
                                image: img,
                                refEmail: refEmail_
                            },
                            success: function (data) {
                                console.log("member id: " + data);
                                //use the id to create event registration
                                registerMember(data, eventId_);
                            },
                            error: function (err) {
                                console.log(err.responseText);
                                errorNote("Oops something's wrong!", err.responseText);
                            }

                        });
                    }
                });
            }

        });

}

function uploadEventImage() {
    var imageForm = document.getElementById('uploadimage');
    var filepath = imageForm.image_file.value;
    var imageFile = filepath.split(/[\\\/]/).pop();

    if (imageFile == '') {
        alert("Please choose image file first");
    } else {
        $.ajax({
            url: "controllers/eventImageUpload.php", // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(imageForm), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false
            success: function (data) // A function to be called if request succeeds
            {
                //successNote(sresponse,'event image uploaded sussessfully! ')
                //var res=$.parseJSON(data);
                console.log(data);
                $("#profileAvatar").html(`<img onclick="showHide('uploadForm')" class="img-responsive avatar-view" src="../images/events/${imageFile}" alt="Avatar" title="Change the avatar" >`);
            },
            error: function (err) {
                console.log(err);
                errorNote(fresponse, 'event image did not upload successfully!')
            }
        });
        $("#uploadForm").hide(500);
    }
}

/*********************************************************/
/**                  EVENT FUNCTIONS                     */
/*********************************************************/

//lock Registration
function lockRegistration(id,isReg){
    $.ajax({
        url: api_base_url + "/cmfi_events/" + id,
        type: "PUT",
        contentType: 'application/json',
        data: {
            isReg:isReg
        },
    })
    .done(function () {
        var m="Event registration Disabled"
        if(isReg==1)
        {
            m="Event registration Enabled";
        }
        successNote(sresponse, m);
        $('#globalEventsTable').DataTable().ajax.reload(); //refresh events table
    })
    .fail(function (msg) {
        console.log('Error locking/unlocking Registration: ' + msg.responseText)
        console.log(msg);
    });
}

//Make Event a Private Event 
function makePrivateEvent(id,isPrivate){
    $.ajax({
        url: api_base_url + "/cmfi_events/" + id,
        type: "PUT",
        contentType: 'application/json',
        data: {
            isPrivate:isPrivate
        },
    })
    .done(function () {
        var m="Event made public"
        if(isPrivate==1)
        {
            m="Event made private";
        }
        successNote(sresponse, m);
        $('#globalEventsTable').DataTable().ajax.reload(); //refresh events table
    })
    .fail(function (msg) {
        console.log('Error making event private: ' + msg.responseText)
        console.log(msg);
    });
}



//create new event
function addNewEvent_DONOTUSE() {
    //var id = uploadimage.eventId.value;
    var start_ = addEventForm.reservation.value.substring(0, 10);
    var end_ = addEventForm.reservation.value.substring(12, 10);

    var data = {
        eventTitle: addEventForm.eventTitle.value,
        eventDesc: addEventForm.eventDesc.value,
        eventVenue: addEventForm.eventVenue.value,
        eventColor: addEventForm.eventColor.value,
        startDate: start_,
        endDate: end_,
        nation: addEventForm.eventRegion.value,
        regFee: addEventForm.regFee.value,
        criteria: addEventForm.criteria.value,
        toBring:addEventForm.toBring.value,
        eventLeader: addEventForm.eventLeader.value
    }

    $.ajax({
            url: api_base_url + "/cmfi_events/",
            type: "POST",
            contentType: 'application/json',
            data: data,
        })
        .done(function () {
            console.log('saved event details successfully');
            successNote(sresponse, "Event details updated successfully!");
            //uploadEventImage(id);
        })
        .fail(function (msg) {
            console.log('Failed to save event details: ' + msg.responseText)
            console.log(msg);
        });


}


//update existing event
function updateEvent(id) {
    //var id = uploadimage.eventId.value;
    var resDate=addEventForm.reservation.value;
    var start_ = resDate.slice(0, 10);
    var end_ = resDate.slice(12, 23);
    console.log('start',start_)
    console.log('end:',end_);
    var data = {
        eventTitle: addEventForm.eventTitle.value,
        eventDesc: addEventForm.eventDesc.value,
        eventVenue: addEventForm.eventVenue.value,
        eventColor: addEventForm.eventColor.value,
        startDate: start_,
        endDate: end_,
        nation: addEventForm.eventRegion.value,
        regFee: addEventForm.regFee.value,
        criteria: addEventForm.criteria.value,
        toBring:addEventForm.toBring.value,
        eventLeader: addEventForm.eventLeader.value
    }

    $.ajax({
            url: api_base_url + "/cmfi_events/" + id,
            type: "PUT",
            contentType: 'application/json',
            data: data,
        })
        .done(function () {
            console.log('saved event details successfully');
            console.log('to bring:',addEventForm.toBring.value);
            successNote(sresponse, "Event details updated successfully!");
            //uploadEventImage(id);
        })
        .fail(function (msg) {
            console.log('Failed to save event details: ' + msg.responseText)
            console.log(msg);
        });


}

//register event from form
function eventRegister() {
    var formName_ = "eventRegForm";
    $("#loaderImg").show(); //show loader image
    var members_ = $("#membersDropDown").val();
    var id_ = $("#eventId").val();
    //alert(id_);
    if (members_ == '') {
        alert("Please select members ");
    } else {
        $.post("../app/api/post.php", //Required URL of the page on server
            { // Data Sending With Request To Server
                formName: formName_,
                members: members_,
                id: id_
            },
            function (data, status) { // Required Callback Function
                var obj = $.parseJSON(data);
                console.log(obj['msg']);
                if (obj['status'] == 1) {
                    successNote(sresponse, obj['msg']);
                } else {
                    errorNote(fresponse, obj['msg'] + " Event Registration failed!");
                }
                $('#eventsRegTable').DataTable().ajax.reload();
                $("#membersDropDown").select2("val", "");
                $("#loaderImg").hide(); //hide loader image
            });

    }

}

/*--------------------
 * Checkin functions
 *------------------*/
function checkIn() {
    var id = getId(1);
    var date = $("#dates option:selected").val();
    console.log('check-in date:', date);
    var members_ = $("#membersDropDown").val();
    $.ajax({
        url: '../app/api/post.php',
        type: 'POST',
        data: {
            formName: "eventCheckinForm",
            members: members_,
            id: id,
            date: date
        },
        success: function (data) {
            console.log("successfully checked in :", data);
            $('#eventsRegTable').DataTable().ajax.reload();
            $("#membersDropDown").select2("val", "");
        },
        error: function (err) {
            console.log("error:", err);
        }
    })
}


//Checkin barcode
function checkInBC(regId) {
    var date = sessionStorage.getItem("sessionDate").substring(0, 10);
    var session_ = sessionStorage.getItem("sessionId");
    var count = sessionStorage.getItem("sessionCount");
    if (!date) {
        date = moment().format("YYYY-MM-DD");
    }
    var time = moment().format("hh:mm");
    date = date + ' ' + time;
    console.log('check-in date:', date);
    var id = getId(1);
    console.log("date: ", date);
    console.log("session: ", session_)
    //check if member is already checked into event

    $.get(`${api_base_url}/cmfi_event_attendance?filter=eventId,eq,${id}&filter=sessionId,eq,${session_}&filter=regId,eq,${regId}`,
        function (data) {
            res = data.records;

            if (res.length == 1) {
                //console.log("found checkin")
                warningNote("Oops duplicate entry", "Delegate already checked into this session");
            } else {
                //console.log("not found")
                $.ajax({
                    url: `${api_base_url}/cmfi_event_attendance/`,
                    type: 'POST',
                    data: {
                        regId: regId,
                        eventId: id,
                        date: date,
                        sessionId: session_
                    },
                    success: function (data) {
                        //console.log("successfully checked in :", data);
                        //$('#eventsRegTable').DataTable().ajax.reload();

                        count++;
                        sessionStorage.setItem("sessionCount", count);
                        console.log(count);
                        $("#count_" + session_).html(count);
                        successNote("Success!", "Checked In!!");

                    },
                    error: function (err) {
                        //console.log("error:", err);
                        errorNote("Oops something went wrong", err.responseText + "\n \n<strong>Please ensure that you hve selected the date and session for check-in first</strong>");
                    }
                })

            }
            //clear inputs
            $("#membersDropDown").select2("val", "");


        })



}


//Checkin barcode
function checkInBCArray(barcodes) {
    var date = sessionStorage.getItem("sessionDate").substring(0, 10);
    var session_ = sessionStorage.getItem("sessionId");
    var count = sessionStorage.getItem("sessionCount");
    count = parseInt(count, 10);
    if (!date) {
        date = moment().format("YYYY-MM-DD");
    }
    var time = moment().format("hh:mm");
    date = date + ' ' + time + ":00";
    console.log('check-in date:', date);
    var id = getId(1);
    var n = 0;

    $.ajax({
        url: `../app/api/post.php/`,
        type: 'POST',
        data: {
            formName: 'eventCheckinForm',
            members: barcodes,
            eventId: id,
            date: date,
            sessionId: session_
        },
        success: function (data) {
            var obj = $.parseJSON(data);
            console.log(obj)
            n = parseInt(obj['n'], 10);
            var z = obj['z'];
            count = count + n;

            sessionStorage.setItem("sessionCount", count);
            //console.log(count);
            $("#count_" + session_).html(count);
            successNote("Success!", "Checked In!! processed " + n + " delegates registered \n" + z + " delegates were already registered");

        },
        error: function (err) {
            //console.log("error:", err);
            errorNote("Oops something went wrong", err.responseText + "\n \n<strong>Please ensure that you hve selected the date and session for check-in first</strong>");
        }
    })



}

//add new session
function addSession() {
    var eventId = getId(1);
    var name = $("#name").val();
    var description = $("#description").val();
    var icon = $("#icon").val();
    var parent = sessionStorage.getItem("sessionParent");
    console.log("parent log:", parent)
    $.get(`${api_base_url}/cmfi_event_sessions?filter=id,eq,${parent}`, function (data) {
        res = data.records[0];
        $.ajax({
            url: api_base_url + "/cmfi_event_sessions/",
            type: "POST",
            data: {
                eventId: eventId,
                name: name,
                description: description,
                icon: icon,
                start: res.start,
                end: res.end,
                parent: parent,
                class: ''

            },
            success: function (response, status) { // Required Callback Function
                successNote(sresponse, 'New session  <strong>' + name + '</strong> created sussessfully!')
                $('#sessionForm')[0].reset();
                showPage('check-in');
            },
            error: function (err) {
                console.log(err)
                errorNote(fresponse, 'New Session not saved! ' + err.responseText)
            }
        });
    });
}



//uncheck someone from event
function eventUncheck(regId) {
    $.ajax({
        url: `${api_base_url}/cmfi_event_attendance/${regId}`,
        type: 'DELETE',
        data: {},
        success: function (data) {
            console.log("successfully deleted checked in :", data);
            $('#eventsRegTable').DataTable().ajax.reload();
        },
        error: function (err) {
            console.log("error:", err);
        }
    });
}

//clear temporary table
function clearTempSilent() {

    $.ajax({
        url: "../app/api/post.php", //Required URL of the page on server
        type: "POST",
        data: { // Data Sending With Request To Server
            procedure: "clearTemp",
            userId: userId
        },
        success: function (response, status) { // Required Callback Function
            if (response == 1) {
                //successNote(sresponse,'data cleared sussessfully!')
                //$('#data_preview').DataTable().ajax.reload();

            } else {
                //errorNote(fresponse,'data not cleared!')
            }
        }
    });;

}

//clear temporary table
function clearTemp() {

    $.ajax({
        url: "../app/api/post.php", //Required URL of the page on server
        type: "POST",
        data: { // Data Sending With Request To Server
            procedure: "clearTemp",
            userId: userId
        },
        success: function (response, status) { // Required Callback Function
            if (response == 1) {
                successNote(sresponse, 'data cleared sussessfully!')
                $('#data_preview').DataTable().ajax.reload();
            } else {
                errorNote(fresponse, 'data not cleared!')
            }
        }
    });;

}



function blockFormat(text) {
    return `<li>
      <p class="excerpt">${text}
      </p>
</li>`
}

function registerFromTempTable() {
    $.getJSON(api_base_url + '/cmfi_memberspreview?filter=addedBy,eq,' + userId, function (data) {
        var members = [];

        data.records.forEach(el => {
            //members.push(el.id);
            console.log(el);
            var eventId_ = getId(1);
            var token_ = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            var dob = el.DOB;
            if (dob === "0000-00-00") {
                dob = "2000-01-01";
            }
            //check if member already exists in database
            $.get(api_base_url + '/cmfi_members?filter=firstName,eq,' + el.firstName + '&filter=lastName,eq,' + el.lastName + '&filter=country,eq,' + el.country, function (data) {
                if (data.records.length != 0) { // record already exists


                } else { //record does not exist
                    //create member record
                    $.ajax({
                        url: api_base_url + "/cmfi_members", //Required URL of the page on server
                        type: "POST",
                        data: { // Data Sending With Request To Server
                            firstName: el.firstName,
                            lastName: el.lastName,
                            email: el.email,
                            city: el.city,
                            phone: el.phone,
                            sex: el.sex,
                            country: el.country,
                            DOB: dob,
                            locality: 1,
                            address: el.address,
                            surburb: el.surburb,
                            nationality: el.country,
                            image: "user.png"

                        },
                        success: function (data) {
                            var memberId = data
                            $('#importResults').append(blockFormat(`${el.firstName} >>New member record imported into db - id: ${memberId}`));
                            //use the id to create event registration
                            //proceed to register member


                            //check if delegate is already registered
                            $.get(api_base_url + "/cmfi_event_registration?filter=eventId,eq," + eventId_ + "&filter=memberId,eq," + data, function (data) {
                                if (data.records.length != 0) {
                                    console.log(data);
                                    var token = data.records[0].token;
                                    //delegate exists

                                } else {
                                    //delegate does not exist .. proceed to register
                                    //uploadMedicalForm(id);
                                    $.ajax({
                                        url: api_base_url + "/cmfi_event_registration", //Required URL of the page on server
                                        type: "POST",
                                        data: { // Data Sending With Request To Server
                                            eventId: eventId_,
                                            memberId: memberId,
                                            remark: 'File Import',
                                            token: token_
                                        },
                                        success: function (id, status) {
                                            console.log(">>successfully registered to event with registration id: " + id);
                                            if (id) {
                                                $('#importResults').append(blockFormat(`${el.firstName} ${el.lastName} Registered succesfully to event-> ${eventId_}`));

                                                //successNote(sresponse,'<p> you have been registered sussessfully! \n\nPlease check your email for more information on how to update your registration details.</p>');
                                                //send email to registrant after successful registration
                                                $.get(api_base_url + "/cmfi_events?filter=eventId,eq," + eventId_,
                                                    function (data) {
                                                        var event = data.records[0];

                                                        $.ajax({
                                                            url: "../app/config/sendEmail.php",
                                                            type: "POST",
                                                            data: {
                                                                firstname: el.firstName,
                                                                lastname: el.lastName,
                                                                email: el.email,
                                                                eventTitle: event.eventTitle,
                                                                eventVenue: event.eventVenue,
                                                                startDate: event.startDate,
                                                                endDate: event.endDate,
                                                                token: token_
                                                            },
                                                            success: function (data) {
                                                                $('#importResults').append(blockFormat('registration email sent to >> ' + el.email));
                                                            },
                                                            error: function (err) {
                                                                $('#importResults').append(blockFormat(err.responseText));
                                                            }
                                                        });


                                                    });

                                            }
                                        },
                                        error: function (err) {
                                            $('#importResults').append(blockFormat('registration failed try again! ' + err.responseText + '</p></li>'));
                                        }
                                    });
                                } //finish registration if delegate does not exist
                            });
                        },
                        error: function (err) {
                            console.log(err.responseText);
                            $('#importResults').append(blockFormat(err.responseText));
                        }

                    });

                } //finish creating member record if member does not exist

            });

        }); //end foreach
        $('#importResults').append(`</ul>`);
    });

}


//register a member to an event automatically
function eventRegisterAuto(memberId) {
    var formName_ = "eventRegFormAuto";
    $("#loaderImg").show(); //show loader image
    var member_ = memberId;
    var id_ = $("#eventId").val();
    var token_ = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

    if (id_ > 0 && id_ != null) {
        //alert(id_);
        if (member_ == '') {
            alert("member Id not specified ");
        } else {
            $.post("../app/api/post.php", //Required URL of the page on server
                { // Data Sending With Request To Server
                    formName: formName_,
                    member: member_,
                    id: id_,
                    token: token_
                },
                function (data, status) { // Required Callback Function
                    var obj = $.parseJSON(data);
                    console.log(obj['msg']);
                    if (obj['status'] == 1) {
                        successNote(sresponse, obj['msg']);
                    } else {
                        $.get(api_base_url + "/cmfi_events?filter=eventId,eq," + eventId_,
                            function (data) {
                                var event = data.records[0];
                                $.post("../app/config/sendEmail.php", {
                                        firstname: firstname_,
                                        lastname: lastname_,
                                        email: email_,
                                        eventTitle: event.eventTitle,
                                        eventVenue: event.eventVenue,
                                        startDate: event.startDate,
                                        endDate: event.endDate,
                                        token: token_
                                    },

                                    function (response) { // Required Callback Function

                                        var res = response;
                                        console.log(res);

                                        if (res.id == 1) {
                                            successNote(sresponse, res.text);
                                        } else {
                                            errorNote(fresponse, res.text);
                                        }
                                    })
                            });
                        (fresponse, obj['msg'] + " Event Registration failed!");
                    }
                    $('#eventsRegTable').DataTable().ajax.reload();
                    $("#membersDropDown").select2("val", "");
                    $("#loaderImg").hide(); //hide loader image
                });

        }
    }

}

function eventDeregister(id) {
    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Are you sure you want to delete this registration?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {
            'dir1': 'down',
            'dir2': 'right',
            'modal': true
        }
    })).get().on('pnotify.confirm', function () {
        $.ajax({
            url: '../app/api/api.php/records/cmfi_event_registration/' + id,
            type: 'DELETE',
            success: function (response, status) {
                successNote(sresponse, 'Delegate deregistered sussessfully!');
                $('#eventsRegTable').DataTable().ajax.reload(); //refresh registrations table
            },
            error: function (err) {
                errorNote(fresponse, 'Deregistration not successful!' + err.responseJSON);
            }
        });
    });
}

//drop event to another date
function deleteTempRec(id) { // alert('now in');
    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Are you sure you want to delete this Delegate?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {
            'dir1': 'down',
            'dir2': 'right',
            'modal': true
        }
    })).get().on('pnotify.confirm', function () {

        $.ajax({
            url: '../app/api/api.php/records/cmfi_memberspreview/' + id,
            type: 'DELETE',
            success: function (response, status) {
                if (response == 1) {
                    successNote(sresponse, 'User deleted sussessfully!');
                } else {
                    errorNote(fresponse, 'Deleting user failed');
                }
                $('#data_preview').DataTable().ajax.reload(); //refresh provinces table

            },
            error: function (err) {
                errorNote(fresponse, 'Deleting user failed' + err.responseText);
            }
        });



    });


}


//--------- ------ adding new event ------- -------------------// 
function addNewEvent(){
    var resDate=addEventForm.reservation.value;
    var start_ = resDate.slice(0, 10);
    var end_ = resDate.slice(12, 23);
    console.log('start:',start_);
    console.log('end:',end_);
    var filepath = uploadimage.image_file.value; console.log('filepath',filepath);
    var imageFile = filepath.split(/[\\\/]/).pop();console.log('image file',imageFile);

    var data = {
        eventTitle: addEventForm.eventTitle.value,
        eventDesc: addEventForm.eventDesc.value,
        eventVenue: addEventForm.eventVenue.value,
        eventColor: addEventForm.eventColor.value,
        image:imageFile,
        startDate: start_,
        endDate: end_,
        nation: addEventForm.eventRegion.value,
        regFee: addEventForm.regFee.value,
        criteria: addEventForm.criteria.value,
        eventLeader: addEventForm.eventLeader.value,
        toBring:addEventForm.toBring.value
    }

    $.ajax({
            url: api_base_url + "/cmfi_events/" ,
            type: "POST",
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function(id){
                console.log(id);
                successNote(sresponse, "NEW event  created successfully!");
                $("#addEventForm")[0].reset(); //clear form
                $("#profileAvatar").html(""); //empty image
                showHideForm("eventAddForm"); //close form
            $('#globalEventsTable').DataTable().ajax.reload(); //refresh events table
            }
        })
        .done(function () {
            console.log('NEW event created successfully');
            
            //uploadEventImage(id);
        })
        .fail(function (msg) {
            console.log('Failed to create event: ' + msg.responseText)
            console.log(msg);
        });


}

//------------------ clone event to new --------------------------//
//update existing event
function cloneEvent(image_) {
    //var id = uploadimage.eventId.value;
    var resDate=addEventForm.reservation.value;
    var start_ = resDate.slice(0, 10);
    var end_ = resDate.slice(12, 23);
   
    
    var data = {
        eventTitle: addEventForm.eventTitle.value,
        eventDesc: addEventForm.eventDesc.value,
        eventVenue: addEventForm.eventVenue.value,
        eventColor: addEventForm.eventColor.value,
        startDate: start_,
        image:image_,
        endDate: end_,
        nation: addEventForm.eventRegion.value,
        regFee: addEventForm.regFee.value,
        criteria: addEventForm.criteria.value,
        toBring:addEventForm.toBring.value,
        eventLeader: addEventForm.eventLeader.value
    }

    $.ajax({
            url: api_base_url + "/cmfi_events/",
            type: "POST",
            contentType: 'application/json',
            data: data,
        })
        .done(function () {
            console.log('Successfully cloned to new event!');
            console.log('to bring:',addEventForm.toBring.value);
            successNote(sresponse, "Successfully cloned to new event!");
            $('#globalEventsTable').DataTable().ajax.reload(); //refresh events table
            //uploadEventImage(id);
        })
        .fail(function (msg) {
            console.log('Failed to save event details: ' + msg.responseText)
            console.log(msg);
        });


}

//------------------ save existing event --------------------------//
function saveEvent(id) {
    var title = $('#eventTitle').val();
    var eventDesc = $('#eventDesc').val();
    var start = addEventForm.reservation.value.substring(0, 10);
    var end = addEventForm.reservation.value.substring(12, 10);
    var eventColor = $('#eventColor').val();
    var eventId = $('#eventId').val(); //alert(eventId);
    var nation = $('#eventRegion option:selected').val();
    var regFee = $('#regFee').val();
    var eventVenue = $('#eventVenue').val();
    var image = $('#image').val();
    var criteria = $('#criteria').val();
    var coodinator = $('#eventLeader').val();
    uploadEventImage(id);


    $.ajax({
        url: '../app/api/post.php',
        data: 'type=fullUpdate&title=' + title + '&eventId=' + eventId + '&start=' + start + '&end=' + end + '&eventColor=' + eventColor + '&eventDesc=' + eventDesc + '&nation=' + nation + '&eventVenue=' + eventVenue + '&criteria=' + criteria + '&regFee=' + regFee + '&image=' + image + '&eventLeader=' + eventLeader,
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            successNote(sresponse, 'event saved sussessfully!')
            //if(response.status == 'success')
            //$('#calendar').fullCalendar('refetchEvents');
            //initialise event form
            uploadEventImage(id);
            $("#addEventForm")[0].reset(); //clear form
            $('#globalEventsTable').DataTable().ajax.reload(); //refresh events table
        },
        error: function (e) {
            errorNote('Error processing your request: ' + e.responseText);
        }
    });


}

//--------------------- edit event form builder --------------------------//
function eventEdit(id) {
    var form = "'addLocalityForm'";
    var container = "'localityAddForm'";
    $.getJSON(api_base_url + "/cmfi_events?filter=eventId,eq," + id, function (data) {
        var res = data.records[0];
        var options = {
            startDate: res.startDate.substring(0, 10),
            endDate: res.endDate.substring(0, 10),
            minDate: '2010-01-01',
            maxDate: '2065-12-31',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            format: 'YYYY-MM-DD',
        }
        $('#reservation').daterangepicker(options);
        addEventForm.eventTitle.value = res.eventTitle;
        addEventForm.eventDesc.value = res.eventDesc;
        addEventForm.reservation.value = res.startDate.substring(0, 10) + ' - ' + res.endDate.substring(0, 10);
        addEventForm.eventColor.value = res.eventColor;
        addEventForm.eventLeader.value = res.eventLeader;
        addEventForm.eventVenue.value = res.eventVenue;
        addEventForm.criteria.value = res.criteria;
        addEventForm.regFee.value = res.regFee;
        addEventForm.toBring.value=res.toBring;
        $('#eventColor').trigger('change');
        addEventForm.eventRegion.value = res.nation;
        $('#eventRegion').trigger('change');
        $('#event-id').html(`<input type="hidden" name="eventId" id="eventId" value="${id}" >`)
        $('#eventTitle').html('<h2>Edit Event <small>select leader</small></h2><div class="clearfix"></div>');
        $('#eventSubmit').html(`<button  onclick="cloneEvent('${res.image}' )" type="submit" class="btn btn-warning" name="submit" ><i class="fa fa-copy"></i>  Clone to new</button>
        <button  onclick="cancel(${form},${container})" type="submit" class="btn btn-default" name="submit" >
        <i class="icon-remove"></i> Cancel</button>
        <button  onclick="updateEvent(${id})" type="submit" class="btn btn-success" name="submit" ><i class="icon-check"></i>  Save</button>`);
        $('#profileAvatar').html(`<img  onclick="showHide('uploadForm1')" class="img-responsive avatar-view" src="../images/events/${res.image}" alt="Avatar" width="100%"
                                              onerror="if (this.src != '../images/events/${res.image}') this.src = 'images/events/event-default.jpg';">`)
        $('#evTitle').html('<h2>Edit Event <small>global event update</small></h2>');
        //alert(response.name);
    });

}

//------------------------ delete event ----------------------//
function deleteEvent(id, title) { // alert('now in');
    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Are you sure you want to delete this event?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {
            'dir1': 'down',
            'dir2': 'right',
            'modal': true
        }
    })).get().on('pnotify.confirm', function () {

        $.ajax({
            url: '../app/api/api.php/records/cmfi_events/' + id,
            type: 'DELETE',
            success: function (response, status) {
                if (response == 1) {
                    successNote(sresponse, 'Event deleted sussessfully!')
                } else {
                    errorNote(fresponse, 'Event not deleted!')
                }

                $('#globalEventsTable').DataTable().ajax.reload(); //refresh provinces table

            }
        });

    });
}

function save() {
    var regForm = document.getElementById("regForm");
    var regForm1 = document.getElementById('regForm1');
    var regForm2 = document.getElementById('regForm2');
    var id = regForm.memberId.value;
    var eventId = getId(1);
    var country_ = $("#country option:selected").val();
    var nationality_ = $("#nationality option:selected").val();
    var gender_ = $("#gender option:selected").val();
    //save member details
    $.ajax({
        url: api_base_url + "/cmfi_members/" + id,
        type: "PUT",
        data: {
            firstName: regForm.firstname.value,
            lastName: regForm.lastname.value,
            email: regForm.email.value,
            city: regForm.city.value,
            phone: regForm.phone.value,
            profession: regForm.profession.value,
            sex: gender_,
            country: country_,
            language: regForm.language.value,
            nationality: nationality_,
            DOB: regForm.birthday.value
        },
        success: function (res) {
            console.log('saved member details successfully');
        },
        error: function (err) {
            console.log("error:", err);
        }
    });

    //user details
    var data = $.parseJSON(sessionStorage.data);
    var userId = data.user.user_id;

    //save event personal details


    var children_ = $("#children option:selected").val();
    var childrenNo_ = $("#howmany option:selected").val();
    var babySittingArr_ = $("#babysittingarrangements option:selected").val();

    var meansOfTransport_ = $("#meansoftransport option:selected").val();
    var entryPort_ = "";

    var preferredAcc_ = $("#preferredAcc option:selected").val();
    var room_ = $("#room option:selected").val();
    var babySitting_ = $("#needBabySitting option:selected").val();
    var transportMeans_ = $("#meansoftransport option:selected").val();
    var transporttoKoume_ = $("#transporToKoume option:selected").val();
    var accommodation_ = $("#accommodation option:selected").val();
    var arrivaldate_ = $("#arrivaldate").val();
    var depaturedate_ = $("#depaturedate").val();
    var time = Date.now();
    var childrenNames_ = "";
    var pickUpEntry_ = $("#pickupport option:selected").val();
    var dietRestrictions_ = $("#dietRestrictions option:selected").val();
    var dietRestrictionsSpec_ = "";
    var transDetails_ = "";
    var accDetails_ = "";


    if (regForm.children == "Yes") {
        childrenNames_ = regForm.elements["childrennames"].value;
    }
    if (dietRestrictions_ == "Yes") {
        dietRestrictionsSpec_ = $("#dietRestrictionSpecification").val();
    }
    if (regForm.transporToKoume == "Yes") {
        transDetails_ = regForm.elements["transportDetails"].value;
    }
    if (regForm3.accommodation == "Yes") {
        accDetails_ = regForm.elements["accommodationDetails"].value;
    }

    if (meansOfTransport_ === "air") {
        entryPort_ = $("#air option:selected").val();
    } else if (meansOfTransport_ === "road") {
        entryPort_ = $("#road option:selected").val();
    } else if (meansOfTransport_ === "water") {
        entryPort_ = $("#water option:selected").val();
    }

    $.ajax({
        url: api_base_url + "/cmfi_event_registration/" + sessionStorage.getItem("regId"),
        type: "PUT",
        data: {
            children: regForm.children.value,
            needBabySitting: babySitting_,
            childrenNames: childrenNames_,
            transportMode: transportMeans_,
            entryPort: entryPort_,
            pickUpEntry: pickUpEntry_,
            arrivalDate: arrivaldate_,
            departureDate: depaturedate_,
            arrangedTransport: transporttoKoume_,
            dietRestrictions: dietRestrictions_,
            dietRestSpec: dietRestrictionsSpec_,
            accommodation: accommodation_,
            transDetails: transDetails_,
            accDetails: accDetails_,
            preferredAcc: preferredAcc_,
            remark: 'Admin Update',
            accType: room_,
            registeredBy: userId
        },
        success: function (res) {
            successNote(sresponse, 'saved member details successfully!');
        },
        error: function (err) {
            errorNote("Oops!", err.responseText);
        }
    });
}


function showProfile(token) {
    //get event registration record
    $.getJSON(api_base_url + "/cmfi_event_registration?filter=token,eq," + token, function (response) {
        var data = response.records[0];
        var imageForm = document.getElementById('uploadimage');
        console.log(data);
        sessionStorage.setItem("regId", data.id);
        $('select#meansoftransport').val(data.transportMode);
        $('select#meansoftransport').trigger('change');
        if (data.transportMode == "Air") {
            $('select#air').val(data.entryPort);
            $('select#air').trigger('change');
        } else if (data.transportMode == "Road") {
            $('select#road').val(data.entryPort);
            $('select#road').trigger('change');
        } else {
            $('select#water').val(data.entryPort);
            $('select#water').trigger('change');
        }
        regForm1.arrivaldate.value = data.arrivalDate;
        regForm1.depaturedate.value = data.departureDate;
        $('select#pickupport').val(data.pickUpEntry);
        $('select#pickupport').trigger('change');
        $('select#children').val(data.children);
        $('select#children').trigger('change');
        $('select#transporToKoume').val(data.transKoume);
        $('select#transporToKoume').trigger('change');
        $('select#dietRestrictions').val(data.dietRestrictions);
        $('select#dietRestrictions').trigger('change');
        $('select#invitation').val(data.inviteMe);
        $('select#invitation').trigger('change');
        $('select#accommodation').val(data.accommodation);
        $('select#accommodation').trigger('change');
        $('select#preferredAcc').val(data.preferredAcc);
        $('select#preferredAcc').trigger('change');
        $('select#healthQn').val(data.healthSpec);
        $('select#healthQn').trigger('change');

        //update passport viewer
        $('#passportViewer').html(`<iframe src = "ViewerJS/#../files/passports/${data.passImg}" width='500' height='350' frameBorder="0" allowfullscreen webkitallowfullscreen></iframe>`);
        $('#ticketViewer').html(`<iframe src = "ViewerJS/#../files/tickets/${data.airticket}" width='500' height='350' frameBorder="0" allowfullscreen webkitallowfullscreen></iframe>`);
        setTimeout(
            function () {
                if (data.children === 1) {
                    //console.log("children number:",data.chnNumber);
                    $('select#howmany').val(data.chnNumber);
                    $('select#howmany').trigger('change');
                    $('#childrennames').val(data.childrenNames);
                    $('select#babysittingarrangements').val(data.bsArr);
                    $('select#babysittingarrangements').trigger('change');
                }
                //if(data.accommodation===1){
                $('#accommodationDetails').val(data.accDetails);
                // }
                //if(data.preferredAcc===1){
                $('select#room').val(data.accType);
                $('select#room').trigger('change');
                //}
                console.log("acc details:", data.accDetails);
                $('#dietRestrictionSpecification').val(data.dietRestSpec);
                $('#healthConditionSpec').val(data.healthCond);
            }, 3000);


        if (regForm1.transporToKoume == "1") {
            regForm1.transportDetails.value = data.transDetails;
        }
        JsBarcode("#barcode", data.id, {
            displayValue: false,
            //fontSize:24,
            lineColor: "#0cc"
        });
        var form = "uploadForm";
        var temp = new Array();
        var id = data.memberId;
        imageForm.memberId.value = id; //set memberId in form



        $.ajax({
            url: api_base_url + "/cmfi_members?join=cmfi_countries&filter=id,eq," + id,
            type: "GET",
            success: function (res) {
                var d = res.records[0];
                $('#memberName').html(d.firstName + ' ' + d.lastName);
                $('#memberEmail').html(d.email);
                $('#memberPhone').html(d.phone);
                if (d.sex === "M") {
                    $('#memberGender').html("<i class='icon-symbol-male'></i> Male");
                } else {
                    $('#memberGender').html("<i class='icon-symbol-female'></i> Female");
                }
                //$('#memberGender').html(response.sex);
                $('#memberAddress').html(d.address + ', ' + d.surburb + ', ' + d.city);
                $('#memberCity').html(d.city);
                $('#memberCountry').html(d.country.name);
                $('#photo').html(`<img onclick="showHide('+form+')" class="img-responsive avatar-view" src="../app/images/members/${d.image}"  alt="Avatar" 
                    onerror="if (this.src != '../app/images/members/user.png') this.src = '../app/images/members/user.png';"/>`);
                $('#uploadimage_avatar').html(`<img id="delegateImg" name="delegateImg"  onclick="showHide('uploadForm')"  class="img-responsive avatar-view" src="../app/images/members/${d.image}"  alt="Avatar" 
                    onerror="if (this.src != '../app/images/members/user.png') this.src = '../app/images/members/user.png';"/>`);

                $("#tags_container").html('');
                $("#tags_container").html('<input id="tags" type="text" class="tags form-control" value="" />');
                var name = d.firstName + ' ' + d.lastName;

                //alert(response.name);

                //regForm.country.value=data.country;
                $('select#country').val(d.country.code);
                $('select#country').trigger('change');
                regForm.firstname.value = d.firstName;
                regForm.lastname.value = d.lastName;
                regForm.email.value = d.email;
                regForm.city.value = d.city;
                regForm.memberId.value = d.id;
                regForm.phone.value = d.phone;
                regForm.profession.value = d.profession;
                $('select#gender').val(d.sex);
                $('select#gender').trigger('change');
                $('select#nationality').val(d.nationality);
                $('select#nationality').trigger('change');

                regForm.language.value = d.language;
                regForm.nationality.value = d.nationality;
                regForm.birthday.value = d.DOB;
                regForm1.passportNo.value = d.passport;
                $('#tags').val(d.tags);

                $('#tags').tagsInput({
                    width: 'auto'
                });

            },
            error: function (err) {
                console.log(err.responseText);
            }
        });


        //get event details
        $.ajax({
            url: api_base_url + "/cmfi_events?filter=eventId,eq," + data.eventId,
            type: "GET",
            success: function (res) {
                var data = res.records[0];
                $('.imgContainer').html(`<img src="images/events/${data.image}" style="width: 100%;top: 0px;margin-top: 0px;">
              <h2 id="eventTitle" class="centered custom-font-element custom-font-4  vc_custom_1431297137931" 
              style="font-size: 12px;
                top:26px;
                width:100%;
                font-weight: 700;
                line-height: 17px;
                text-transform: uppercase;
                letter-spacing: 0.5px;
                text-shadow:1px 1px #333333;
                color: #ffffff; ">
                ${data.eventTitle}</h2>`);
                console.log("event Title:", data.eventTitle);
                //eventRegForm.eventId.value=data.eventId;
            },
            error: function (err) {
                console.log(err);
            }
        });

    });


}



//Edit Temp Record
function editTempRec(id, name, value) {
    var rowId = "row" + id;
    var currentTD = $("#editRec" + id).parents('tr').find('td');
    var x = $("#editRec" + id).html();
    //alert (x);
    if ($("#editRec" + id).html() == '<i class="glyphicon glyphicon-pencil"></i>') {
        $.each(currentTD, function () {
            $(this).prop('contenteditable', true),
                $(this).prop('style', 'color:red')
        });

    } else {
        $.each(currentTD, function () {
            $(this).prop('contenteditable', false),
                $(this).prop('style', 'color:black')

        });
        var id_ = id;
        var name_ = $("#name" + id).html();
        var value_ = $("#value" + id).html();
        saveSetting(id_, name_, value_);
    }
    $("#editRec" + id).html($("#editRec" + id).html() == '<i class="glyphicon glyphicon-pencil"></i>' ? '<i class="glyphicon glyphicon-ok"></i> Save' : '<i class="glyphicon glyphicon-pencil"></i>')
    //$("#"+rowId).remove();
    //$("#"+rowId).after('<tr id="rowId"><form id="settings" method="post"><td><input type="text" class="form-control" name="name" id="name"  value="'+name+'" required="required"></td><td><input type="text" class="form-control" id="value" name="value"  value="'+value+'" required="required"><input id="settingId" name="settingID" type="hidden" value="'+id+'" /></td></form><td><a onclick="saveSetting()" id="settingSave" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-ok"></i></a></td></tr>');
}


function Lang(lang) {
    var lang = sessionStorage.setItem("lang");
    switch (lang) {
        case 'en':
            //assign form elements to english language

            break;
        case 'fr':
            //assign form elements to french

            break;


    }
}

//----------------------------------- USER FUNCTIONS -------------------------------------//
//save Member as user	
function makeUser(id_, firstName_, email_, phone_) { // alert("now in");
    console.log(id_ + "," + firstName_ + "," + email_ + "," + phone_);
    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Are you sure you want to make this person a system user?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {
            'dir1': 'down',
            'dir2': 'right',
            'modal': true
        }
    })).get().on('pnotify.confirm', function () {
        var formName_ = "makeUser";
        $.post("../app/api/post.php", //Required URL of the page on server
            { // Data Sending With Request To Server
                formName: formName_,
                email: email_,
                phone: phone_,
                firstName: firstName_,
                memberId: id_

            },
            function (data, status) { // Required Callback Function
                console.log("response:", data);
                //var obj= $.parseJSON(data);
                if (data.includes("true")) {
                    successNote(sresponse, "User created successfully");
                } else {
                    errorNote(fresponse, "user not created");
                }
            });
    });
}
// send user credentials by email
function sendUserCredentials(email) { // alert("now in");
    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Are you sure you want to reset this account?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {
            'dir1': 'down',
            'dir2': 'right',
            'modal': true
        }
    })).get().on('pnotify.confirm', function () {
        var formName_ = "sendUserCredentials";
        $.post("../app/api/post.php", //Required URL of the page on server
            { // Data Sending With Request To Server
                formName: formName_,
                email: email

            },
            function (data, status) { // Required Callback Function
                var obj = $.parseJSON(data);
                console.log(obj['text']);
                if (obj['id'] == "1") {
                    successNote(sresponse, obj['text']);
                } else {
                    errorNote(fresponse, obj['text']);
                }
            });
    });
}




//----------------------------------notifications------------------------------------------------//
//success nofication
function successNote(title_, text_) {
    new PNotify({
        title: title_,
        text: text_,
        type: 'success'
    });
}

//failure or Error notifications
function errorNote(title_, text_) {
    new PNotify({
        title: title_,
        text: text_,
        type: 'error'
    });
}


//wrning  notifications
function warningNote(title_, text_) {
    new PNotify({
        title: title_,
        text: text_,
        type: 'warning'
    });
}