<?php
//---------------------------wcdb version 1.0-------------------------------------------
//------------Function/Procedure to import excel file into DB---------------------------
//-------------------------Tapiwa Jeka 14/07/2016 --------------------------------------

require_once('../app/config-small3.php'); //load configuration settings
//require("../app/config/db-settings.php"); //Require DB connection
//require("../app/config/class.member.php");
//require("../app/config/class.event.php");
//require_once("../app/api/funcs.php");
$userId=$_POST['userId'];

if (isset($_POST['fn']) && $_POST['fn'] == 'importTemp') {
        
            $file = $_FILES['file']['tmp_name'];
            $country=$_POST['wnation'];
            $city=$_POST['wcity'];
            $DOB='0000-00-00';
            $handle = fopen($file, "r");
            $c = 0;

            //upload only of this is an excel file
            $extension = end(explode('.', $file));

            try{
                while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
                {
                    //set default values
                    $sex="F";
                    $address="";
                    $surburb="";
                    $phone="";
                    $email="";
                    $tags="";

                    //assign actual values from worksheet
                    $firstName = $filesop[0];
                    $lastName = $filesop[1];
                    if(isset($filesop[2])){ $sex=$filesop[2];}
                    if(isset($filesop[3])){ $address=$filesop[3];}
                    if(isset($filesop[4])){ $surburb=$filesop[4];}
                    if(isset($filesop[5])){ $phone=$filesop[5];}
                    if(isset($filesop[6])){ $email=$filesop[6];}
                    if(isset($filesop[7])){ $tags=$filesop[7];}
                    
                    $m=new member;
                    $m->firstName=$firstName;
                    $m->lastName=$lastName;
                    $m->sex=$sex;
                    $m->address=$address;
                    $m->surburb=$surburb;
                    $m->phone=$phone;
                    $m->email=$email;
                    $m->tags=$tags;
                    $m->country=$country;
                    $m->city=$city;
                    $m->userId=$userId;
                    
                    //$res= $m->AddTemp();
                    if($m->AddTemp()){
                        $c++;
                    } 
                    
                }
                if($c>0){
                    echo "success";
                }else{
                    echo "error 001";
                } 
        }catch(Exception $e){
                echo $e.getMessage();
            }
        
        
}

if (isset($_POST['fn']) && $_POST['fn'] == 'finishImport') {
        $count=0;
        
    //import preview members into the members table
        $members=array();
        $m=new member;
        $temp=getTempMembers($userId);
        foreach ($temp as $t){
                $m->firstName=$t["firstName"];
                $m->lastName=$t["lastName"];
                $m->phone=$t["phone"];
                $m->email=$t["email"];
                $m->sex=$t["sex"];
                $m->address=$t["address"];
                $m->surburb=$t["surburb"];
                $m->city=$t["city"];
                $m->locality=$t["locality"];
                $m->country=$t["country"];
                $m->DOB=$t["DOB"];
                $m->modifiedDate=$t["modifiedDate"];
                $m->userId=$t["addedBy"];
                
                //add member to table

                if($m->Add()){
                $count++;}
                array_push($members,$mysqli->insert_id); //push last insterted id into array
            }
 
    
    if($count>0){
        //register members to event
        $e=new event();
		$e->eventId = $mysqli->real_escape_string($_POST['eventId']);
		$e->members = $members;
		$e->registeredBy=$mysqli->real_escape_string($_POST['userId']);
		$result=$e->Register();
        echo "success";
    }
    else{echo "error 002";}
    
}

function getTempMembers($userId)
{
     global $mysqli,$db_table_prefix; 
    try{
	$stmt = $mysqli->prepare("SELECT 
	    m.id,
		firstName,
		lastName,
		salutation,
		address,
		surburb,
		city,
		locality,
		c.name,
		sex,
		DOB,
		assembly,
		email,
		phone,
		addedBy,
		m.modifiedDate,
		image,
		tags	
		FROM ".$db_table_prefix."memberspreview m
		LEFT JOIN ".$db_table_prefix."countries c on m.country=c.code 
		LEFT JOIN ".$db_table_prefix."localities l ON m.locality=l.id
		WHERE m.addedBy=?");
		$stmt->bind_param('i',$userId);
	    $stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		if($check != 0){
			$stmt->bind_result($id, $firstName, $lastName, $salutation, $address, $surburb, $city,
			$locality, $country, $sex, $DOB,$assembly, $email,$phone,$addedBy,$modifiedDate,$image,$tags);
			while ($stmt->fetch()){
		 
			$row[] = array('id' => $id, 'firstName' => $firstName, 'lastName' => $lastName, 'salutation' => $salutation, 'address' => $address,
			'surburb' => $surburb, 'city' => $city, 'locality' => $locality, 'country' => $country, 'sex' => $sex, 
			'DOB' => $DOB,'assembly' => $assembly,'email' => $email,'phone' => $phone,'addedBy' => $addedBy, 'modifiedDate' => $modifiedDate, 'image' => $image,'tags'=>$tags);
			 }
			$stmt->close();
			return ($row);
		}
        else{echo "no data";
        }
    }catch(Exception $e){
        echo $e.getMessage();
    }
}



?>