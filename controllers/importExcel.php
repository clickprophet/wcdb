<?php
//---------------------------wcdb version 1.0-------------------------------------------
//------------Function/Procedure to import excel file into DB---------------------------
//-------------------------Tapiwa Jeka 14/07/2016 --------------------------------------

//include_once'../app/config-small3.php'; //load configuration settings
require_once("../app/config/db-settings.php"); //Require DB connection
//require_once("../app/config/class.member.php");
require_once("../app/config/class.event.php");
//require_once("../app/api/funcs.php");
$userId=$_POST['userId'];

if (isset($_POST['fn']) && $_POST['fn'] == 'importTemp') {
        
            $file = $_FILES['file']['tmp_name'];
            $country=$_POST['wnation'];
            $city=$_POST['wcity'];
            $DOB='0000-00-00';
            $handle = fopen($file, "r");
            $c = 0;

            //upload only of this is an excel file
            $extension = end(explode('.', $file));

            try{
                while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
                {
                    //set default values
                    $sex="F";
                    $address="";
                    $surburb="";
                    $phone="";
                    $email="";
                    $tags="";

                    //assign actual values from worksheet
                    $firstName = $filesop[0];
                    $lastName = $filesop[1];
                    if(isset($filesop[2])){ $sex=$filesop[2];}
                    if(isset($filesop[3])){ $address=$filesop[3];}
                    if(isset($filesop[4])){ $surburb=$filesop[4];}
                    if(isset($filesop[5])){ $phone=$filesop[5];}
                    if(isset($filesop[6])){ $email=$filesop[6];}
                    if(isset($filesop[7])){ $tags=$filesop[7];}
                    $locality=1;
                    
                    try{
                    global $mysqli,$db_table_prefix; 
                    $time=date("Y-m-d");
                    $stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."memberspreview (
                        firstName,
                        lastName,
                        phone,
                        email,
                        sex,
                        city,
                        locality,
                        country,
                        DOB,
                        addedBy
                        )
                        VALUES (
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?
                        )");
                    $stmt->bind_param("sssssssssi", 
                                        $firstName,
                                        $lastName,
                                        $phone,
                                        $email,
                                        $sex,
                                        $city,
                                        $locality,
                                        $country,
                                        $DOB,
                                        $userId
                                        );
                    if($stmt->execute()){
                        $c++;
                    } 
                    $stmt->close();	
                }catch(Exception $e) {
                    echo $e.getMessage();
                }
            }
            if($c>0){
                echo "success";
            }else{
                echo "error 001";
            } 
        }catch(Exception $e){
                echo $e.getMessage();
            }
        
        
}

if (isset($_POST['fn']) && $_POST['fn'] == 'finishImport') {
        $count=0;
        
    //import preview members into the members table
        $members=array();
        $m=new member;
        $temp=fetchTempMembers($userId);
        foreach ($temp as $t){
                
                //add member to table
                global $mysqli,$db_table_prefix; 
                $time=date("Y-m-d");
                if(nameExists($this->firstName,$this->lastName)==0){
                try{
                    $stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."members (
                        firstName,
                        lastName,
                        phone,
                        email,
                        sex,
                        city,
                        locality,
                        country,
                        DOB,
                        dateAdded,
                        addedBy,
                        modifiedDate
                        )
                        VALUES (
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?
                        )");
                    $stmt->bind_param("ssssssssssis", 
                                        $t["firstName"],
                                        $t["lastName"],
                                        $t["phone"],
                                        $t["email"],
                                        $t["sex"],
                                        $t["city"],
                                        $t["locality"],
                                        $t["country"],
                                        $t["DOB"],
                                        $time,
                                        $userId,
                                        $time
                                        );
                    if($stmt->execute()){
                        $count++;}
                    ;
                    $stmt->close();	
                    $result=array('status'=>1,'msg'=>'successfully added new disciple <strong>'.$t["firstName"].' '.$t["lastName"].'</strong>');
                }catch(Exception $e){
                    $result=array('status'=>0,'msg'=>$e.getMessage());
                }
                return $count;
                }
                else{
                    return array('status'=>0,'msg'=>'Name Exists');
                }
                
                array_push($members,$mysqli->insert_id); //push last insterted id into array
            }
 
    
    if($count>0){
        //register members to event
        $e=new event();
		$e->eventId = $mysqli->real_escape_string($_POST['eventId']);
		$e->members = $members;
		$e->registeredBy=$mysqli->real_escape_string($_POST['userId']);
		$result=$e->Register();
        echo "success";
    }
    else{echo "error 002";}
    
}

function nameExists($fn,$ln)
{
	global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("SELECT firstName,lastName FROM ".$db_table_prefix."members 
		WHERE lastName=? and firstName=?");
		$stmt->bind_param('ss', $ln,$fn);
		$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		$stmt->close();
		if($check != 0){
			return 1;
		}else{
			return 0;
		}
}

function fetchTempMembers($userId)
{
     global $mysqli,$db_table_prefix; 
    try{
	$stmt = $mysqli->prepare("SELECT 
	    m.id,
		firstName,
		lastName,
		salutation,
		address,
		surburb,
		city,
		locality,
		c.name,
		sex,
		DOB,
		assembly,
		email,
		phone,
		addedBy,
		m.modifiedDate,
		image,
		tags	
		FROM ".$db_table_prefix."memberspreview m
		LEFT JOIN ".$db_table_prefix."countries c on m.country=c.code 
		LEFT JOIN ".$db_table_prefix."localities l ON m.locality=l.id
		WHERE m.addedBy=?");
		$stmt->bind_param('i',$userId);
	    $stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		if($check != 0){
			$stmt->bind_result($id, $firstName, $lastName, $salutation, $address, $surburb, $city,
			$locality, $country, $sex, $DOB,$assembly, $email,$phone,$addedBy,$modifiedDate,$image,$tags);
			while ($stmt->fetch()){
		 
			$row[] = array('id' => $id, 'firstName' => $firstName, 'lastName' => $lastName, 'salutation' => $salutation, 'address' => $address,
			'surburb' => $surburb, 'city' => $city, 'locality' => $locality, 'country' => $country, 'sex' => $sex, 
			'DOB' => $DOB,'assembly' => $assembly,'email' => $email,'phone' => $phone,'addedBy' => $addedBy, 'modifiedDate' => $modifiedDate, 'image' => $image,'tags'=>$tags);
			 }
			$stmt->close();
			return ($row);
		}
        else{echo "no data";
        }
    }catch(Exception $e){
        echo $e.getMessage();
    }
}



?>